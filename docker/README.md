# Docker

We use docker to create a dev environment. For more information, see our
[ansible docker docs](https://docs.mayfirst.org/ansible-docker-initialize/).
