#!/usr/bin/python3

# This script parses the yaml file and does sanity checking. If it finds a
# problem, it prints the problem and exits with 1. If no problems are detected,
# it is silent and exits with 0.

import os
import sys
import yaml

def main(argv = None):
    if argv == None:
        argv = sys.argv

    try:
      hosts_yaml_path = argv[1]
      host_vars_path = os.path.dirname(hosts_yaml_path) + "/host_vars"

      # After reading the yaml files, we should end up with two dicts, one
      # with a list of hosts...
      hosts_yaml = {}
      # ... and one with all the host vars keyed by host name.
      hosts_vars_yaml = {} 

      # Load the hosts.yml file.
      with open(hosts_yaml_path) as f:
        hosts_yaml = yaml.load(f, Loader=yaml.FullLoader)
        for host_vars_filename in os.listdir(host_vars_path):
            host_vars_yaml_path = host_vars_path + "/" + host_vars_filename
            host_name = os.path.splitext(host_vars_filename)[0]
            if os.path.isfile(host_vars_yaml_path):
                with open(host_vars_yaml_path) as f:
                    try:
                        hosts_vars_yaml[host_name] = yaml.load(f, Loader=yaml.FullLoader)
                    except yaml.parser.ParserError as e:
                        print("Yaml parse error: {0}.".format(e))
                        return 1 

    except FileNotFoundError:
        print("Failed to load hosts or host_vars file. Please pass full path as the only argument, e.g. ./initialize-docker-environment ../inventories/dev/hosts.yml.")
        return 1 

    seen_ips = []
    seen_data_disks = []
    seen_kvm_names = []
    seen_simplemonitor_labels = []
    for host_name in hosts_vars_yaml:
        values = hosts_vars_yaml[host_name]
        # Ensure no IP address is assigned twice.
        if "network" in values:
            for network in values['network']:
                if "networks" in network:
                    for address_block in network['networks']:
                        if 'address' in address_block:
                            address = address_block['address']
                            if address in seen_ips:
                                raise ValueError("The IP address {0} was found twice. Last found in {1}.".format(address, host_name ))
                            seen_ips.append(address)

        # Ensure no kvm name is defined twice.
        if "kvm" in values:
            if values["kvm"]["name"] in seen_kvm_names:
                raise ValueError("The kvm name {0} was found twice. Last found in {1}.".format(values["kvm"]["name"], host_name ))
            seen_kvm_names.append(values["kvm"]["name"])

            if "data_disks" in values['kvm']:
                for data_disk in values['kvm']['data_disks']:
                    if data_disk['name'] in seen_data_disks:
                        raise ValueError("The data disk {0} was found twice. Last found in {1}.".format(data_disk['name'], host_name ))
                    seen_data_disks.append(data_disk['name'])

        # Ensure no simple monitor label is defined twice.
        if "simplemonitor" in values:
            for label in values["simplemonitor"]["checks"]:
                if label in seen_simplemonitor_labels:
                    raise ValueError("The simplemonitor label {0} was found twice. Last found in {1}.".format(label, host_name ))
            seen_simplemonitor_labels.append(label)

if __name__ == "__main__":
    try:
        sys.exit(main())
    except ValueError as err:
        print(err)
        sys.exit(1)

