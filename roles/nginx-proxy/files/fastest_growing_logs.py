#!/usr/bin/python3
import os
import time
import argparse
import math

def get_file_sizes(directory):
    file_sizes = {}
    for root, _, files in os.walk(directory):
        for file in files:
            file_path = os.path.join(root, file)
            try:
                size = os.path.getsize(file_path)
                file_sizes[file_path] = size
            except OSError as e:
                print(f"Could not access {file_path}: {e}")
    return file_sizes

def calculate_size_differences(initial_sizes, final_sizes):
    size_differences = {}
    for file_path, initial_size in initial_sizes.items():
        final_size = final_sizes.get(file_path, initial_size)
        size_difference = final_size - initial_size
        size_differences[file_path] = size_difference
    return size_differences

def display_top_files(size_differences, top_n=5):
    sorted_files = sorted(
        size_differences.items(),
        key=lambda x: abs(x[1]),
        reverse=True
    )
    print(f"\nTop {top_n} files with the biggest difference in size:")
    for file_path, size_diff in sorted_files[:top_n]:
        size_diff_pretty = convert_bytes(size_diff)
        print(f"{file_path}: {size_diff_pretty}")

def convert_bytes(size_bytes):
    if size_bytes == 0:
        return "0B"

    size_name = ("B", "KB", "MB", "GB", "TB", "PB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return f"{s} {size_name[i]}"

def main():
    directory = "/var/log/nginx"

    parser = argparse.ArgumentParser(
        description='Detect which nginx log files are growing the fastest.'
    )
    parser.add_argument(
        '--sleep',
        help='How many seconds to wait between checking, default is 15 seconds',
        type=int
    )
    parser.set_defaults(sleep = 15)
    args = parser.parse_args()

    if not os.path.isdir(directory):
        print(f"Error: {directory} is not a valid directory.")
        return

    print("First pass gathering log file sizes...")
    initial_sizes = get_file_sizes(directory)

    print(f"Waiting for {args.sleep} seconds...")
    time.sleep(args.sleep)

    print("Second pass gathering log file sizes...")
    final_sizes = get_file_sizes(directory)

    size_differences = calculate_size_differences(initial_sizes, final_sizes)
    display_top_files(size_differences)

if __name__ == "__main__":
    main()
