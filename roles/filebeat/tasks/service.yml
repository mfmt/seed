---
# tasks file for filebeat 
- name: install packages
  apt:
    name:
      # sudo is needed for ansible to "become" a user.
      - sudo

- name: Create filebeat group
  group:
    name: filebeat
    system: yes

# They have to be part of the adm gruop to read nginx
# log files. Maybe other groups will need to be added
# as we add more log files to be read.
- name: create filebeat user
  user:
    name: filebeat 
    home: /var/lib/filebeat
    group: filebeat
    groups: "{{ filebeat_groups }}" 
    system: yes
  notify: restart filebeat

# If filebeat starts before we properly tell systemd to run it as the
# filebeat user, then the registry file will be owned as root and
# when we restart filebeat as the filebeat user it will fail to
# start. 
#
# To avoid this problem, check for the file ownership of the registry
# file and stop filebeat if it exists and is owned by root.
- name: check of /var/lib/filebeat/registry is root owned
  stat:
    path: /var/lib/filebeat/registry
  register: filebeat_registry

- name: stop filebeat so registry permissions get properly set
  command: systemctl stop filebeat
  when: filebeat_registry.stat.exists and filebeat_registry.stat.uid == 0

- name: ensure filebeat dirs are fully owned by filebeat user
  file:
    path: "{{ item }}" 
    state: directory
    owner: filebeat
    recurse: yes
  with_items:
    - /var/lib/filebeat
    - /var/log/filebeat

# There is nothing sensitive in this file.
- name: ensure filebeat user can read filebeat.yml file
  file:
    path: /etc/filebeat/filebeat.yml
    mode: "0655"

# When using become and become_as we need this tmp directory to
# be created to avoid an ansible warning.
- name: create remote temp directory to avoid ansible complaints
  file:
    state: directory
    path: /var/lib/filebeat/.ansible/tmp
    owner: filebeat

- name: register filebeat keystore users
  command: filebeat keystore list
  register: list_keystore
  become: yes
  become_user: filebeat

# See https://www.elastic.co/guide/en/beats/filebeat/current/keystore.html
- name: create filebeat keystore
  command: filebeat keystore create
  args:
    creates: /var/lib/filebeat/filebeat.keystore
  become: yes
  become_user: filebeat

- name: set filebeat password
  shell: echo {{ vault_elasticsearch_filebeat_writer_password }} | filebeat keystore add FILEBEAT_WRITER_PWD --stdin
  when: vault_elasticsearch_filebeat_writer_password is defined and 'FILEBEAT_WRITER_PWD' not in list_keystore.stdout_lines
  notify: restart filebeat
  become: yes
  become_user: filebeat

- name: set elasticsearch host
  lineinfile:
    path: /etc/filebeat/filebeat.yml
    regex: '  hosts:'
    line: '  hosts: ["{{ m_elasticsearch_host }}:{{ m_elasticsearch_port }}"]'
  notify: restart filebeat
  tags: filebeat-yml

- name: set elasticsearch protocol 
  lineinfile:
    path: /etc/filebeat/filebeat.yml
    regex: "protocol: "
    line: '  protocol: "https"'
  notify: restart filebeat

- name: set elasticsearch username 
  lineinfile:
    path: /etc/filebeat/filebeat.yml
    regex: "username: "
    line: '  username: "filebeat_writer"'
  notify: restart filebeat

- name: set elasticsearch password 
  lineinfile:
    path: /etc/filebeat/filebeat.yml
    regex: "password: "
    line: '  password: "${FILEBEAT_WRITER_PWD}"'
  notify: restart filebeat

# We load templates manually on the elasticsearch host.
- name: disable template loading 
  lineinfile:
    path: /etc/filebeat/filebeat.yml
    regex: "setup.template.enabled:"
    line: "setup.template.enabled: false"
  notify: restart filebeat

# We don't have permission to check the ilm, so avoid
# permission errors.
- name: disable ilm check
  lineinfile:
    path: /etc/filebeat/filebeat.yml
    regex: "setup.ilm.check_exists:"
    line: "setup.ilm.check_exists: false"
  notify: restart filebeat

# We don't have permission to write to the ilm, so avoid
# any permission errors.
- name: disable ilm overwrite 
  lineinfile:
    path: /etc/filebeat/filebeat.yml
    regex: "setup.ilm.overwrite:"
    line: "setup.ilm.overwrite: false"
  notify: restart filebeat

# See https://code.mayfirst.org/mfmt/seed-inventory/-/issues/27
- name: avoid seccomp bug in bookworm.
  lineinfile:
    path: /etc/filebeat/filebeat.yml
    regex: "seccomp.enabled: false"
    line: "seccomp.enabled: false"
  when: ansible_distribution_release == "bookworm"
  notify:
    - restart filebeat

- name: enabled nginx module
  import_tasks: service.nginx.yml
  when: '"nginx" in filebeat_modules'
 
- name: enabled powerbase module
  import_tasks: service.powerbase.yml
  when: '"powerbase" in filebeat_modules'

- name: create systemd override directory
  file:
    path: /etc/systemd/system/filebeat.service.d
    state: directory

- name: copy systemd overide file
  copy:
    src: user.conf
    dest: /etc/systemd/system/filebeat.service.d/user.conf
  notify:
    - systemctl refresh 
    - restart filebeat

- name: Ensure filebeat is running
  service:
    name: filebeat
    state: started
    enabled: yes
