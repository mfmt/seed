# This postfix instance runs all email through both clamsmtp and spampd via
# post queue filters and then relays either to a mailstore or to the filtered 
# relay queue. We first send the email to clamsmtpd via port 10024, clamsmtp
# sends it to spampd via port 10025, and spampd returns it to postfix via port
# 10026.

- name: install filter specific packages
  apt:
    name:
      - spampd
      - razor
      - clamav-freshclam
      - clamav-daemon
      - clamsmtp
      - git
      - wget
      - postfix-pcre

- name: set proper path to tls cert
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_tls_cert_file='
    line: 'smtpd_tls_cert_file=/etc/ssl/mayfirst/{{ postfix_hostname }}/fullchain.pem'
  notify: restart postfix

- name: set proper path to tls key
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_tls_key_file='
    line: 'smtpd_tls_key_file=/etc/ssl/mayfirst/{{ postfix_hostname }}/privkey.pem'
  notify: restart postfix

# virtual alias domains should include all the domains for which we accept email -
# both forwarded email and email we will eventually delivery ourselves.
- name: ensure virtual domains file exists 
  copy:
    content: ""
    dest: /etc/postfix/virtual-alias-domains
    force: no
  notify: postmap filter databases 

# Virtual alias maps specify email addresses that forward to other email
# addresses. Email addresses that deliver to a user account are omitted from
# this list and included in the transport maps below.  We don't need a
# virtual-alias-domains file because all domains are already listed in
# relay-domains.
- name: ensure virtual-alias-maps file exists
  copy:
    content: ""
    dest: /etc/postfix/virtual-alias-maps
    force: no
  notify: postmap filter databases 

# The transport maps file lists all the email addresses that deliver to exactly
# one user account. For each email address it lists the host server for the
# corresponding user account.
- name: ensure transport maps file exists
  copy:
    content: ""
    dest: /etc/postfix/transport-maps
    force: no
  notify: postmap filter databases 

- name: designate routes and header checks files in main.cf.
  blockinfile:
    path: /etc/postfix/main.cf
    block: |
      virtual_alias_domains = hash:/etc/postfix/virtual-alias-domains
      virtual_alias_maps = hash:/etc/postfix/virtual-alias-maps
      transport_maps = hash:/etc/postfix/transport-maps
  notify: restart postfix

# Any email not included in the transport map should get relayed via our
# filtered email service (these should only be email that fowards to other
# providers).
- name: relay all email to the route relay 
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: relayhost =
    line: relayhost = route.relay.mayfirst.{{ m_tld }}
  notify: restart postfix

- name: configure postfix to filter email
  lineinfile:
    path: /etc/postfix/main.cf
    line: content_filter = scan:127.0.0.1:10024
  notify: restart postfix

- name: build postfix_filter_relay_ips - weborigins
  set_fact:
    postfix_filter_relay_ips: "{{ postfix_filter_relay_ips + [ hostvars[item]['network'][0]['networks'][0]['address'] | regex_replace('/[0-9]+', '') ] }}"
  loop: "{{ groups['weborigin'] }}"

- name: build postfix_filter_relay_ips - mailcf servers
  set_fact:
    postfix_filter_relay_ips: "{{ postfix_filter_relay_ips + [ hostvars[item]['network'][0]['networks'][0]['address'] | regex_replace('/[0-9]+', '') ] }}"
  loop: "{{ groups['postfix'] }}"
  when: 
    - hostvars[item].postfix_profile == 'cf'

- name: set postfix mynetworks parameters
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: mynetworks =
    line: mynetworks = {{ postfix_filter_relay_ips  | join(' ') }}
  notify: restart postfix

# clamsmtp already sends scanned email to 127.0.0.1:10025
# (where spampd is listening).
- name: configure clamsmtp with proper listen port
  lineinfile:
    path: /etc/clamsmtpd.conf
    regexp: "^Listen: 127.0.0.1:"
    line: "Listen: 127.0.0.1:10024"
  notify: 
    # Sometimes clamav-daemon won't start if clamav-freshclam has not
    # downloaded virus definitions so let's restart it here to make sure
    # it has a chance to start after freshclam has been running.
    # See: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=979077
    - ensure freshclam completes
    - restart clamav-daemon
    - restart clamsmtp

- name: set clamsmtp max connections 
  lineinfile:
    path: /etc/clamsmtpd.conf
    regexp: "MaxConnections"
    line: "MaxConnections: {{ clamsmtp_max_connections }}"
  notify: restart clamsmtp

- name: set clamav max threads 
  lineinfile:
    path: /etc/clamav/clamd.conf
    regexp: "MaxThreads"
    line: "MaxThreads {{ clamav_max_threads }}"
  notify: restart clamav-daemon

# Now setup clamav unofficial sigs - to get more protection.
# See: https://github.com/extremeshok/clamav-unofficial-sigs
# See below where we copy a patched version.
- name: git checkout clamav unofficial sigs
  git:
    repo: https://github.com/extremeshok/clamav-unofficial-sigs
    dest: /usr/local/src/clamav-uofficial-sigs
    version: "{{ clamav_unofficial_sigs_version }}"
    update: no
    force: no


# See: https://github.com/extremeshok/clamav-unofficial-sigs/issues/398#issuecomment-951635621
# for why we are using our local copy. Also, consider switching to:
# https://github.com/rseichter/fangfrisch
- name: copy clamav unofficial sigs patched shell script
  copy:
    src: clamav-unofficial-sigs.sh
    dest: /usr/local/src/clamav-uofficial-sigs/clamav-unofficial-sigs.sh

- name: add symlink to clamav unofficial sigs
  file:
    state: link
    path: /usr/local/sbin/clamav-unofficial-sigs.sh
    src: /usr/local/src/clamav-uofficial-sigs/clamav-unofficial-sigs.sh

- name: setup clamav unofficial sigs required directories
  file:
    state: directory
    path: "{{ item }}"
  with_items:
    - /var/log/clamav-unofficial-sigs/
    - /var/lib/clamav-unofficial-sigs
    - /etc/clamav-unofficial-sigs

- name: copy master configuration file
  copy:
    remote_src: true
    src: /usr/local/src/clamav-uofficial-sigs/config/master.conf
    dest: /etc/clamav-unofficial-sigs/master.conf

- name: copy os configuration file
  copy:
    remote_src: true
    src: /usr/local/src/clamav-uofficial-sigs/config/os/os.debian8.systemd.conf
    dest: /etc/clamav-unofficial-sigs/os.conf

- name: add user configuration
  copy:
    content: user_configuration_complete="yes"
    dest: /etc/clamav-unofficial-sigs/user.conf

- name: add systemd files
  copy:
    remote_src: true
    src: /usr/local/src/clamav-uofficial-sigs/systemd/{{ item }}
    dest: /etc/systemd/system/{{ item }}
  with_items:
    - clamav-unofficial-sigs.timer
    - clamav-unofficial-sigs.service
  notify: 
    - reload systemd
    - systemctl enable clamav-unofficial-sigs timer
    - systemctl start clamav-unofficial-sigs timer


# Spampd needs a few tweaks to meet our needs
# Setting envelope from allows SPF checks to work.
- name: configure spampd with additional options
  lineinfile:
    path: /etc/default/spampd
    regex: "^ADDOPTS="
    line: ADDOPTS="--set-envelope-from  --maxsize=2048"
  notify: restart spampd

- name: configure spampd to allow network tests
  lineinfile:
    path: /etc/default/spampd
    regex: "^LOCALONLY="
    line: LOCALONLY=0 
  notify: restart spampd

- name: configure spampd with more children 
  lineinfile:
    path: /etc/default/spampd
    regex: "^CHILDREN"
    line: CHILDREN={{ spampd_children }} 
  notify: restart spampd

# And now, spamassassin changes
- name: Turn on spamassassin cron job for daily updates pre bookworm
  lineinfile:
    path: /etc/default/spamassassin
    regex: CRON=
    line: CRON=1
  when: ansible_distribution_release != "bookworm"

- name: enable spamassassin maintenance timer post bookworm
  service:
    enabled: true
    state: started
    name: spamassassin-maintenance.timer
  when: ansible_distribution_release == "bookworm"

- name: Turn on spamassassin subject line warning
  lineinfile:
    path: /etc/spamassassin/local.cf
    regex: rewrite_header Subject
    line: rewrite_header Subject *****SPAM*****
  notify: restart spampd

- name: Turn off report safe, edit message don't shove into attachment
  lineinfile:
    path: /etc/spamassassin/local.cf
    regex: report_safe 
    line: report_safe 0 
  notify: restart spampd

# bayes filtering is a preformance killer and bug magnet and we don't have
# access to individual settings anyway.
- name: Disable spamassassin bayes filter
  lineinfile:
    path: /etc/spamassassin/local.cf
    regex: "use_bayes " 
    line: use_bayes 0 
  notify: restart spampd

# Note: This short circuits email sent from either internal_networks or
# trusted_networks. We set internal_networks below to the IP addresses
# of our cf servers.
- name: Enable spamassassin short circuit plugin 
  lineinfile:
    path: /etc/spamassassin/v320.pre
    regex: loadplugin Mail::SpamAssassin::Plugin::Shortcircuit 
    line: loadplugin Mail::SpamAssassin::Plugin::Shortcircuit
  notify: restart spampd

- name: Enable trusted networks spamassassin short circuits 
  lineinfile:
    path: /etc/spamassassin/local.cf
    regex: " shortcircuit ALL_TRUSTED" 
    line: shortcircuit ALL_TRUSTED on 
  notify: restart spampd

- name: Add block of custom spamassassin settings
  blockinfile:
    path: /etc/spamassassin/local.cf
    block: |
      internal_networks {{ postfix_sa_internal_networks | join(' ') }}
      # TextCat - language guesser
      loadplugin Mail::SpamAssassin::Plugin::TextCat
      # Set all languages to ok - this will enable the addition of the 
      # X-Language header which will allow individuals to filter as
      # needed.
      ok_languages all
      add_header all Language _LANGUAGES_
      # Ensure spamassassin and spampd are working together. We pass 
      # # --set-envelope-from in /etc/default/spampd. The following
      # # line instructs spamassassin to use this header for checking
      # # spf, etc. 
      envelope_sender_header X-Envelope-From
  notify: restart spampd

- name: checkout spamhaus settings repo
  git:
    repo: https://github.com/spamhaus/spamassassin-dqs
    dest: /usr/local/src/spamassassin-dqs
    version: "{{ spamhaus_config_version }}"

- name: copy spam haus conf files that require updates.
  copy:
    remote_src: true
    src: /usr/local/src/spamassassin-dqs/{{ item }}
    dest: /etc/spamassassin/{{ item }}
    force: no
  with_items:
    - sh.cf
    - sh_hbl.cf
    - sh.pre

- name: add key sh files. 
  command:
    cmd: sed -i "s/your_DQS_key/{{ vault_spamhaus_key }}/g" /etc/spamassassin/{{ item }} 
  with_items:
    - sh.cf
    - sh_hbl.cf
  notify: restart spampd
  when: vault_spamhaus_key is defined

- name: add conf directory to sh.pre file
  lineinfile:
    regexp: loadplugin      Mail::SpamAssassin::Plugin::SH
    line: loadplugin      Mail::SpamAssassin::Plugin::SH /etc/spamassassin/SH.pm
    path: /etc/spamassassin/sh.pre
  notify: restart spampd

- name: copy spam haus conf files that DON'T require updates.
  copy:
    remote_src: true
    src: /usr/local/src/spamassassin-dqs/{{ item }}
    dest: /etc/spamassassin/{{ item }}
  with_items:
    - SH.pm
    - sh_scores.cf
    - sh_hbl_scores.cf
  notify: restart spampd

- name: setup scan and re-injection settings in master.cf
  blockinfile:
    path: /etc/postfix/master.cf
    marker: "# {mark} ANSIBLE MANAGED BLOCK SCAN"
    block: |
      # spampd scan filter (used by content_filter)
      scan      unix  -       -       n       -       16      smtp
        -o smtp_send_xforward_command=yes
        -o smtp_tls_security_level=none
      # For injecting mail back into postfix from the filter
      127.0.0.1:10026 inet  n -       n       -       16      smtpd
        -o content_filter=
        -o receive_override_options=no_unknown_recipient_checks,no_header_body_checks
        -o smtpd_helo_restrictions=
        -o smtpd_client_restrictions=
        -o smtpd_sender_restrictions=
        -o smtpd_recipient_restrictions=permit_mynetworks,reject
        -o mynetworks_style=host
        -o smtpd_authorized_xforward_hosts=127.0.0.0/8
        -o smtpd_tls_security_level=none
  notify: restart postfix


