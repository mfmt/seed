# CF stands for client facing. These servers are the ones you get
# when you go to mail.mayfirst.org. They will both send out going 
# email for you and also allow you to check your IMAP or POP mail.

- name: install submission packages
  apt:
    name:
      - postfwd
      - postfix-pcre
      - sasl2-bin

- name: copy saslauthd default configuration file 
  copy:
    src: saslauthd 
    dest: /etc/default/saslauthd
  notify: restart saslauthd 

- name: copy postfix saslauthd configuration file 
  copy:
    src: sasl-smtpd.conf 
    dest: /etc/postfix/sasl/smtpd.conf
  notify: restart saslauthd 

- name: ensure saslauthd is running
  service:
    name: saslauthd.service
    state: started

- name: ensure postfix is in sasl group
  user:
    name: postfix
    groups: sasl
    append: yes
  notify: restart postfix

# For some reason, the debian postfwd package in buster and in
# bookworm come with a systemd file, but not in bullseye. So
# we have to check for it and add it ourselves if it's missing.
- name: copy postfwd service file (if it's missing)
  copy:
    src: postfwd.service
    dest: /lib/systemd/system/postfwd.service
  notify: reload systemd

- name: set proper path to tls cert
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_tls_cert_file='
    line: 'smtpd_tls_cert_file=/etc/ssl/mayfirst/{{ postfix_hostname }}/fullchain.pem'
  notify: restart postfix

- name: set proper path to tls key
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_tls_key_file='
    line: 'smtpd_tls_key_file=/etc/ssl/mayfirst/{{ postfix_hostname }}/privkey.pem'
  notify: restart postfix

- name: add postfwd configuration file
  template:
    src: postfwd.submission.cf.j2
    dest: /etc/postfix/postfwd.cf
  notify: reload postfwd

- name: ensure postfwd is enabed in /etc/default/postfwd
  lineinfile:
    path: /etc/default/postfwd
    regexp: STARTUP=
    line: STARTUP=1
  notify: restart postfwd

- name: ensure postfwd is running
  service:
    name: postfwd
    state: started 
    enabled: true

# This file removes all Received lines from incoming email to preserve privacy
- name: add header checks file 
  copy:
    src: header-checks.cf.pcre 
    dest: /etc/postfix/header-checks.pcre

# Our name is mail.mayfirst.org, but we don't accept email sent to
# @mail.mayfirst.org.
- name: remove mail.mayfirst.org as destination
  lineinfile:
    regexp: ^mydestination =
    line: mydestination = localhost.localdomain, localhost
    path: /etc/postfix/main.cf

# Only relay email from authenticated users.
- name: restrict who can relay
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: smtpd_relay_restrictions = 
    line: smtpd_relay_restrictions = permit_sasl_authenticated,reject

- name: create over quota mailbox file 
  copy:
    force: no
    dest: /etc/postfix/reject-over-quota-mailboxes
    content: ""
  notify: postmap over quota mailboxes

- name: create sender login map file 
  copy:
    force: no
    dest: /etc/postfix/sender-login-map
    content: ""
  notify: postmap sender login map

- name: add sender login pcre map 
  copy:
    dest: /etc/postfix/sender-login-map.pcre
    content: '/(.*)@mail.mayfirst.org $1'

- name: set sender login map  
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_sender_login_map'
    line: '#smtpd_sender_login_map = pcre:/etc/postfix/sender-login-map.pcre, hash:/etc/postfix/sender-login-map'
  notify: restart postfix

- name: set recipient restrictions  
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_recipient_restrictions='
    line: 'smtpd_recipient_restrictions=check_recipient_access hash:/etc/postfix/reject-over-quota-mailboxes'
  notify: restart postfix

# postfwd has to be invoked after the data command to properly count recipients.
- name: invoke postfwd 
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: smtpd_data_restrictions = 
    line: smtpd_data_restrictions = check_policy_service inet:127.0.0.1:10040

- name: relay all mail to our filter servers. 
  lineinfile:
    path:  /etc/postfix/main.cf
    regexp: relayhost =
    line: relayhost = filter.mayfirst.{{ m_tld }}

- name: set submission specific settings in main.cf
  blockinfile:
    path: /etc/postfix/main.cf
    marker: "# {mark} ANSIBLE MANAGED BLOCK SUBMISSION"
    block: |
      smtpd_sasl_auth_enable = yes
      smtpd_tls_auth_only = yes
      header_checks = pcre:/etc/postfix/header-checks.pcre
  notify: restart postfix

- name: enable submission port for cf
  blockinfile: 
    marker: "# {mark} ANSIBLE MANAGED BLOCK SUBMISSION"
    path: /etc/postfix/master.cf
    block: |
      submission inet n       -       y       -       {{ postfix_submission_process_limit }}       smtpd
        -o syslog_name=postfix/submission
        -o smtpd_tls_security_level=encrypt
        -o smtpd_reject_unlisted_recipient=no
        -o milter_macro_daemon_name=ORIGINATING
      smtps     inet  n       -       y       -       {{ postfix_smtps_process_limit }}       smtpd
        -o syslog_name=postfix/smtps
        -o smtpd_tls_wrappermode=yes
        -o smtpd_reject_unlisted_recipient=no
        -o milter_macro_daemon_name=ORIGINATING 
  notify: restart postfix


