# A library of functions to be tested by the test.py script.
import ldap3
import smtplib
import imaplib
from time import sleep
import os.path
import requests
import email
import json

# Build the body of the message with proper headers and some real
# text in the subject.
def buildEmailBody(mailto, mailfrom, subject, body):
    return "Date: {0}\nFrom: {1}\nTo: {2}\nSubject: {3}\n\n{4}\n{5}\n".format(
            email.utils.formatdate(),
            mailfrom,
            mailto,
            subject,
            "Hi,\nJust confirming the meeting for tomorrow at 5. take care,\nJoe",
            body
    )

class plant():
    emailServer = None
    flowerApiBaseUrl = None
    flowerAdminAuthUser = None
    flowerAdminAuthPassword = None
    flowerUserAuthUsername = 'testuser'
    flowerUserAuthPassword = 'testuserpassword'
    redUser = None 
    redPass = None 
    redUrl = None
    imapConn = None
    imapMessageNum = None

    def redApi(self, obj, action, setProps = {}, whereProps = {}):
        data = {
           "action": action,
           "object": obj,
           "user_name": self.redUser,
           "user_pass": self.redPass,
        }
        for key in setProps:
            data["set:{0}".format(key)] = setProps[key]
        for key in whereProps:
            data["where:{0}".format(key)] = whereProps[key]

        r = requests.post(self.redUrl, data=data)
        try:
            json_result = r.json()
        except json.decoder.JSONDecodeError:
            print("Could not decode red control panel response when processing the following data:")
            print(data)
            print(f"Got result: {r.text}")
            return False
        if json_result['is_error'] != 0:
            print("Red conrol panel returned the following error:")
            print(json_result)
            print("when processing the following data:")
            print(data)
            return False
        key = f"{obj}_id"
        if "values" in json_result:
            return json_result['values'][0][key]
        
    def flowerApiCreate(self, obj=None, params={}, auth=()):
        r = self.flowerApi('api/' + obj + '/', 'post', params, auth)
        # Return the created object.
        return r.json()

    def flowerApi(self, path, method, params, auth):
        url = self.flowerApiBaseUrl + path
        if method == "post":
            r = requests.post(url=url, data=params, auth=auth)
        elif method == "get":
            r = requests.get(url=url, params=params, auth=auth)
        elif method == "delete":
            r = requests.delete(url=url, auth=auth)
        # Raise an exception for 400 or 500 errors
        r.raise_for_status()
        return r


    # Directly submit a message that should be refused (i.e. by postscreen on
    # the first try)
    def emailSubmitRefuse(self, host, mailto, mailfrom, subject, body = None):
        smtp = smtplib.SMTP(host, 25)
        try:
            smtp.ehlo()
            smtp.sendmail(mailfrom, mailto, buildEmailBody(mailto, mailfrom, subject, body))
        except smtplib.SMTPRecipientsRefused:
            return True
        finally:
            smtp.quit()
        return False 

    def emailSubmitAccept(self, host, mailto, mailfrom, subject, body = None):
        smtp = smtplib.SMTP(host, 25)
        try:
            smtp.ehlo()
            smtp.sendmail(mailfrom, mailto, buildEmailBody(mailto, mailfrom, subject, body))
        finally:
            smtp.quit()
        return True

    def emailSend(self, user, password, mailto, mailfrom, subject, body = None, server = 'mail'):
        if server == 'mail':
            server = self.emailServer
        else:
            server = self.emailBulkServer

        smtp = smtplib.SMTP(self.emailServer, 587)
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login(user, password)
        smtp.sendmail(mailfrom, mailto, buildEmailBody(mailto, mailfrom, subject, body))
        smtp.quit()
        return True 

    # Login and return a connection. Return FALSE on failed login.
    def emailImapLogin(self, user, password):
        self.imapConn = imaplib.IMAP4(self.emailServer)
        self.imapConn.starttls()
        self.imapConn.login(user, password)
        self.imapConn.enable('UTF8=ACCEPT')
        return True

    def emailImapSelectMailbox(self):
        selected = self.imapConn.select()
        if selected[0] == 'NO':
                return False
        return True 

    # Find the email with the given subject. Return FALSE on failure
    # or the message ID on success.
    def emailImapFindMessage(self, subject):
        attempt = 0
        while attempt < 3:
            typ, data = self.imapConn.search(None, f'(SUBJECT "{subject}")')
            if typ != 'OK':
                print(f"IMAP search did not return OK when searching for {subject}, it return {typ}")
                return False
            if data[0]:
                for num in data[0].split():
                    self.imapMessageNum = num
                    print("Found the email!")
                    return True
            # Wait 5 more seconds and try again.
            seconds = 5
            print(f"Still looking for email ... will try again after sleeping for {seconds} seconds.")
            sleep(seconds)
            attempt += 1
        return False

    def emailImapMatchHeader(self, header):
        typ, msg_data = self.imapConn.fetch(self.imapMessageNum, '(RFC822)')
        for part in msg_data:
            if isinstance(part, tuple):
                decoded = part[1].decode('utf-8')
                msg = email.message_from_string(decoded)
                key = header[0]
                value = header[1]
                if key in msg:
                    # We do fuzzy search because we only want the beginning
                    # of the spam value header (just "No," in "X-Spam-Status: No,
                    # score=-1.0 required=5.0 etc...")
                    index = msg[key].find(value)
                    if index > -1:
                        return True
        return False

    def emailImapClose(self):
        self.imapConn.close()
        self.imapConn.logout()
                
    def webTestContent(self, url, content):
        r = requests.get(url=url)
        # Raise an exception for 400 or 500 errors
        r.raise_for_status()
        if content in r.text:
            return True
        return False

    def webTestResponse(self, url):
        r = requests.get(url=url)
        # Raise an exception for 400 or 500 errors
        r.raise_for_status()
        return True

    def imapLoginWithBadPassword(self, count):
        # Generate 3 attempts (range is not inclusive)
        for i in range(1, count + 1):
                m = imaplib.IMAP4(self.emailServer)
                m.starttls()
                try:
                    m.login('baduser', 'badpassword')
                except imaplib.IMAP4.error:
                    # I can't seem to find a more specific error for auth failre.
                    pass
                m.logout()

    def fileExists(self, path):
        return os.path.isfile(path)

    def deleteFileIfExists(self, path):
        ret = os.path.isfile(path)
        if ret:
            os.remove(path)

