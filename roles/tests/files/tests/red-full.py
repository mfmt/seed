import unittest
import greenhouse
import os
import random, string
from time import sleep
import imaplib
import requests
import subprocess
import tempfile
import pexpect
from dns import resolver
import slixmpp

class TestXmppLogin(slixmpp.ClientXMPP):
    def __init__(self, jid, password):
        super().__init__(jid, password)

        # Handle session start
        self.add_event_handler("session_start", self.start)

        # Handle failed authentication
        self.add_event_handler("failed_auth", self.failed_auth)

        # To track result
        self.success = False

    def start(self, event):
        self.success = True
        self.disconnect()

    def failed_auth(self, event):
        self.disconnect()

class GreenhouseTestCase(unittest.TestCase):
    # Our top level domain will either be dev in the dev environment or
    # test in the test environment. The main domain will always be either
    # mayfirst.test or mayfirst.dev.
    plant = None
    environment = None
    # List of item ids we have to cleanup
    itemIds = {}
    memberId = None
    uniqueUnixGroupId = None
    hostingOrderId = None
    hostingOrderIdentifier = 'myorg.dev'
    memberFriendlyName = "My Org"
    redAdminUser = 'admin'
    redAdminPass = 'admin23'
    testEmail = "reggie@myorg.dev"
    testFqdn = "myorg.dev"
    testFqdnForDnsARecord = "custom.myorg.dev"
    testCanonicalEmail = "regular_user@mail.mayfirst.dev"
    testUser = "regular_user"
    testPass = "reggie123"
    testAliasFqdn = "www.myorg.dev"
    testPtrFqdn = "myorg.dev"
    testPtrIpv4 = "1.2.3.4"
    testIpv4 = "172.18.0.15"
    testIpv6 = "fc00::1"
    testText = "test-text-123"
    testMxServerName = "mx.mayfirst.dev"
    testMxDist = "10"
    testAliasServerName = "a.webproxy.mayfirst.dev"
    testSrvServerName = "im.mayfirst.dev"
    testSrvFqdn = "_xmpp._tcp.myorg.dev"
    testSrvPort = "5233"
    testSrvWeight = "5"
    testSrvDist = "10"
    testSshfpr = "9b8910e44952157492afafa164dea63c9ae4f6d1b2234cb8342b155ecfb2effd"
    testSshfpAlgorithm = "1"
    testSshfpType = "2"
    testCaaFlags = 0
    testCaaTag = 'issue'
    testCaaValue = 'letsencrypt.org;validationmethods=http-01'
    testDatabaseName = "testo"
    testDatabaseUser = "testo"
    testDatabasePassword = "testotesto1"
    testMysqlPasswordEncrypted = '*AC9316FB9D12D0ED66EA3BC4F86F648E71354F90'
    testPsqlPasswordEncrypted = 'SCRAM-SHA-256$4096:its7rlALcfbveo+uAQTrXQ==$znOitaO+WXzF+6mdWNkmNz47UpKOk9pxLhaqJf9uTBU=:aZv3SzDLtkA2eurVqUN2J/cSit9ol3vZ/Q504uCEWIk='
    testDomain = "myorg.dev"
    testLoginServiceUrl = "https://id.mayfirst.dev/check"
    testNextcloudAppId = "1234"
    testNextcloudUrl = "https://share.mayfirst.dev/"

    def setUp(self):
        # Setup class variables in the test class.
        self.environment = os.environ.get('GREENHOUSE_ENVIRONMENT', 'dev')

        # Setup the plant object we will be using through the tests.
        self.plant = greenhouse.plant()
        self.plant.emailServer = 'mail.mayfirst.' + os.environ.get('GREENHOUSE_TLD', 'dev')
        self.plant.redUrl = "https://members.mayfirst.{0}/api.php".format(os.environ.get('GREENHOUSE_TLD', 'dev'))
        self.plant.redUser = self.redAdminUser
        self.plant.redPass = self.redAdminPass

        self.setupControlPanelItems()

    def tearDown(self):
        self.deleteControlPanelItems()
        return

    def deleteControlPanelItems(self):
        print("Deleting control panel items.")
        # Use the admin user to avoid problems when we disable the active user.
        self.plant.redUser = self.redAdminUser
        self.plant.redPass = self.redAdminPass

        # Delete all items.
        for itemId in reversed(list(self.itemIds.keys())):
            serviceId = self.itemIds[itemId]
            whereProps = {
                "service_id": serviceId,
                "item_id": itemId
            }
            setProps = {}
            self.assertIsNot(self.plant.redApi('item', 'delete', setProps, whereProps), False)

        # Delete the hosting order.
        whereProps = {
            "hosting_order_id": self.hostingOrderId
        }
        setProps = {}
        self.assertIsNot(self.plant.redApi('hosting_order', 'delete', setProps, whereProps), False)
 
        # Delete the member.
        whereProps = {
            "member_id": self.memberId
        }
        setProps = {}
        self.assertIsNot(self.plant.redApi('member', 'delete', setProps, whereProps), False)
        print("Done deleting control panel items.")

    def disableControlPanelItems(self):
        print("Disabling control panel items.")
        # Use the admin user to avoid problems when we disable the active user.
        self.plant.redUser = self.redAdminUser
        self.plant.redPass = self.redAdminPass

        for itemId in reversed(list(self.itemIds.keys())):
            serviceId = self.itemIds[itemId]
            whereProps = {
                "service_id": serviceId,
                "item_id": itemId
            }
            setProps = {}
            self.assertIsNot(self.plant.redApi('item', 'disable', setProps, whereProps), False)
        print("Done disabling control panel items.")

    def setupControlPanelItems(self):
        print("Setting up control panel items.")
        setProps = {
            "unique_unix_group_name": self.testUser
        }
        self.uniqueUnixGroupId = self.plant.redApi('unique_unix_group', 'insert', setProps)
        self.assertIsNot(self.uniqueUnixGroupId, False)

        setProps = {
            "member_friendly_name": self.memberFriendlyName,
            "member_status": "active",
            "member_benefits_level": "standard",
            "member_type": "organization",
            "member_currency": "USD",
            "member_quota": 232 * 1024 * 1024 * 1024,
            "unique_unix_group_id": self.uniqueUnixGroupId,
        }
        self.memberId = self.plant.redApi('member', 'insert', setProps)
        self.assertIsNot(self.memberId, False)

        setProps = {
            "member_id": self.memberId,
            "hosting_order_identifier": self.hostingOrderIdentifier,
            "hosting_order_name": "Initial hosting order",
            "notification_lang": "en_US",
            "unique_unix_group_name": self.testUser
        }
        self.hostingOrderId = self.plant.redApi('hosting_order', 'insert', setProps)
        self.assertIsNot(self.hostingOrderId, False)

        # The hosting order step created several items. We have to look them up
        # and record their item ids so they can be properly deleted and disabled.
        # In addition, the user was setup with a random password. We need to
        # get that item id so we can set a new password.

        # Fetch the user account.
        whereProps = {
            "user_account_login": self.testUser,
            "hosting_order_id": self.hostingOrderId,
            "service_id": 1
        }
        setProps = {}
        itemId = self.plant.redApi('item', 'select', setProps, whereProps)
        self.itemIds[itemId] = 1

        # Set the new password.
        setProps = {
            "user_account_password": self.testPass
        }
        whereProps = {
            "hosting_order_id": self.hostingOrderId,
            "item_id": itemId,
            "service_id": 1
        }
        self.plant.redApi('item', 'update', setProps, whereProps)

        # Fetch the hosting order access record.
        whereProps = {
            "hosting_order_id": self.hostingOrderId,
            "hosting_order_access_login": self.testUser,
            "service_id": 11
        }
        setProps = {}
        itemId = self.plant.redApi('item', 'select', setProps, whereProps)
        self.itemIds[itemId] = 11

        # Fetch the MX record.
        setProps = {}
        whereProps = {
            "hosting_order_id": self.hostingOrderId,
            "dns_type": 'mx',
            "dns_server_name": f'a.{self.testMxServerName}',
            "service_id": 9 
        }
        setProps = {}
        itemId = self.plant.redApi('item', 'select', setProps, whereProps)
        self.itemIds[itemId] = 9 

        # txt record
        setProps = {}
        whereProps = {
            "hosting_order_id": self.hostingOrderId,
            "dns_type": 'txt',
            "service_id": 9 
        }
        setProps = {}
        itemId = self.plant.redApi('item', 'select', setProps, whereProps)
        self.itemIds[itemId] = 9 

        # dkim record
        setProps = {}
        whereProps = {
            "hosting_order_id": self.hostingOrderId,
            "dns_type": 'dkim',
            "service_id": 9 
        }
        setProps = {}
        itemId = self.plant.redApi('item', 'select', setProps, whereProps)
        self.itemIds[itemId] = 9 

        # From now on, create red items using the newly create non admin user
        # and the newly created hosting order id.
        self.plant.redUser = self.testUser
        self.plant.redPass = self.testPass

        # Create all additional DNS records.
        setProps = {
           "service_id": 9,
           "hosting_order_id": self.hostingOrderId,
           "dns_fqdn": self.testFqdnForDnsARecord,
           "dns_type": "a",
           "dns_ip": self.testIpv4,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": self.hostingOrderId,
           "dns_fqdn": self.testFqdnForDnsARecord,
           "dns_type": "aaaa",
           "dns_ip": self.testIpv6,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": self.hostingOrderId,
           "dns_fqdn": self.testPtrFqdn,
           "dns_type": "ptr",
           "dns_ip": self.testPtrIpv4,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": self.hostingOrderId,
           "dns_fqdn": self.testSrvFqdn,
           "dns_type": "srv",
           "dns_server_name": self.testSrvServerName,
           "dns_ttl": "3600",
           "dns_port": self.testSrvPort,
           "dns_weight": self.testSrvWeight,
           "dns_dist": self.testSrvDist,
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": self.hostingOrderId,
           "dns_fqdn": self.testFqdn,
           "dns_type": "sshfp",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": self.testSshfpAlgorithm,
           "dns_sshfp_type": self.testSshfpType,
           "dns_sshfp_fpr": self.testSshfpr,
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": self.hostingOrderId,
           "dns_fqdn": self.testFqdn,
           "dns_type": "caa",
           "dns_ttl": "3600",
           "dns_caa_flags": self.testCaaFlags,
           "dns_caa_tag": self.testCaaTag,
           "dns_caa_value": self.testCaaValue,
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        # Mysql Database
        setProps = {
            "service_id": 20,
            "hosting_order_id": self.hostingOrderId,
            "mysql_db_name": self.testDatabaseName
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 20

        # Mysql Database user
        setProps = {
            "service_id": 21,
            "hosting_order_id": self.hostingOrderId,
            "mysql_user_name": self.testDatabaseUser,
            "mysql_user_db": self.testDatabaseName,
            "mysql_user_password": self.testMysqlPasswordEncrypted,
            "mysql_user_priv": "full"

        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 21

        # Postgres Database
        setProps = {
            "service_id": 39,
            "hosting_order_id": self.hostingOrderId,
            "psql_name": self.testDatabaseName,
            "psql_password": self.testPsqlPasswordEncrypted
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 39

        # Mailbox
        setProps = {
           "service_id": 38,
           "hosting_order_id": self.hostingOrderId,
           "mailbox_login": self.testUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 38

        setProps = {
           "service_id": 2,
           "hosting_order_id": self.hostingOrderId,
           "email_address": self.testEmail,
           "email_address_recipient": "{0}@mail.mayfirst.dev".format(self.testUser)
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 2

        # Web configuration.
        setProps = {
           "service_id": 7,
           "hosting_order_id": self.hostingOrderId,
           "web_conf_domain_names": self.testFqdn,
           "web_conf_tls": 1,
           "web_conf_settings": "ProxyPass /proxy http://localhost:8000/",
           "web_conf_cache_type": "none"
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 7 

        setProps = {
            "service_id": 3,
            "hosting_order_id": self.hostingOrderId,
            "server_access_login": self.testUser,
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 3 
       
        # Scheduled job.
        setProps = {
            "service_id": 24,
            "hosting_order_id": self.hostingOrderId,
            "cron_schedule": "forever",
            "cron_cmd": "php -S localhost:8000"
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 24 

        # Nextcloud 
        setProps = {
            "service_id": 37,
            "hosting_order_id": self.hostingOrderId,
            "nextcloud_login": self.testUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 37

        # Jitsi Meet 
        setProps = {
            "service_id": 40,
            "hosting_order_id": self.hostingOrderId,
            "xmpp_login": self.testUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 40

        # XMPP/Prosody 
        setProps = {
            "service_id": 41,
            "hosting_order_id": self.hostingOrderId,
            "xmpp_login": self.testUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 41

        print("Done setting up control panel items.")

    # Login with the provied username and password, search for the message with the given
    # subject and then ensure the provided headers are set, asserting each step of the way
    # so we can clearly see where we failed.
    def imapLoginAndFindMessage(self, username, password, subject = None, headers = {}):
        try:
            self.assertTrue(self.plant.emailImapLogin(username, password), "Log in with username '{0}'.".format(username))
        except imaplib.IMAP4.error:
            self.assertTrue(False, "Logged in with username '{0}'.".format(username))
            return

        if subject:
            self.assertTrue(self.plant.emailImapSelectMailbox(), "Select mailbox.")
            self.assertTrue(self.plant.emailImapFindMessage(subject), "Find message with subject {0}.".format(subject))
        if headers:
            for header in headers:
                self.assertTrue(self.plant.emailImapMatchHeader(header), "Find message with headers {0}.".format(header))
        self.plant.emailImapClose()

    def ensureMxVerified(self):
        count = 0
        while True:
            print("Checking if MX record is verified.")
            whereProps = {
                "hosting_order_id": self.hostingOrderId,
                "dns_type": 'mx',
                "dns_server_name": f'a.{self.testMxServerName}',
                "service_id": 9,
                "dns_mx_verified": 1,
            }
            setProps = {}
            itemId = self.plant.redApi('item', 'select', setProps, whereProps)
            if itemId:
                print("MX record is verified!")
                return True
            else:
                count += 1
                print("MX not yet verified, sleeping for 5 seconds.")
                sleep(5)
            if count > 12:
                print("Gave up waiting for MX record.")
                return False

    """
    Test to ensure we can create a user with an email box and send ourselves an email
    message that will be properly delivered to our mailbox. This tests multiple components
    together (nsauth and nscache, ldap, postfix, dovecot, etc.)
    """
    def testRed(self):
        verified = self.ensureMxVerified()
        self.assertTrue(verified, "MX domain name verified")
        if not verified:
            return

        username = self.testUser 
        password = self.testPass 
        email_address = self.testEmail 
        email_subject = "Hi Friend: {0}".format(random.random())
        # After domain verification, give a few seconds for the email record to be created.
        sleep(5)

        self.assertTrue(self.plant.emailSend(username, password, email_address, email_address, email_subject), "Send email")
        print("")
        print("Sending to {0}".format(email_address))

        # Try to find the message. And ensure it has headers indicating it has
        # been virus scanned and spam scanned. Note no dkim check because we
        # don't sign internal emails.
        headers = [
                ( "X-Virus-Scanned", "ClamAV using ClamSMTP"),
                ('X-Spam-Status', 'No, score=')
        ]
        self.imapLoginAndFindMessage(username, password, subject = email_subject, headers = headers)

        email_subject = "Hi Friend Canonical: {0}".format(random.random())
        print("")
        print("Sending to {0}".format(self.testCanonicalEmail))
        self.assertTrue(self.plant.emailSend(username, password, self.testCanonicalEmail, email_address, email_subject), "Send canonical email")

        # Try to find the message. 
        self.imapLoginAndFindMessage(username, password, subject = email_subject)

    
        # Test dns.
        dns_tests = [
          ['A', self.testFqdnForDnsARecord, self.testIpv4],
          ['AAAA', self.testFqdnForDnsARecord, self.testIpv6],
          ['SRV', self.testSrvFqdn, f"{self.testSrvDist} {self.testSrvWeight} {self.testSrvPort} {self.testSrvServerName}."],
          ['SSHFP', self.testFqdn, f"{self.testSshfpAlgorithm} {self.testSshfpType} {self.testSshfpr}"],
          ['CAA', self.testFqdn, f'{self.testCaaFlags} {self.testCaaTag} "{self.testCaaValue}"'],
        ]
        for test in dns_tests:
            dns_type = test[0]
            domain = test[1]
            right_answer = test[2]
            try:
                for given_answer in resolver.resolve(domain, dns_type):
                    self.assertEqual(right_answer, given_answer.to_text(), f"Ensure DNS lookup for {domain}/{dns_type} is accurate.")
            except resolver.NoAnswer:
                # Try again.
                for given_answer in resolver.resolve(domain, dns_type):
                    self.assertEqual(right_answer, given_answer.to_text(), f"Ensure DNS lookup for {domain}/{dns_type} is accurate.")

        # Test login service.
        data = {
            'user': self.testUser,
            'password': self.testPass,
            'app_id': self.testNextcloudAppId,
        }
        r = requests.post(self.testLoginServiceUrl, data=data) 
        self.assertEqual(200, r.status_code, "Admin user can login via login service.")

        # Test Nextcloud.
        data = {
            'user': self.testUser,
            'password': self.testPass,
            'app_id': self.testNextcloudAppId,
        }
        url = f"{self.testNextcloudUrl}remote.php/dav/files"
        r = requests.get(url, auth=requests.auth.HTTPBasicAuth(self.testUser, self.testPass)) 
        self.assertEqual(200, r.status_code, "Admin user can login via Nextcloud.")

        # Static web page access.
        try:
            r = requests.get("https://{0}/index.html".format(self.testDomain)) 
        except:
            self.assertTrue(False, "Failed to run requests.get for the URL https://{0}/index.html".format(self.testDomain))
            return
        self.assertEqual(200, r.status_code, "Check status code for the URL https://{0}/index.html".format(self.testDomain))
        self.assertIn("Welcome", r.text, "Check for the word welcome in URL https://{0}/index.html".format(self.testDomain))

        # Test scheduled job that runs a systemd process via php.
        try:
            r = requests.get("https://{0}/proxy".format(self.testDomain)) 
        except:
            self.assertTrue(False, "Failed to run requests.get for the URL https://{0}/proxy".format(self.testDomain))
            return
        self.assertEqual(200, r.status_code, "Check status code for the URL https://{0}/proxy".format(self.testDomain))
        self.assertIn("Welcome", r.text, "Check for word welcome in https://{0}/proxy".format(self.testDomain))

        # Test ability to copy php file to web server and access it.
        # Login via API to trigger access to be granted.
        whereProps = {
            "service_id": 1,
            "hosting_order_id": 1,
            "user_account_login": self.testUser,
        }
        self.plant.redApi('item', 'select', {}, whereProps)

        # Create a simple php script that uses the database.
        # Note: every PHP {} has to be escaped with {{ }}.
        contents = """
        <?php
        try {{
          foreach (["pgsql" => "psql001.mayfirst.cx", "mysql" => "localhost"] as $type => $host) {{
              $conn = new PDO("$type:host=$host;dbname={0}", '{1}', '{2}');
              $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              $sql = "DROP TABLE IF EXISTS test_table";
              $conn->query($sql) || trigger_error("Failed to drop table");
              $sql = "CREATE TABLE test_table (test_field varchar(64))";
              $conn->query($sql) || trigger_error("Failed to create table");
              $sql = "INSERT INTO test_table VALUES('success')";
              $conn->query($sql) || trigger_error("Failed to insert record");
              $sql = "SELECT test_field FROM test_table LIMIT 1";
              foreach ($conn->query($sql) as $row) {{
                if ($row['test_field'] == 'success') {{
                  echo "SUCCESS";
                }}
              }}
            }}
        }} catch(PDOException $e) {{
          echo "Connection failed: " . $e->getMessage();
        }}
        ?>
""".format(self.testDatabaseName, self.testDatabaseUser, self.testDatabasePassword)
        tmp = tempfile.NamedTemporaryFile()

        with open(tmp.name, 'w') as f:
            f.write(contents)

        command = "rsync -e 'ssh -o StrictHostKeyChecking=accept-new' " +  tmp.name + " {0}@shell.mayfirst.dev:web/hi.php".format(self.testUser)
        #print(command)
        child = pexpect.spawn(command, encoding='utf-8')
        #child.logfile = sys.stdout
        child.expect("{0}@shell.mayfirst.dev's password:".format(self.testUser))
        child.sendline(self.testPass)
        child.wait()
        try:
            r = requests.get("https://{0}/hi.php".format(self.testDomain)) 
        except:
            self.assertTrue(False, "Failed to run requests.get for the URL https://{0}/hi.php".format(self.testDomain))
            return
        self.assertEqual(200, r.status_code, "Execute PHP script to find value in database.")
        self.assertIn("SUCCESS", r.text, "SUCCESS found in output of PHP script.")

        # Text XMPP
        xmpp = TestXmppLogin(f"{self.testUser}@mayfirst.dev", self.testPass)
        xmpp.connect()
        xmpp.process(forever=False)
        self.assertTrue(xmpp.success, "Failed to login to XMPP server.")

        # Now disable all items. 
        self.disableControlPanelItems()

