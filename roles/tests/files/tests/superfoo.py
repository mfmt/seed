import unittest
import greenhouse 
import os
import random, string
from time import sleep
import socket
import imaplib

class GreenhouseTestCase(unittest.TestCase):
    # Our top level domain will either be dev in the dev environment or
    # test in the test environment. The main domain will always be either
    # mayfirst.test or mayfirst.dev.
    plant = None
    environment = None 

    def setUp(self):
        # Setup class variables in the test class.
        self.environment = os.environ.get('GREENHOUSE_ENVIRONMENT', 'dev')

        # Setup the plant object we will be using through the tests.
        self.plant = greenhouse.plant()
        self.plant.emailServer = 'mail.mayfirst.' + os.environ.get('GREENHOUSE_TLD', 'dev')

    def tearDown(self):
        return 

    """
    Test to see if https://foo.mayfirst.dev contains the right text. 
    """
    def testSuperFooContent(self):
        self.assertTrue(self.plant.webTestContent('https://foo.mayfirst.dev/', "Welcome to SuperFoo, your amazing web service."))
        
