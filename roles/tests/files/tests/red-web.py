import unittest
import greenhouse 
import os
import sys
from time import sleep
import socket
import imaplib
import requests
import subprocess
import tempfile
import pexpect

class GreenhouseTestCase(unittest.TestCase):
    # Our top level domain will either be dev in the dev environment or
    # test in the test environment. The main domain will always be either
    # mayfirst.test or mayfirst.dev.
    plant = None
    environment = None 
    # List of item ids we have to cleanup
    itemIds = {} 
    redAdminUser = 'admin'
    redAdminPass = 'admin23'
    testDomain = "myorg.dev"
    databaseName = "testo"
    databaseUser = "testo"
    databasePassword = "testotesto1"
    databasePasswordEncrypted = '*AC9316FB9D12D0ED66EA3BC4F86F648E71354F90'

    def setUp(self):
        # Setup class variables in the test class.
        self.environment = os.environ.get('GREENHOUSE_ENVIRONMENT', 'dev')

        # Setup the plant object we will be using through the tests.
        self.plant = greenhouse.plant()
        self.plant.redUrl = "https://members.mayfirst.{0}/api.php".format(os.environ.get('GREENHOUSE_TLD', 'dev'))
        self.plant.redUser = self.redAdminUser
        self.plant.redPass = self.redAdminPass
        self.setupControlPanelItems()

    def tearDown(self):
        self.deleteControlPanelItems()
        return 

    def deleteControlPanelItems(self):
        # Resort so we delete the scheduled job before the web configuration.
        for itemId in reversed(list(self.itemIds.keys())):
            serviceId = self.itemIds[itemId]
            whereProps = {
                "service_id": serviceId,
                "item_id": itemId
            }
            setProps = {}
            self.assertTrue(self.plant.redApi('item', 'delete', setProps, whereProps))


    # Add dns, email address via the control panel API.
    def setupControlPanelItems(self):
        # DNS record.
        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testDomain,
           "dns_type": "a",
           "dns_ttl": "3600",
           "dns_ip": "172.18.0.15",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        # Web configuration.
        setProps = {
           "service_id": 7,
           "hosting_order_id": 1,
           "web_conf_domain_names": self.testDomain,
           "web_conf_tls": 1,
           "web_conf_settings": "ProxyPass /proxy http://localhost:8000/",
           "web_conf_cache_type": "none"
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 7 

        # Shell access.
        setProps = {
            "service_id": 3,
            "hosting_order_id": 1,
            "server_access_login": self.redAdminUser,
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 3 
       
        # Scheduled job.
        setProps = {
            "service_id": 24,
            "hosting_order_id": 1,
            "cron_schedule": "forever",
            "cron_cmd": "php -S localhost:8000",
            "cron_login": self.redAdminUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 24 

        # Database 
        setProps = {
            "service_id": 20,
            "hosting_order_id": 1,
            "mysql_db_name": self.databaseName
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 20 

        # Database user
        setProps = {
            "service_id": 21,
            "hosting_order_id": 1,
            "mysql_user_name": self.databaseUser,
            "mysql_user_db": self.databaseName,
            "mysql_user_password": self.databasePasswordEncrypted,
            "mysql_user_priv": "full"

        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 21 

    """
    All tests together to save on setup/tear down. 
    """
    def testRedWeb(self):
        # Wait for DNS records to finish
        print("Waiting 75 seconds for DNS records to settle.")
        sleep(75)
        # Static web page access.
        try:
            r = requests.get("https://{0}/index.html".format(self.testDomain)) 
        except:
            self.assertTrue(False, "Failed to run requests.get for the URL https://{0}/index.html".format(self.testDomain))
            return
        self.assertEqual(200, r.status_code, "Check status code for the URL https://{0}/index.html".format(self.testDomain))
        self.assertIn("Welcome", r.text, "Check for the word welcome in URL https://{0}/index.html".format(self.testDomain))

        # Test scheduled job that runs a systemd process via php.
        try:
            r = requests.get("https://{0}/proxy".format(self.testDomain)) 
        except:
            self.assertTrue(False, "Failed to run requests.get for the URL https://{0}/proxy".format(self.testDomain))
            return
        self.assertEqual(200, r.status_code, "Check status code for the URL https://{0}/proxy".format(self.testDomain))
        self.assertIn("Welcome", r.text, "Check for word welcome in https://{0}/proxy".format(self.testDomain))

        # Test ability to copy php file to web server and access it.
        # Login via API to trigger access to be granted.
        whereProps = {
            "service_id": 1,
            "hosting_order_id": 1,
            "user_account_login": self.redAdminUser,
        }
        self.plant.redApi('item', 'select', {}, whereProps)

        # Create a simple php script that uses the database.
        # Note: every PHP {} has to be escaped with {{ }}.
        contents = """
        <?php 
        try {{
          $conn = new PDO("mysql:host=localhost;dbname={0}", '{1}', '{2}');
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $sql = "DROP TABLE IF EXISTS `test_table`";
          $conn->query($sql) || trigger_error("Failed to drop table");
          $sql = "CREATE TABLE `test_table` (`test_field` varchar(64))";
          $conn->query($sql) || trigger_error("Failed to create table");
          $sql = "INSERT INTO test_table VALUES('success')";
          $conn->query($sql) || trigger_error("Failed to insert record");
          $sql = "SELECT test_field FROM test_table LIMIT 1";
          foreach ($conn->query($sql) as $row) {{ 
            if ($row['test_field'] == 'success') {{
              echo "SUCCESS";
            }}
          }}
        }} catch(PDOException $e) {{
          echo "Connection failed: " . $e->getMessage();
        }}
        ?>
""".format(self.databaseName, self.databaseUser, self.databasePassword)
        tmp = tempfile.NamedTemporaryFile()

        with open(tmp.name, 'w') as f:
            f.write(contents)

        command = "rsync -e 'ssh -o StrictHostKeyChecking=accept-new' " +  tmp.name + " {0}@shell.mayfirst.dev:web/hi.php".format(self.redAdminUser)
        #print(command)
        child = pexpect.spawn(command, encoding='utf-8')
        #child.logfile = sys.stdout
        child.expect("{0}@shell.mayfirst.dev's password:".format(self.redAdminUser))
        child.sendline(self.redAdminPass)
        child.wait()
        try:
            r = requests.get("https://{0}/hi.php".format(self.testDomain)) 
        except:
            self.assertTrue(False, "Failed to run requests.get for the URL https://{0}/hi.php".format(self.testDomain))
            return
        self.assertEqual(200, r.status_code, "Execute PHP script to find value in database.")
        self.assertIn("SUCCESS", r.text, "SUCCESS found in output of PHP script.")

