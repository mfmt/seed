from mailman.interfaces.member import MemberRole
from mailman.interfaces.usermanager import IUserManager
from mailman.interfaces.member import AlreadySubscribedError
from mailman.interfaces.subscriptions import RequestRecord
from mailman.app.membership import add_member
from zope.component import getUtility

def mayfirst_make_owner(mlist, addr):
    # Ensure we have user and address records.
    name = None
    user_manager = getUtility(IUserManager)
    user_manager.make_user(addr, name)
    try:
        # Now we need string name?
        name = ""
        add_member(mlist, RequestRecord(addr, name), MemberRole.owner)
    except AlreadySubscribedError:
        pass 
