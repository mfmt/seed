# Apply May First default list configuration settings to all new
# lists. This script is execute via withlist:
# mailman-wrapper withlist --run default_list_configuration.py --listspec list@domain": "
from mailman.model.mailinglist import Personalization
from mailman.interfaces.mailinglist import DMARCMitigateAction
from mailman.interfaces.archiver import ArchivePolicy
from mailman.interfaces.action import Action

def mayfirst_default(mlist):
        # Don't display name of list publicly.
        mlist.advertised = False
        # Don't send welcome message when added, or goodbye when leaving.
        mlist.send_welcome_message = False
        mlist.send_goodbye_message = False
        # Full personalization ensures VERP replies for everyone.
        mlist.personalize = Personalization.full 
        # Always convert FROM address if DMARC is detected.
        mlist.dmarc_mitigate_action = DMARCMitigateAction.munge_from
        # Allow bigger attachments (in KB)
        mlist.max_message_size = 5000
        # Make all archives private
        mlist.archive_policy = ArchivePolicy.private
        # Default discard posts by non-members to avoid spam.
        mlist.default_nonmember_action = Action.discard 
