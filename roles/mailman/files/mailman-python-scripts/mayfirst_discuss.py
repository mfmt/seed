# Apply May First default list configuration settings to all new
# lists. This script is execute via withlist:
# mailman-wrapper withlist --run default_list_configuration.py --listspec list@domain": "
from mailman.interfaces.action import Action

def mayfirst_discuss(mlist):
    mlist.allow_list_posts: True 
    mlist.default_member_action: Action.default
