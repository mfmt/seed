---
- name: Install flower hub debian packages
  apt:
    name:
      - nginx
      - python3-publicsuffix 
      - python3-wheel 
      - python3-sqlparse 
      - default-libmysqlclient-dev 
      - python3-dev
      - mariadb-server
      - python3-tabulate
        # provides /usr/share/dict/words for password testing.
      - wamerican-small 

- name: initialize root mysql password
  command:
    cmd: mysqladmin -u root password '{{ vault_flower_database_root_password }}'
    # We don't really create this file, it happens next.
    creates: /root/.my.cnf
  when: vault_flower_database_root_password is defined 

- name: add root .my.cnf file
  template:
    src: my.cnf.j2
    dest: /root/.my.cnf
  when: vault_flower_database_root_password is defined 

# This is idempotent by design, but it will get executed on every run. Bah.
- name: create flower database
  command: mysql -e "CREATE DATABASE IF NOT EXISTS flower"

# Also idempotent.
- name: create flower database user
  command: mysql -e "GRANT ALL ON flower.* to 'flower'@'localhost' IDENTIFIED BY '{{ vault_flower_database_user_password }}'" mysql
  when: vault_flower_database_user_password is defined

- name: pull in mysqlclient library
  command:
    cmd: pip3 install mysqlclient 

- name: create django migrations
  command:
    cmd: python3 manage.py makemigrations
    chdir: /usr/local/share/flower
  environment:
    FLOWER_MODE: production

- name: apply django migrations
  command:
    cmd: python3 manage.py migrate 
    chdir: /usr/local/share/flower
  environment:
    FLOWER_MODE: production

# The safe-createsuper doesn't run if the superuser exists.
- name: create django superuser
  command: 
    cmd: python3 manage.py createadmin --username admin --email {{ flower_admin_email }} --password '{{ vault_flower_admin_password }}'
    chdir: /usr/local/share/flower
  environment:
    FLOWER_MODE: production
  when: vault_flower_admin_password is defined

- name: prepopulate petal and partition records. 
  command: 
    cmd: python3 manage.py prepopulate --tld dev
    chdir: /usr/local/share/flower
  environment:
    FLOWER_MODE: production

- name: Copy nginx logging conf file to ensure it goes to journald
  copy:
    src: journald-logging.conf
    dest: /etc/nginx/conf.d/
  notify: reload nginx

- name: create dhparams file
  command: 
    cmd: openssl dhparam -out /etc/ssl/dhparam.pem 2048
    creates: /etc/ssl/dhparam.pem

- name: Add nginx control panel configuration file
  template:
    src: cp.conf.j2
    dest: /etc/nginx/sites-enabled/cp.conf
  notify: reload nginx

- name: Ensure nginx is started and enabled
  systemd:
    name: nginx
    enabled: true
    state: started

