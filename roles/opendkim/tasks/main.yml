---
# tasks file for opendkim
# An OpenDKIM server can have an opendkim_profile of "generator" - which
# means you generate keys and then sync them with our mail relay servers
# or "relayer" which means you run opendkim and sign keys. Or, both.
#
# Note: we don't technically need opendkim server on the generator
# servers, since they are only generating keys. But we do a full
# sync of the /etc/opendkim directory, so we may as well make sure
# it all works on the generator.
- name: install essential opendkim packages
  apt:
    name:
      - opendkim
      - rsync
      - opendkim-tools

# On the relayers, we use sudo to put all the pieces in place.
- name: install opendkim packages for relayers
  apt:
    name:
      - sudo
  when: "'relayer' in opendkim_profile"

# Simplify opendkim configuration by listening via TCP instead of
# via a socket.
- name: set opendkim to listen via localhost 
  lineinfile:
    path: /etc/opendkim.conf
    regexp: "Socket *inet:"
    line: "Socket inet:8892@localhost" 
  notify: restart opendkim 

- name: set opendkim to NOT listen via socket 
  lineinfile:
    path: /etc/opendkim.conf
    regexp: "Socket\\s*local:"
    line: "#Socket local:/var/run/opendkim/opendkim.sock" 
  notify: restart opendkim 

- name: set opendkim to only sign, not verify
  lineinfile:
    path: /etc/opendkim.conf
    regexp: "Mode "
    line: "Mode                    s"
  notify: restart opendkim

- name: set opendkim to sign the List-Unsubscribe-Post header
  lineinfile:
    path: /etc/opendkim.conf
    regexp: "^SignHeaders:"
    line: "SignHeaders *,+List-Unsubscribe-Post"
  notify: restart opendkim

- name: configure opendkim to consult files for domain configuration
  blockinfile:
    path: /etc/opendkim.conf
    marker: "# {mark} ANSIBLE MANAGED OPENDKIM INCLUDE BLOCK"
    block: |
      KeyTable /etc/opendkim/KeyTable 
      SigningTable /etc/opendkim/SigningTable
      ExternalIgnoreList /etc/opendkim/TrustedHosts
      InternalHosts /etc/opendkim/TrustedHosts
  notify: restart opendkim 

- name: create opendkim config directory for holding keys.
  file:
    state: directory
    path: /etc/opendkim/keys

- name: create opendkim files for holding config details
  copy:
    content: "" 
    dest: /etc/opendkim/{{ item }}
    force: no
  loop:
    - KeyTable
    - SigningTable

- name: configure opendkim TrustedHosts
  template:
    src: TrustedHosts.j2
    dest: /etc/opendkim/TrustedHosts
  notify: restart opendkim

- name: create user for creating and sync'ing opendkim keys
  user:
    name: opendkim-creator 
  when: "'relayer' in opendkim_profile"

- name: create ssh directory 
  file:
    state: directory
    path: /home/opendkim-creator/.ssh
    owner: opendkim-creator
    group: opendkim-creator
  when: "'relayer' in opendkim_profile"

- name: use tofu for ssh access for distributing keys
  copy:
    content: "Host *.mayfirst.{{ m_tld }}\nStrictHostKeyChecking accept-new"
    dest: /root/.ssh/config
  when: opendkim_profile == "generator"

- name: create list of opendkim relayers to rsync to
  template:
    src: opendkim-servers.j2
    dest: /etc/opendkim-servers
  when: "'generator' in opendkim_profile"

- name: allow nsstage servers to ssh in to create opendkim keys
  template:
    src: authorized_keys.j2
    dest: /home/opendkim-creator/.ssh/authorized_keys
  when: "'relayer' in opendkim_profile"

- name: copy dkim helper files into place for generator
  copy:
    src: "{{ item }}"
    dest: /usr/local/bin/
    mode: 0755
  loop:
    - dkim-rsync
    - dkim-generate
  when: "'generator' in opendkim_profile"

- name: copy dkim helper file into place for server
  copy:
    src: dkim-place
    dest: /usr/local/bin/
    mode: 0755
  when: "'relayer' in opendkim_profile"

- name: ensure sudo access by opendkim-creator user
  copy:
    src: opendkim-creator.sudo
    dest: /etc/sudoers.d/opendkim-creator
    mode: 0600
  when: "'relayer' in opendkim_profile"

- name: create ansible tmp directory to avoid ansible warning from become command
  file:
    state: directory
    path: /home/opendkim-creator/.ansible/tmp
    recurse: true
    owner: opendkim-creator
  when: "'relayer' in opendkim_profile"

- name: bootstrap opendkim keys
  command:
    cmd: /usr/local/bin/dkim-generate {{ item }} mf1
  loop: "{{ opendkim_bootstrap }}"
  when: opendkim_bootstrap is defined
