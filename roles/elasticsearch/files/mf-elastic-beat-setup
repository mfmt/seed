#!/usr/bin/python3

# This script is intended to be run after we upgrade elasticsearch to ensure
# the journalbeat and metricbeat indices are properly re-created and also have
# the right lifetime management policy applied (which ensures they are purged
# regularly).

import requests
import os
import sys
import subprocess
import json
from datetime import date
import re

quiet = os.getenv('ELASTIC_QUIET')

def console(msg):
  if not quiet:
    print(msg)

def main():
  # Check for user supplied parameters
  service = os.getenv('ELASTIC_PACKAGE')
  if service == None:
    service = 'metricbeat'
  if service != "metricbeat" and service != "journalbeat":
    console("Please use either metricbeat or journalbeat as the serivce.")
    quit(1)
  service_user = "{0}_writer".format(service)

  days = os.getenv('ELASTIC_DAYS')
  if days == None:
    days = 7 

  host = os.getenv('ELASTIC_HOST')
  if host == None:
    host = "log.mayfirst.org"
  url = "https://{0}".format(host)

  port = os.getenv('ELASTIC_PORT')
  if port == None:
    port = 443 

  # Derive some variables.
  version_cmd = [ service, 'version' ]
  output = subprocess.run(version_cmd, capture_output=True, text=True).stdout.split()
  # Output starts with "service version n.n.n", so get the third part
  version = output[2].strip()
  if not re.match('^[0-9.]*$', version):
    console("I could not determine the version to use.")
    quit(1)

  # And get the elastic user and password
  elastic_user = os.getenv('ELASTIC_USER')
  if elastic_user == None:
    elastic_user = "elastic"
  with open('/root/.elastic.password') as f:
      elastic_password = f.read().strip()

  auth = requests.auth.HTTPBasicAuth(elastic_user, elastic_password)

  #console("service: {0}, days: {1}, version: {2}, host: {3}, elastic_user: {4}, elastic_password: {5}, service_user: {6}".format(service, days, version, host, elastic_user, elastic_password, service_user))

  console("Disabling user {0}...".format(service_user))
  out = requests.put("{0}/_security/user/{1}/_disable".format(url, service_user), auth=auth)
  out.raise_for_status()
  console("Done.\n")

  # As long as we run this command before upgrading metricbeat/journalbeat
  # (i.e. by running right after upgrading the log server but before the
  # others), this should not delete any indexes. However, just in case any were
  # created, we have to remove them or the next step won't work.
  console("Deleting existing index ({0}) to ensure we can properly re-create...".format(service))
  out = requests.delete("{0}/{1}-{2}-*".format(url, service, version), auth=auth)
  out.raise_for_status()
  console("Done.\n")

  console("Recreating index template and policy for {0}...".format(service))
  index_cmd = [ 
    service, 
    'setup', 
    '--index-management', 
    '-E', 'output.logstash.enabled=false', 
    '-E', 'output.elasticsearch.hosts={0}:{1}'.format(host, port), 
    '-E', 'output.elasticsearch.username={0}'.format(elastic_user),
    '-E', 'output.elasticsearch.password={0}'.format(elastic_password),
    '-E', 'output.elasticsearch.protocol=https',
    '-E', 'setup.template.enabled=true',
    '-E', 'setup.ilm.check_exists=true',
    '-E', 'setup.ilm.enabled=true',
    #'-E', 'setup.ilm.overwrite=true'
  ]
  subprocess.run(index_cmd)
  console("Done.\n")

  console("Get the ILM policy")
  out = requests.get("{0}/_ilm/policy/{1}".format(url, service), auth=auth)
  out.raise_for_status()

  # Update
  ilm_policy = out.json()
  # Output: {'metricbeat': {'version': 33, 'modified_date': '2020-11-04T14:19:23.048Z', 'policy': {'phases': {'hot': {'min_age': '0ms', 'actions': {'rollover': {'max_size': '50gb', 'max_age': '30d'}}}}}}}

  # We don't want the entire results when posting back, just the metricbeat
  # values.
  ilm_policy = ilm_policy[service]
  ilm_policy.pop("version")
  ilm_policy.pop("modified_date")

  # Update the way we want it - with warm and delete phases.
  ilm_policy["policy"]["phases"]["hot"]["actions"]["rollover"]["max_age"] = '1d'
  ilm_policy["policy"]["phases"]["warm"] = {
    "min_age": "1d",
    "actions": {
      "set_priority": {
        "priority": None 
      }
    }
  }
  ilm_policy["policy"]["phases"]["delete"] = {
    "min_age": "{0}d".format(days),
    "actions": {
      "delete": {
        "delete_searchable_snapshot": True
      }
    }
  }
  console("Updating ilm policy {0}".format(service))
  out = requests.put("{0}/_ilm/policy/{1}".format(url, service), auth=auth, headers= { "Content-Type": "application/json" }, data=json.dumps(ilm_policy))
  out.raise_for_status()
  console("Done.\n")

  console("Getting template policy for {0}".format(service))
  out = requests.get("{0}/_template/{1}-{2}".format(url, service, version), auth=auth)
  out.raise_for_status()
  console("Done.\n")

  index_template = out.json()
  index_template = index_template.pop("{0}-{1}".format(service, version))

  # We need to set number of replicas to 0 or it will default to 1 and, since
  # we don't have a second elastic search server to provide the replica, the
  # index will remain in the yellow state and will never transition to warm,
  # and then delete phases.
  index_template["settings"]["index"]["number_of_replicas"] = "0"

  console("Updating index template {0}".format(service))
  out = requests.put("{0}/_template/{1}-{2}".format(url, service, version), auth=auth, headers= { "Content-Type": "application/json" }, data=json.dumps(index_template))
  out.raise_for_status()
  console("Done.\n")

  # For some reason, the first index is sometimes created without being linked
  # to the policy.  This step ensures they are linked. And ensures replicas
  # are set to 0 so we don't get in a yellow status.
  console("Add lifecycle policy to the latest index.")
  todays_date = date.today().strftime("%Y.%m.%d")
  index_settings = { 
    "index": {
      "lifecycle": {
        "rollover_alias": "{0}-{1}".format(service, version),
        "name": service
      },
      "number_of_replicas": "0"
    }
  }
  out = requests.put("{0}/{1}-{2}-{3}-000001/_settings".format(url, service, version, todays_date), auth=auth, headers= { "Content-Type": "application/json" }, data=json.dumps(index_settings))
  out.raise_for_status()
  console("Done.\n")


  console("Re-enable the user {0}".format(service_user))
  out = requests.put("{0}/_security/user/{1}/_enable".format(url, service_user), auth=auth)
  out.raise_for_status()
  console("Done.\n")

if __name__ == "__main__":
  try:
      sys.exit(main())
  except ValueError as err:
      print(err)
      sys.exit(1)






