#!/bin/bash

# Ensure local gitlab backup process is run

gitlab-backup create BACKUP=dump GZIP_RSYNCABLE=yes SKIP=tar
