# Install slapd and ensure it is setup with our special ldap schema and it is
# initialized with our custom settings. This role also stores the slapd admin
# password and binddn in /root's home directory.
#
# Openldap has moved to a dynamic configuration model where configuration
# settings are imported into the database rather than being written to a
# configuration file (this allows configuration changes to be made to a running
# server without the need for a reboot). Our problem is that we can't add
# configuration files idempotent(ly). So, we only import on first run.
#
# The presence of /etc/ldap/slapd.d indicates whether slapd has been
# initialized.  The registered variable slapd_initialized is used throughout to
# determine whether or not to import our configuration settings.

- name: check if slapd has been installed and setup yet. 
  stat:
    path: /etc/ldap/slapd.d
  register: slapd_initialized

- name: fail if we are installing slapd and don't have the root password.
  fail:
    msg: slapd is not yet installed and you have not unlocked the ansible vault. 
  when: slapd_initialized.stat.exists == False and vault_slapd_root_password is not defined

# We have to create /etc/ldap first so we can include our debconf
# settings somewhere.
- name: ensure /etc/ldap exists
  file: 
    path: /etc/ldap 
    state: directory

- name: create folder to hold our slapd configuration ldifs. 
  file:
    path: /etc/ldap/mayfirst-config-ldifs
    state: directory

- name: copy our custom schemas and configurations (files)
  copy:
    src: "{{ item }}"
    dest: /etc/ldap/mayfirst-config-ldifs/
  with_fileglob:
    - config-ldifs/*.ldif

- name: copy our custom schemas and configurations (templates)
  template:
    src: config-ldifs/grant-system-users-read-access.ldif.j2 
    dest: /etc/ldap/mayfirst-config-ldifs/grant-system-users-read-access.ldif

- name: store slapd admin password securely on the host.
  copy:
    dest: /root/.slapdpassword
    mode: 0600
    content: "{{ vault_slapd_root_password }}"
  when: vault_slapd_root_password is defined 

# This file means we don't have to specify -D for every command.
- name: store our binddn for the root user
  copy:
    dest: /root/.ldaprc
    content: BINDDN cn=admin,{{ m_ldap_base }}

# Copy over the debconf file with pre-answered slapd debconf answers.
# This approach seems to be the easiest way to get our root password
# configured without an interactive slapd installation.
- name: pre-configure slapd
  template: 
    src: debconf.slapd.selections.j2
    dest: /etc/ldap/debconf.slapd.selections.txt
  when: slapd_initialized.stat.exists == False and vault_slapd_root_password is defined

- name: use debconf to configure slapd prior to installation
  command: /usr/bin/debconf-set-selections /etc/ldap/debconf.slapd.selections.txt
  register: slapd_selections_set
  when: slapd_initialized.stat.exists == False

- name: cleanup /etc/ldap/debconf.slapd.selections.txt now that it's configured
  file: 
    path: /etc/ldap/debconf.slapd.selections.txt
    state: absent

- name: install slapd
  apt:
    install_recommends: no
    name: 
      - slapd
      - ldap-utils

- name: set base domain
  replace:
    path: /etc/ldap/ldap.conf
    regexp: '^#(BASE).*$'
    replace: '\1 {{ m_ldap_base }}' 

- name: ensure we are enabled and running 
  service:
    name: slapd 
    state: started
    enabled: yes

- name: add our organizationalUnits
  shell: >
    printf 'dn: ou={{ item }},{{ m_ldap_base }}\nobjectClass: organizationalUnit\nou: {{ item }}\n' | ldapadd -xy /root/.slapdpassword
  when: slapd_initialized.stat.exists == False
  with_items: 
    - users
    - emailDomains
    - emailAliases
    - systemUsers

# Add our system users
- name: add postfix ldap user 
  shell: >
    printf 'dn: cn=postfix,ou=systemUsers,{{ m_ldap_base }}\nobjectClass: simpleSecurityObject\nobjectClass: organizationalRole\ncn: postfix\nuserPassword: {crypt}{{ vault_postfix_ldap_password_hashed }}' | ldapadd -xy /root/.slapdpassword
  when: slapd_initialized.stat.exists == False and vault_postfix_ldap_password_hashed is defined

- name: add dovecot ldap user 
  shell: >
    printf 'dn: cn=dovecot,ou=systemUsers,{{ m_ldap_base }}\nobjectClass: simpleSecurityObject\nobjectClass: organizationalRole\ncn: dovecot\nuserPassword: {crypt}{{ vault_dovecot_ldap_password_hashed }}' | ldapadd -xy /root/.slapdpassword
  when: slapd_initialized.stat.exists == False and vault_dovecot_ldap_password_hashed is defined

# Import our ldif configuration files. Note: by default debian's slapd only
# allows cn=config changes via SASL authentication (EXTERNAL mechanism) via the
# unix socket by the user with uid 0. So, our syntax is different then when we
# added the organizational units above and we don't need to specify a password
# or BINDDN.
- name: import mayfirst ldif files
  command: ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/mayfirst-config-ldifs/{{ item }}
  when: slapd_initialized.stat.exists == False
  with_items:
    # Add an extra schema to support fields for postfix/dovecot mail delivery. See: https://github.com/ValV/postfix-dovecot-ldap-schema
    - postfix-dovecot.ldif
    # Without these indices, slapd complains
    - add-extra-indices.ldif
    # We don't want to allow anonymous users to bind
    - disable-anon-bind.ldif
    # By default, any user can read the whole directory. We don't want that.
    - disable-global-read.ldif
    # But we do want the postfix and dovecot users to have read access
    - grant-system-users-read-access.ldif
