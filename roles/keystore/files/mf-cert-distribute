#!/usr/bin/python3

# This script is called when a certificate has been renewed. It copies the cert
# and key to all relevant hosts and runs a script on each host to reload any
# services that need to be reloaded.
#
# The script is called by both certbot for letsencrypt certs and also by our
# own mfca scripts.
#
# The script requires a path to the directory containing the certificates.  It
# can be provided either via the environment variable $RENEWED_LINEAGE or as
# the first argument (e.g. /etc/letsencrypt/live/domain.com or
# /etc/ssl/mfca/domain.com)

import yaml
import os
import subprocess 
import sys

base = os.getenv('RENEWED_LINEAGE', default=None)

# Basic sanity checking.
if not base:
    try:
        base = sys.argv[1]
    except IndexError:
        print("Please pass path to the cert directory via the RENEWED_LINEAGE environment variable.")
        exit(1)

if not os.path.isdir(base):
    print("Can't find base directory {0}.".format(base))
    exit(1)

# Strip trailing slash (if any) otherwise basename will return ''
base = base.rstrip("/")
name = os.path.basename(base)

# Define helper functions.
def ssh_command(host, command):
    args = [ "/usr/bin/ssh", "-o", "StrictHostKeyChecking=accept-new", "-l", "keypusher", host ] + command
    return subprocess.run(args)

def copy_file(host, local_path, remote_path):
    args = [ "/usr/bin/rsync", "--copy-links", "-e", "ssh -l keypusher -o StrictHostKeyChecking=accept-new", local_path, host + ":" + remote_path  ]
    return subprocess.run(args)

# Begin main program logic.
with open("/etc/ssl/ansible.certs.yml") as f:
    data = yaml.load(f, Loader=yaml.Loader)

for item in data:
    if name == item["name"]:
        distribute_hosts = item["distribute_hosts"]
        # Check for key
        key_path = base + "/privkey.pem"
        if not os.path.isfile(key_path):
            print("Failed to find key path {0} for {1}".format(key_path, name))
            exit(1) 

        cert_path = base + "/fullchain.pem"
        if not os.path.isfile(cert_path):
            print("Failed to find cert path {0} for {1}".format(key_path, name))
            exit(1) 

        for host in distribute_hosts:
            # Ensure target directory exists
            proc = ssh_command(host, ["mkdir", "-p", name])
            if proc.returncode != 0:
                print("Failed to mkdir on {0} with name {1}".format(host, name))
                exit(1)

            # Copy both files.
            proc = copy_file(host, key_path, name + "/privkey.pem")
            if proc.returncode != 0:
                print("Failed to copy file {0} on {1}".format(key_path, host))
                exit(1)

            proc = copy_file(host, cert_path, name + "/fullchain.pem")
            if proc.returncode != 0:
                print("Failed to copy file {0} on {1}".format(cert_path, host))
                exit(1)
  
            # Run renew hooks
            for renew_hook in distribute_hosts[host]:
                # We modify the hook - prepend sudo and append the name of the cert.
                cmd = ["/usr/bin/sudo", renew_hook, name]
                proc = ssh_command(host, cmd)
                if proc.returncode != 0:
                    print("Failed to run renew command {0} on {1} for {2}".format(renew_hook, host, name))
                    exit(1)
