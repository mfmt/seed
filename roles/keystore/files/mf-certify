#!/usr/bin/python3

# This script generate x509 certificates using the May First
# certificate authority.  It's like the "certbot" command except it
# uses the May First certificate authority instead of Lets Encrypt.
# Also, it handles distribution of certificates to the appropriate
# hosts (whereas certbot uses the renewal-hook to do that task.
#
# The first argument is the name that will be used to save the key,
# csr and cert (e.g. site1234). All remaining arguments will be
# considered the common names for the cert (i.e. the domain names).
# 
# mf-certify site1234 foo.org www.foo.org
#

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.x509.oid import ExtensionOID
import datetime
import shutil
import os
import sys
import subprocess

def get_contents(path):
    with open(path, "rb") as f:
      return f.read()

# Helper script to return our signer info.
def get_mayfirst_x509_name():
    return x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"US"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"New York"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Brooklyn"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"May First Movement Technology"),
    ])

# Ensure the given csr includes all the domain names passed in via
# expected_names (and does not have any extras).
def csr_alt_names_match(csr_path, expected_names):
    # Load csr object.
    csr = x509.load_pem_x509_csr(get_contents(csr_path), default_backend())

    # Extract the common names from the CSR.
    ext = csr.extensions.get_extension_for_oid(ExtensionOID.SUBJECT_ALTERNATIVE_NAME)
    existing_names = []
    for domain in ext.value.get_values_for_type(x509.DNSName):
        existing_names.append(domain)
    if expected_names.sort() == existing_names.sort():
        return True
    return False

# Check if we should renew a cert.
def cert_expires_before(crt_path, time):
    crt = x509.load_pem_x509_certificate(get_contents(crt_path), backend=default_backend())
    if crt.not_valid_after < time:
        return True
    return False

def generate_rsa_key(path):
    # Generate key.
    key = rsa.generate_private_key(
      public_exponent=65537,
      key_size=2048,
      backend=default_backend()
    )
    # Write to disk. 
    with open(path, "wb") as f:
      f.write(key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption(),
    ))

def generate_csr(key_path, common_names, out_path):
    subject_alt_names = []

    # Load string key as object.
    key = serialization.load_pem_private_key(
      get_contents(key_path),
      password=None,
      backend=default_backend()
    ) 

    # Convert common names to format used below.
    for common_name in common_names:
        subject_alt_names.append(x509.DNSName(common_name))

    name = get_mayfirst_x509_name()

    csr = x509.CertificateSigningRequestBuilder().subject_name(name).add_extension(
        x509.SubjectAlternativeName(subject_alt_names),
        critical=False,
    ).sign(key, hashes.SHA256(), default_backend())

    # Write our CSR out to disk.
    with open(out_path, "wb") as f:
        f.write(csr.public_bytes(serialization.Encoding.PEM))

def generate_cert(cakey_path, csr_path, out_path):
    issuer = get_mayfirst_x509_name() 
    
    # Load csr object.
    csr = x509.load_pem_x509_csr(get_contents(csr_path), default_backend())

    # Extract the common names from the CSR.
    ext = csr.extensions.get_extension_for_oid(ExtensionOID.SUBJECT_ALTERNATIVE_NAME)
    subject_alt_names = []
    subject_domain = None
    for domain in ext.value.get_values_for_type(x509.DNSName):
        # Use the first common name as the subject
        if subject_domain == None:
            subject_domain = domain
        subject_alt_names.append(x509.DNSName(domain))

    # Add the certificate subject (just the domain name)
    subject =  x509.Name([
        x509.NameAttribute(NameOID.COMMON_NAME, subject_domain),
    ])

    # Load cakey as object
    cakey = serialization.load_pem_private_key(
      get_contents(cakey_path),
      password=None,
      backend=default_backend()
    )

    cert = x509.CertificateBuilder().subject_name(
        subject
    ).issuer_name(
        issuer
    ).public_key(
        csr.public_key()
    ).serial_number(
        x509.random_serial_number()
    ).not_valid_before(
        datetime.datetime.utcnow()
    ).not_valid_after(
        # Our certificate will be valid for 3 months
        datetime.datetime.utcnow() + datetime.timedelta(weeks=12)
    ).add_extension(
        x509.SubjectAlternativeName(subject_alt_names),
        critical=False,
    ).sign(cakey, hashes.SHA256(), default_backend())

    # Write our certificate out to disk.
    with open(out_path, "wb") as f:
        f.write(cert.public_bytes(serialization.Encoding.PEM))

def distribute_cert(cert_path, config_file):
    path = os.path.dirname(cert_path)
    args = [
        "/usr/local/sbin/mf-cert-distribute",
    ]
    if config_file:
        args.append("--config")
        args.append(config_file)
    args.append(path)
    subprocess.run(args)

# Begin program logic.
try:
    config_file = None 
    if sys.argv[1] == "--config":
        config_file = sys.argv[2]
        name = sys.argv[3]
        common_names = sys.argv[4:]
    else:
        name = sys.argv[1]
        common_names = sys.argv[2:] 
except IndexError:
    print("Not enough arguments: mf-certify [--config /path/to/config.yml] <name> <domain1> [<domain2> ...]")
    sys.exit(1)

base = "/etc/ssl/mfca/" + name
cakey_path = "/etc/ssl/private/mfca.key"

# Check for directory
if not os.path.isdir(base):
    os.mkdir(base)

# Check for key
key_path = base + "/privkey.pem"
if not os.path.isfile(key_path):
    # Generate Key
    print("Generating key {0}".format(key_path))
    generate_rsa_key(key_path)

# Check for csr
csr_path = base + "/csr.pem"
overwrite_cert = False
if not os.path.isfile(csr_path):
    # Generate CSR
    print("Generating csr {0}".format(csr_path))
    generate_csr(key_path, common_names, csr_path)
    overwrite_cert = True
else:
    # Confirm csr contains correct common names
    if not csr_alt_names_match(csr_path, common_names):
        # Regenerate csr
        print("Regenerating csr {0}".format(csr_path))
        generate_csr(key, common_names, csr_path)
        overwrite_cert = True

# Check for certificate
cert_path = base + "/fullchain.pem"
if not os.path.isfile(cert_path):
    # Create certificate
    print("Generating cert {0}".format(cert_path))
    generate_cert(cakey_path, csr_path, cert_path)
    distribute_cert(cert_path, config_file)
else:
    # Regenerate if expiration is less than 1 month away
    compare_date = datetime.datetime.utcnow() + datetime.timedelta(weeks=4)
    if cert_expires_before(cert_path, compare_date) or overwrite_cert:
        print("Regenerating cert {0}".format(cert_path))
        generate_cert(cakey_path, csr_path, cert_path)
        distribute_cert(cert_path, config_file)


