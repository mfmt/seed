- name: install keystore packages
  apt:
    name:
      - openssl
      - python3-yaml
      - python3-cryptography
      - certbot
      - nginx

- name: copy mfca key 
  copy:
    dest: /etc/ssl/private/mfca.key
    content: "{{ vault_mfca_key }}"
    mode: 0600
  when: vault_mfca_key is defined

# This is not defined in dev or testing, because in dev/testing all servers get
# the same public ssh key (copied over in the mayfirst role). But in
# production, this key should be defined in the ansible vault and should have a
# corresponding public key defined in the hosts.yml file.
- name: copy root ssh private key
  copy:
    dest: /root/.ssh/id_rsa
    content: "{{ vault_keystore_ssh_private_key }}"
    mode: 0600
  when: vault_keystore_ssh_private_key is defined

- name: create mfca directory
  file:
    state: directory
    path: /etc/ssl/mfca/
    mode: 0600

# These directories are created automatically by certbot when the
# first certificate is created, but we need it to ensure that
# our mf-cert-process command doesn't fail if we run it before
# certbot does it's work.
- name: create letsencrypt live directory
  file:
    state: directory
    path: /etc/letsencrypt/{{ item }}
    mode: 0700
  loop:
    - live
    - renewal-hooks
    - renewal-hooks/deploy

- name: copy script that processes all certs
  copy:
    src: mf-cert-process
    dest: /usr/local/sbin/mf-cert-process
    mode: 0755

- name: copy script that distributes certs and keys
  copy:
    src: mf-cert-distribute
    dest: /usr/local/sbin/mf-cert-distribute
    mode: 0755

# With this link in place, we don't have to mess with any
# hook specifications.
- name: add symlink to mf-cert-distribute for letsencrypt
  file:
    state: link
    src: /usr/local/sbin/mf-cert-distribute
    dest: /etc/letsencrypt/renewal-hooks/deploy/mf-cert-distribute

- name: create mfca configuration file 
  template:
    src: ansible.certs.yml.j2
    dest: /etc/ssl/ansible.certs.yml
  notify: process certs

# We have to run mf-cert-process on a timer to ensure our certs
# get renewed.
- name: copy systemd files for mf-cert-process
  copy:
    src: "{{ item }}"
    dest: /etc/systemd/system/{{ item }}
  with_items:
    - mf-cert-process.service
    - mf-cert-process.timer
  notify: 
    - reload systemd
    - enable mf-cert-process timer
    - start mf-cert-process timer
