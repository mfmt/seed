FROM my-buster:latest 

# Install packages
RUN apt-get update && \
    export DEBIAN_FRONTEND=noninteractive && \ 
    apt-get install -y \
    --no-install-recommends \
    redis-server \
    redis-tools && \
    rm -rf /var/lib/apt/lists/*

# Ensure we don't daemonize - we want to keep running.
RUN sed -i 's/daemonize yes/daemonize no/' /etc/redis/redis.conf

# Listen all all IPs so nextcloud can connect.
RUN sed -i 's/^bind 127.0.0.1 ::1/# bind 127.0.0.1 ::1/' /etc/redis/redis.conf

# Turn off protected mode - we are only listening on a private IP range, so it's safe
RUN sed -i 's/^protected-mode yes/protected-mode no/' /etc/redis/redis.conf

VOLUME ["/data"]
CMD ["redis-server", "/etc/redis/redis.conf"]
EXPOSE 6379
