#!/bin/bash
set -e

# Update the php version when we upgrade docker base image to the next Debian
# version.
php_version=7.3

# We should inherit the NEXTCLOUD_VERSION environment variable from the Docker
# file. The env file sourced below brings in the UID variable and the various
# PHP variables.
source "/var/www/nextcloud/${NEXTCLOUD_VERSION}/config/env"


# Update www-data user to use our UID. This ensures all nextcloud hosts operate
# under separate permissions.
usermod --uid "$PHP_UID" www-data
groupmod --gid "$PHP_UID" www-data

# Create the pool file.
printf "
[www]
user = www-data 
group = www-data 
listen = 9000 
listen.owner = www-data 
listen.group = www-data 
pm = dynamic
pm.max_children = ${PHP_MAX_CHILDREN} 
pm.start_servers = ${PHP_START_SERVERS} 
pm.min_spare_servers = ${PHP_MIN_SPARE_SERVERS} 
pm.max_spare_servers = ${PHP_MAX_SPARE_SERVERS} 
env[HOSTNAME] = \$HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
" > /etc/php/${php_version}/fpm/pool.d/www.conf

# If the command passed to docker run/exec starts with a - or -- then
# set the command.
if [ "${1:0:1}" = '-' ]; then
  set -- php-fpm${php_version} "$@"
fi

if [ "$1" = "php-fpm${php_version}" ]; then
  mkdir -p /run/php
  set -- "$@" --nodaemonize --fpm-config /etc/php/${php_version}/fpm/php-fpm.conf
fi

exec "$@"








