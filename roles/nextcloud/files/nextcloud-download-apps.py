#!/usr/bin/python3

# Thanks autistici

import argparse
import requests
import subprocess
from packaging import version


def _latest_version(l):
    return sorted(l, key=lambda x: version.parse(x['version']), reverse=True)[0]


def get_latest_nextcloud_release():
    versions = requests.get('https://apps.nextcloud.com/api/v1/platforms.json').json()
    versions = [x for x in versions if x['hasRelease'] and x['isSupported']]
    return _latest_version(versions)['version']


def get_nextcloud_apps(release):
    return requests.get(f'https://apps.nextcloud.com/api/v1/platform/{release}/apps.json').json()


def find_app(apps, name):
    for app in apps:
        if app['id'] == name:
            return app
    return None


def get_app_download_url(apps, name):
    app = find_app(apps, name)
    if not app:
        raise Exception(f'Could not find app {name}')
    app_release = _latest_version(app['releases'])
    return app_release['download']


def download_and_decompress(url, target):
    print(f'downloading {url}')
    if url.endswith('.gz') or url.endswith('.tgz'):
        dec_cmd = 'gunzip -c'
    elif url.endswith('.bz2'):
        dec_cmd = 'bunzip2 -c'
    else:
        raise Exception('unknown compression format')
    subprocess.check_call(
        f"curl -sL '{url}' | {dec_cmd} | tar -C '{target}' -x -f-",
        shell=True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--version',
                        help='Nextcloud version (default: latest)')
    parser.add_argument('apps', metavar='APP', nargs='+')
    args = parser.parse_args()

    version = args.version
    if not version:
        version = get_latest_nextcloud_release()

    target = f'/usr/local/src/nextcloud/{version}/apps'
    print(f'fetching app list for {version}')
    apps = get_nextcloud_apps(version)
    for app_name in args.apps:
        dl_url = get_app_download_url(apps, app_name)
        download_and_decompress(dl_url, target)


if __name__ == '__main__':
    main()

