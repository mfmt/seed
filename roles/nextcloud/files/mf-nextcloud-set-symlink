#!/bin/bash -e

# Add or update symlink from a nextcloud web directory to the nextcloud src
# directory for this user, in other words: /var/www/nextcloud/$user ->
# /usr/local/src/nextcloud/$version-$user
#
# By using symlinks we can immediately switch to a new version (or switch
# back).

user="$1"

if [ -z "$user" ]; then
  printf "Pass the nextcloud user as the first argument.\n"
  exit 1
fi

data_path="/srv/nextcloud/$user"
web_path="/var/www/nextcloud/$user"

env="${data_path}/config/env"
if [ ! -f "$env" ]; then
  printf "No env file. Have you run ansible?\n"
  exit 1
fi

version=$(grep ^VERSION "${data_path}/config/env" | cut -d= -f2)
if [ -z "$version" ]; then
  printf "Could not determination nextcloud version from env file.\n"
  exit 1
fi

src_path="/usr/local/src/nextcloud/${version}-${user}"
if [ ! -d "$src_path" ]; then
  printf "The source path (%s) has not been created. Be sure to run ansible first.\n" "$src_path"
  exit 1
fi

# On new installs, there is no data directory so we have to create it.
if [ ! -e "${src_path}/data" ]; then
  ln -s "${data_path}/data" "$src_path/data"
fi

# And we have to delete the config directory and replace with a symlink.
if [ ! -L "${src_path}/config" ]; then
  rm -rf "${src_path}/config"
  ln -s "${data_path}/config" "${src_path}/config"
fi

if [ -s "$web_path" ]; then
  points_to=$(readlink "$web_path")
  if [ "$points_to" = "$src_path" ]; then
    printf "%s is already pointing to version %s.\n" "$user" "$version"
    exit 0
  fi
  # We are updating the symlink, first remove it.
  printf "Upgrading..."
  rm "$web_path"
elif [ -e "$web_path" ]; then
  printf "Target exists and is not symlink: %s\n" "$web_path"
  exit 1
fi
# Now we finally create the symlink.
ln -s "$src_path" "$web_path"
printf "%s is now pointing to %s\n" "$user" "$src_path"


