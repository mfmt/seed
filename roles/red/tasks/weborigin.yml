- name: Install general packages needed on weborigin red servers.
  apt:
    name:
      - bzip2
      - composer
      - curl
      - diffstat
      - gcc
      - imagemagick
      - libjs-jquery
      - python3
      - python3-pip
      - python3-venv
      - pkg-config
      - default-libmysqlclient-dev
      - unzip
      - emacs
      - zip
      - sqlite3
      - npm
      - nodejs 
      - yarnpkg
      - ruby
      - rbenv
      # Needed to compile ruby gems
      - ruby-dev
      # Postgres dev needed for building gems (decidim).
      - libpq-dev
      # Libicu-dev needed for building gems (decidim)
      - libicu-dev
      - rake
      - bundler
      - yarnpkg
      - quota
      # Needed for systemctl --user scripts to linger
      - dbus-user-session
      - wget
      - postgresql-client
      - memcached
      # utitlities for monitoring and debugging
      - ncdu
      - goaccess
      - strace
      - dnsutils
      # member requested packages
      - zsh

- name: add web-mover user
  user:
    name: web-mover
    system: yes 
    home: /var/lib/web-mover

- name: add web-mover .ssh directory
  file:
    state: directory
    owner: web-mover
    path: /var/lib/web-mover/.ssh

- name: add web-rsyncer user
  user:
    name: web-rsyncer
    system: yes 
    home: /var/lib/web-rsyncer

- name: add web-rsyncer .ssh directory
  file:
    state: directory
    owner: web-rsyncer
    path: /var/lib/web-rsyncer/.ssh

- name: add sudo configuration file for web-mover
  copy:
    src: web-mover.sudo
    dest: /etc/sudoers.d/web-mover
    mode: 0600

- name: add sudo configuration file for web-rsyncer
  copy:
    src: web-rsyncer.sudo
    dest: /etc/sudoers.d/web-rsyncer
    mode: 0600

# Home directories are not writable by the user, this allows people
# to use .config/zsh for zsh config files.
- name: set default zsh directory
  lineinfile:
    path: /etc/zsh/zshenv
    regexp: export ZDOTDIR
    line: export ZDOTDIR="$HOME/.config/zsh/"

# memcached is only installed so users can run their ow instance via
# their own socket. We don't want anyone to accidentally use the
# globally available one
- name: stop and disable memcached
  service:
    name: memcached
    state: stopped
    enabled: no 

# I'm not sure why this is required.
- name: ensure /run/php exists
  file:
    path: /run/php
    state: directory
    owner: www-data
    group: www-data

- name: Set php versions
  set_fact:
    php_versions:
      - php7.0
      - php7.1
      - php7.2
      - php7.3
      - php7.4
      - php8.0
      - php8.1
      - php8.2
      - php8.3

- name: Set php packages 
  set_fact:
    php_packages:
      - apcu
      - bcmath
      - bz2
      - cli
      - curl
      - fpm
      - imagick
      - imap
      - intl
      - gd
      - gmp
      - mbstring
      - memcached
      - mcrypt
      - mysql
      - opcache
      - phpdbg
      - pgsql
      - readline
      - soap
      - sqlite3
      - xml
      - xmlrpc
      - yaml
      - zip

- name: create php version package list
  set_fact:
    php_versioned_packages: "{{ php_versioned_packages|default([]) + [ item[0] ~ '-' ~ item[1] ]}}"
  loop: "{{ php_versions | product(php_packages) }}"
  # I don't know why mcrypt is not available on 8.3, perhaps deprecated.
  when: not (item[0] == "php8.3" and item[1] == "mcrypt")

- name: add sury apt gpg key
  get_url:
    dest: /etc/apt/trusted.gpg.d/sury.gpg
    url: https://packages.sury.org/apt.gpg 

- name: add sury repo
  copy:
    dest: /etc/apt/sources.list.d/sury.list
    content: deb [signed-by=/etc/apt/trusted.gpg.d/sury.gpg] https://packages.sury.org/php/ {{ ansible_distribution_release }} main
  notify: refresh apt cache

- name: Install php packages needed on weborigin red servers.
  apt:
    name: "{{ php_versioned_packages }}" 
    update_cache: True

- name: Install generic php packages or packages in some versions but not others.
  apt:
    name:
      - php7.0-json
      - php7.1-json
      - php7.2-json
      - php7.3-json
      - php-getid3
      - php-pear

- name: ensure drupal 7 directory exists
  file:
    state: directory
    path: /usr/local/share/drupal-7

# Note: drush8 is handled in drush8 role.
- name: download drupal 7 
  unarchive:
    src: https://ftp.drupal.org/files/projects/drupal-7.102.tar.gz
    dest: /usr/local/share/drupal-7
    remote_src: yes
    extra_opts:
      - --strip-components=1

- name: make authkeys directory
  file:
    path: /var/lib/red/authkeys
    state: directory
    recurse: true

- name: configure ssh authorized keys file
  copy:
    src: sshd-authorized-keys.conf
    dest: /etc/ssh/sshd_config.d/sshd-authorized-keys.conf
  notify: reload ssh

- name: install script to rebuild authorized keys
  copy:
    src: mf-ssh-rebuild-authorized-keys
    dest: /usr/local/sbin/
    mode: 0755

- name: install script scan all web sites 
  copy:
    src: web_scan.py 
    dest: /usr/local/sbin/mf-web-scan
    mode: 0755

- name: install script scan single web site
  copy:
    src: web_scan_site.py 
    dest: /usr/local/bin/mf-web-scan-site
    mode: 0755

- name: copy rebuild authorized keys systemd files
  copy:
    src: systemd/{{ item }}
    dest: /etc/systemd/system/
  loop:
    - rebuild-authorized-keys.service
    - rebuild-authorized-keys.timer
  notify:
    - reload systemd

- name: set rebuild authorized keys service
  service:
    name: rebuild-authorized-keys.timer
    enabled: yes
    state: started

- name: copy script to handle user systemd service
  copy:
    src: mf-systemctl
    dest: /usr/local/sbin/mf-systemctl
    mode: 0755

- name: collect list of php etc directories
  command: ls /etc/php/
  register: php_etc_dirs

- name: add mayfirst fpm php.ini file
  copy:
    src: 99-mayfirst-fpm.ini
    dest: /etc/php/{{ item }}/fpm/conf.d/99-mayfirst.ini
  with_items: "{{  php_etc_dirs.stdout_lines }}"

- name: add mayfirst cli php.ini file
  copy:
    src: 99-mayfirst-cli.ini
    dest: /etc/php/{{ item }}/cli/conf.d/99-mayfirst.ini
  with_items: "{{  php_etc_dirs.stdout_lines }}"

- name: copy helpful utilities
  copy:
    src: "{{ item }}"
    mode: 0755
    dest: /usr/local/sbin/{{ item }}
  with_items:
    - mf-php-processes
    - mf-top-cpu-users-now

- name: add log rotate file for apache log files
  copy:
    src: logrotate.red
    dest: /etc/logrotate.d/red

- name: limit users RAM usage to avoid a run on system memory
  copy:
    content: "@users soft as {{ red_ulimit_ram }}\n@users hard as {{ red_ulimit_ram }}\n"
    dest: /etc/security/limits.d/users.conf

# Always get the latest
- name: install wp
  get_url:
    url: https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    mode: 0755
    dest: /usr/local/bin/wp
  tags: red-wp
