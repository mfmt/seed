#!/bin/bash

# /var/lib/red/authorize contains a directory for every site that is currently
# authorized to allow ssh connections. In the directory is a file named for
# every shell user that is allowed to ssh into that site.
#
# Find all shell user files in /var/lib/red/authorize with a time stamp in the
# future. These files represent the users who should have access. If the time
# stamp is in the past, it means their access has expired and their ssh access
# should be removed until they login to the control panel again.

# Optionally, limit to just one site. If empty, run on all sites.
limit_to_site="$1"

if [ -z "$MF_SHELL_SERVER" ]; then
  # Set default shell server. If in a docker environment, we assume
  # we are in a dev environment. Otherwise it's a live environment.
  if [ -f /.dockerenv ]; then
    MF_SHELL_SERVER=https://shell.mayfirst.dev/
  else
    MF_SHELL_SERVER=https://shell.mayfirst.cx/
  fi
fi
red_authorized_dir=/var/lib/red/authorize

# Build new authorized_keys files here. Later move them into
# /var/lib/red/authkeys so sshd will allow access.
temp_authorized_keys=$(mktemp -d)

# We only find files with time stamps in the future. Create
# a files with a timestamp of now to compare with.
time_compare=$(mktemp)

cleanup () {
  local err="$1"
  rm "$time_compare"
  rm -rf "$temp_authorized_keys"
  if [ -n "$err" ]; then
    printf "%s\n" "$err"
    exit 1
  fi
}

# Delete all old access files
find "$red_authorized_dir"  -type f ! -newer "$time_compare" -delete

# Delete any empty directories left over
find "$red_authorized_dir" -mindepth 1 -type d -empty -delete

# Download latest keys for the remaining ones.
for line in $(find "$red_authorized_dir"  -type f | cut -d/ -f6,7); do
  site=$(echo "$line" | cut -d/ -f1)
  user=$(echo "$line" | cut -d/ -f2)
  if [ -n "$limit_to_site" ] && [ "$site" != "$limit_to_site" ]; then
    continue
  fi
  curl --silent --show-error "${MF_SHELL_SERVER}/${user}" >> "${temp_authorized_keys}/$site" || cleanup "Failed to download $user key from $MF_SHELL_SERVER."
done

for site in ls /home/sites/*; do
  site="$(basename "$site")"
  if [ -n "$limit_to_site" ] && [ "$site" != "$limit_to_site" ]; then
    continue
  fi
  # This is the location of authorized_keys as configured in
  # /etc/ssh/sshd_conf.d/sshd-authorized-keys.conf
  authorized_keys="/var/lib/red/authkeys/site${site}writer"
  if [ -f "${temp_authorized_keys}/$site" ]; then
    mv "${temp_authorized_keys}/$site" "$authorized_keys"
  elif [ -f "$authorized_keys" ]; then
    truncate --size 0 "$authorized_keys"
  fi
done
cleanup

