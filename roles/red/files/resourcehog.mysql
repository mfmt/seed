#!/bin/bash

savedir="/var/log/resourcehog"
day=""

getusage(){
  rc="${1:-0}"
  printf "  Usage:\n"
  printf "  %s [--rotate] [-n]\n\n" "$0"
  printf "  --rotate: save current stats to /var/log/resourcehog, reset current stats to 0.\n"
  printf "    If you want to see previous stats, use -n argument.\n"
  printf "  -n: display results from n days back.\n";
  exit "$rc" 
}

getstats() {
  if [ -n "$1" ]; then
    # Get saved stats from a previous day.
    file="${savedir}/rh-saved.${1}"
    if [ ! -f "$file" ]; then
      printf "No saved stats for that day: $file.\n"
      exit 1
    else 
      date=$(stat --format %y "$file" | cut -d. -f1)
      printf "Saved: %s\n" "$date"
      cat "$file"
    fi
  else
    # Print live stats for today. Force --table output so when saving we get the same
    # format.
    mysql --table -e "SELECT USER, ROUND(SUM(BUSY_TIME), 4) as cpu_usage
      FROM information_schema.user_statistics 
      GROUP BY USER 
      ORDER BY cpu_usage;"
  fi
}

flushstats() {
  # Reset all counters to zero.
  for stats in USER_STATISTICS CLIENT_STATISTICS INDEX_STATISTICS TABLE_STATISTICS; do
    mysql -e "FLUSH ${stats}" information_schema
  done
}

rotatestats() {
  for file in $(ls --reverse "${savedir}/"); do
    day="${file##*.}"
    next_day=$(( $day + 1 ))
    mv "${savedir}/${file}" "${savedir}/rh-saved.${next_day}"
  done
  getstats > "${savedir}/rh-saved.1" 
  flushstats
  # Purge old files.
  find "${savedir}/" -type f -mtime +14 -delete
  exit 0
}

if [ ! -d "$savedir" ]; then
    mkdir "$savedir"
fi

if [ "$1" = "--rotate" ]; then
  rotatestats
elif [ -n "$1" ]; then
  # Strip the "-" from the beginning of the argument to get the numeric days back.
  day="${1:1}"
  # Make sure it's a number.
  re='^[0-9]+$'
  if ! [[ $day =~ $re ]] ; then
    printf "Second argument should be --rotate or -n where n is a number, not %s.\n" "$day"
    exit 1
  fi
fi

getstats "$day"
