#!/usr/bin/python3

import os
import sys
import subprocess
import argparse


def get_all_sites():
    return [os.path.join("/home/sites", d) for d in os.listdir("/home/sites") if os.path.isdir(os.path.join("/home/sites", d)) and os.path.exists(f"/etc/apache2/sites-enabled/site{d}.conf")]

def scan(scan_paths, clear=False, quiet=False, quarantine=False):
    site_problems = []

    # Iterate through paths and run the scan command
    for path in scan_paths:
        site = os.path.basename(path)
        user = f"site{site}writer"
        if not os.path.exists(f"/etc/apache2/sites-enabled/site{site}.conf"):
            print(f"WARNING Site ({path}) is disabled.")
        cmd = [
            'runuser',
            '--user',
            user,
            '--',
            '/usr/local/bin/mf-web-scan-site',
            '--path',
            path
        ]
        if clear:
            cmd.append('--clear')
        if quiet:
            cmd.append('--quiet')
        if quarantine:
            cmd.append('--quarantine')

        try:
            # Run the scan as the owner
            subprocess.run(cmd, check=True)
        except subprocess.CalledProcessError:
            site_problems.append(path)
    return site_problems

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Scan web directories for malicious code.")
    parser.add_argument('--path', type=str, help="The path to scan.")
    parser.add_argument('--all', action='store_true', help="Scan all paths in /home/sites.")
    parser.add_argument('--quiet', action='store_true', help="Scan all paths in /home/sites.")
    parser.add_argument('--clear', action='store_true', help="Add all found files to the clear list.")
    parser.add_argument('--quarantine', action='store_true', help="Move suspicious files to ~/.local/webscan/quarantine.")

    # Parse the arguments
    args = parser.parse_args()

    if args.path and args.all:
        print("You can't pass a path and --all. Pick one.")
        sys.exit(1)

    if args.all and args.clear:
        print("You can't clear all sites. Please use --path to clear just one site.")
        sys.exit(1)

    if not args.all and not args.path:
        print("Please pass either --all or --path.")
        sys.exit(1)

    if args.path:
        args.path = args.path.rstrip('/')
        if not os.path.isdir(args.path):
            print(f"The directory {args.path} does not seem to exist.")
            sys.exit(1)
        dirname_check = os.path.dirname(args.path)
        if dirname_check != "/home/sites":
            print(f"Please choose a path in the /home/sites directory, not {dirname_check}.")
            sys.exit(1)
        paths = [args.path]
    else:
        print("Scanning all sites in /home/sites.")
        paths = get_all_sites()

    problems = scan(paths, clear=args.clear, quiet=args.quiet, quarantine=args.quarantine)
    # Report any problems found
    if problems:
        print(f"FOUND PROBLEMS: {' '.join(problems)}")
        sys.exit(1)
    else:
        print("All sites clean.")
        sys.exit(0)
