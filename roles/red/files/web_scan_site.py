#!/usr/bin/python3
# pylint: disable=missing-docstring,line-too-long

import os
import sys
import shutil
import subprocess
import tempfile
import argparse
import logging
import pwd
import re

class WebScanner:
    path = None
    clear = False
    quiet = False
    quarantine = False
    suspicious_files = []
    problems = []

    def is_root_user(self):
        return os.geteuid() == 0

    def wp_scan(self):
        wp_conf_files = self.wp_find_conf_files()
        if wp_conf_files:
            for wp_conf_file in wp_conf_files:
                wpdir = os.path.dirname(wp_conf_file)
                logging.info("Found WP site: %s", wpdir)
                if self.wp_is_clean(wpdir):
                    logging.info("WP is Clean")
                else:
                    logging.info("WP is NOT Clean")
        else:
            logging.info("No WP sites here.")


    def tmp_scan(self):
        """
        Check for any user owned files in /tmp or /var/tmp
        """
        current_user = pwd.getpwuid(os.geteuid()).pw_name
        # Too many false positives with /tmp 
        #search_dirs = ["/tmp/", "/var/tmp"]
        search_dirs = ["/var/tmp"]
        issues = []
        for search_dir in search_dirs:
            found_files = []
            find_command = ['find', search_dir, '-user', current_user]
            try:
                result = subprocess.run(find_command, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, text=True)

                # Print each found file path
                for line in result.stdout.splitlines():
                    found_files.append(line)

            except subprocess.CalledProcessError:
                # We will get a lot of permission denied errors.
                pass

            if len(found_files) > 0:
                problem = f"Found {len(found_files)} files owned by {current_user} in {search_dir}"
                logging.info(problem)
                issues.append(problem)

        if len(issues) > 0:
            self.problems += issues

    def wp_find_conf_files(self):
        """Check for wp-config.php in the web directory going down 3 directories max."""
        wp_conf_files = []
        for root, _, files in os.walk(f"{self.path}/web"):
            if len(root.split(os.sep)) - len(self.path.split(os.sep)) > 3:
                continue
            if 'wp-config.php' in files:
                wp_conf_files.append(os.path.join(root, 'wp-config.php'))
        return wp_conf_files

    def wp_output_line_indicates_suspicious_file(self, line):
        warning_msg = 'File should not exist:'
        # Mac's create resource files in the form _regular_file.php, so exclude these.
        pattern = r'^(?!.*\/_)[^_]*\.php$'
        # These files keep showing up and look valid
        allowed_php_files = [
            "wp-commentsrss2.php",
            "wp-rdf.php",
            "wp-rss.php",
            "wp-feed.php",
            "wp-atom.php",
            "wp-pass.php",
            "wp-register.php",
            "wp-rss2.php"
        ]
        if warning_msg in line and not line.endswith(tuple(allowed_php_files)) and re.search(pattern, line):
            return True
        return False

    def wp_is_clean(self, path):
        """
        Check if the WordPress site is clean by verifying checksums.
        And handle both false positives and false negatives.
        """

        try:
            # Get WordPress core version
            cmd = ['/usr/local/bin/wp', 'core', 'version', f'--path={path}', '--quiet']
            version = subprocess.check_output(cmd, stderr=subprocess.DEVNULL).strip().decode()
            logging.info("version: %s", version)
        except subprocess.CalledProcessError:
            # If wp command fails, consider it as an incomplete install and return clean.
            return True

        # Assume a clean installation.
        ret = True
        issues = []

        success_msg = 'Success: WordPress installation verifies against checksums.'
        file_not_verify_msg = "File doesn't verify against checksum:"
        wp_not_verify_msg = "Error: WordPress installation doesn't verify against checksums."
        no_checksums = "Error: Couldn't get checksums from WordPress.org."
        # Temporary file to hold the checksum output
        with tempfile.NamedTemporaryFile(delete=False) as tmp:
            try:
                cmd = ['/usr/local/bin/wp', 'core', 'verify-checksums', f'--path={path}']
                subprocess.check_call(cmd, stdout=tmp, stderr=subprocess.STDOUT)
                tmp.seek(0)
                output = [line.decode('utf-8').rstrip() for line in tmp.readlines()]
                for line in output:
                    if self.wp_output_line_indicates_suspicious_file(line):
                        issues.append(line)
                        # Output the full path so it's easy to examine.
                        file_path = line.replace('Warning: File should not exist: ', f"{path}/")
                        self.suspicious_files.append(file_path)
                        ret = False
            except subprocess.CalledProcessError:
                # wp-cli thinks this is a problem. But we check for common false positives.
                tmp.seek(0)
                output = [line.decode('utf-8').rstrip() for line in tmp.readlines()]
                allowed_missing_files = [
                    'readme.html',
                    'wp-config-sample.php',
                    'license.txt',
                ]
                real_errors = []
                for line in output:
                    if line == no_checksums:
                        # Someday we should flag these
                        logging.info("No checksums available, cannot check.")
                        break
                    append_line = True
                    for file in allowed_missing_files:
                        if line.endswith(file):
                            append_line = False
                            break
                    if line == wp_not_verify_msg:
                        # Don't append the extra message - we wonly want to
                        # track the error lines themselves.
                        append_line = False
                    elif file_not_verify_msg in line:
                        append_line = True
                    elif not self.wp_output_line_indicates_suspicious_file(line):
                        # Print these errors, but don't append to issues
                        logging.info(line)
                        append_line = False

                    if append_line:
                        real_errors.append(line)

                if len(real_errors) != 0:
                    issues = real_errors
                    ret = False
            finally:
                os.remove(tmp.name)

        if ret is False:
            for line in issues:
                logging.info(line)
            self.problems += issues

        return ret

    def get_ignore_file(self):
        home = os.getenv("HOME")
        return f"{home}/.config/webscan/ignore"

    def add_problems_to_ignore_file(self):
        # Ensure config directory exists.
        home = os.getenv("HOME")
        webscan_dir = f"{home}/.config/webscan"
        os.makedirs(webscan_dir, exist_ok=True)
        file = self.get_ignore_file()
        if len(self.problems) == 0:
            logging.error("No problems to add to ignore file.")
            return
        with open(file, 'w', encoding='utf8') as file:
            file.write('\n'.join(self.problems))
        logging.info("All problems added to ignore file.")
        # No more problems!
        self.problems = []

    def ignore_problems(self):
        path = self.get_ignore_file()
        remaining_problems = []
        if os.path.exists(path):
            with open(path, 'r', encoding='utf8') as file:
                lines = [line.strip() for line in file] 
                # Reduce the list of problems, removing ones in the clear lsit.
                remaining_problems = [item for item in self.problems if item not in lines]
            ignored = len(self.problems) - len(remaining_problems)
            if ignored > 0:
                logging.info("Removing %d ignored lines.", ignored)
                self.problems = remaining_problems

    def quarantine_suspicious_files(self):
        home = os.getenv("HOME")
        quarantine_dir = f"{home}/.local/webscan/quarantine"
        count = 0
        for abs_src_file in self.suspicious_files:
            rel_src_file = abs_src_file.replace(home, "").lstrip('/')
            rel_src_dir = os.path.dirname(rel_src_file)
            abs_dest_file = f"{quarantine_dir}/{rel_src_file}"
            abs_dest_dir = f"{quarantine_dir}/{rel_src_dir}/"
            if os.path.exists(abs_dest_file):
                raise RuntimeError(f"The file {file} has already been quarantined once. You should isolate this site.")
            os.makedirs(abs_dest_dir, exist_ok=True)
            shutil.move(abs_src_file, abs_dest_file)
            count += 1
        logging.info(f"Quarantined {count} files.")
        self.suspicious_files = []

    def main(self):
        if self.quiet:
            logging.basicConfig(level=logging.ERROR)
        else:
            logging.basicConfig(level=logging.INFO)

        if self.is_root_user():
            logging.error("Please run as the owner of the web directory.")
            sys.exit(1)

        home = os.getenv("HOME")
        skip = f"{home}/.config/webscan/skip"

        if os.path.exists(skip):
            logging.info("Skip file (%s) is present.", skip)
            return []

        logging.info("Scanning %s.", self.path)
        self.wp_scan()
        self.tmp_scan()

        self.ignore_problems()
        if self.clear:
            self.add_problems_to_ignore_file()
        if self.quarantine:
            self.quarantine_suspicious_files()
            return []

        return self.problems

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Scan a web directory for malicious files.")
    parser.add_argument('--path', type=str, help="The path to the web site scan.")
    parser.add_argument('--clear', action="store_true", help="Add found suspicious files to the clear list.")
    parser.add_argument('--quarantine', action="store_true", help="Move extra php files to ~/.local/mf-web-scan/quarantine.")
    parser.add_argument('--quiet', action="store_true", help="Quieter output. Default is more verbose")
    args = parser.parse_args()

    # Parse the arguments
    scanner = WebScanner()
    scanner.clear = args.clear
    scanner.path = args.path
    scanner.quiet = args.quiet
    scanner.quarantine = args.quarantine
    problems = scanner.main()
    if problems:
        logging.error(
            "Found problems with %s (%s)",
            args.path,
            pwd.getpwuid(os.geteuid()).pw_name
        )
        if scanner.suspicious_files:
            for line in scanner.suspicious_files:
                logging.info("Please examine: %s", line)
        sys.exit(1)
    else:
        logging.info("No problems found.")
        sys.exit(0)
