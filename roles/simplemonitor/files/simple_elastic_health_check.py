#!/usr/bin/python3

# Ensure elastic search is healthy

import sys
import os
import argparse
import requests
import yaml

# Parse the command line options.
parser = argparse.ArgumentParser(
    description=(
        'Ensure elastic search is healthy'
    )
)
parser.add_argument(
  '--conf',
  help='Path to configuration file, default /etc/simplemonitor/elastic.yml'
)
parser.set_defaults(
    conf = "/etc/simplemonitor/elastic.yml",
)

args = parser.parse_args()

conf = args.conf

settings = {}
if os.path.isfile(conf):
    with open(conf, 'r', encoding='utf-8') as file:
        settings_raw = file.read(10000)
        settings = yaml.safe_load(settings_raw)
if not settings:
    raise RuntimeError("Failed to find a configuration file.")

username = settings['username']
password = settings['password']
host = settings['host']
port = settings['port']

query = "_cat/health?format=json&pretty"
headers = { 'Content-Type': 'application/json' }
try:
    r = requests.get(
        f'https://{host}:{port}/{query}',
        headers=headers,
        auth=(username, password),
        timeout=30
    )
except Exception as exception:
    print(f"Failed to connect to Elastic Search: {exception}")
    sys.exit(1)
if r.status_code == 200:
    sys.exit(0)
print(f"Elastic search health check returned {r.status_code} instead of 200.")
sys.exit(1)
