#!/usr/bin/python3

"""
Simple Monitor elastic search.

Search for a given line in the message field in elastic search. If found, print
the line and exit with exit code 1. Otherwise, exit with exit code 0.

This script is used by simple monitor to alert us if any lines appear in our
logs that indicate a serious problem.
"""

import sys
import os
import re
import argparse
import yaml
import elasticsearch

# Parse the command line options.
parser = argparse.ArgumentParser(
    description=(
        'Search for a given line in the elastic search database.',
        'If found, exit with error and the full line. If not found,',
        'exit successfully'
    )
)
parser.add_argument(
    '--limit',
    help=(
      'Limit search to the given filter, specify as field=value, e.g.',
      '--limit systemd.unit=dovecot.service'
    )
)
parser.add_argument(
    '--since',
    help=(
      'Limit search to records since the given time frame, e.g. 10m or 4h.',
      '10m by default'
    )
)
parser.add_argument(
  '--until',
  help=(
    'Limit search to records until the given time frame, e.g. 10m or 4h.',
    'Now by default'
  )
)
parser.add_argument(
  '--conf',
  help='Path to configuration file, default /etc/simplemonitor/elastic.yml'
)
parser.add_argument(
  '--index',
  help='Index to search, defaults to "journalbeat-*"'
)
parser.add_argument(
  '--count',
  help='Designate number of hits to trigger, defaults to just 1.',
  type=int
)
parser.add_argument(
  '--lines',
  help='Designate number of hits to include in the output. Defautls to just 1.',
  type=int
)
parser.add_argument(
    'search_string', help='The string you would like to search for.',
    nargs='?'
)
parser.set_defaults(
    conf = "/etc/simplemonitor/elastic.yml",
    count = 1,
    lines = 1,
    since = '10m',
    until = 'now',
    index = 'journalbeat-*',
    search_string = None
)

args = parser.parse_args()

limit = args.limit
since = args.since
until = args.until
conf = args.conf
index = args.index
search_string = args.search_string
count = args.count
lines = args.lines

settings = {}
if os.path.isfile(conf):
    with open(conf, 'r', encoding='utf-8') as file:
        settings_raw = file.read(10000)
        settings = yaml.safe_load(settings_raw)
if not settings:
    raise RuntimeError("Failed to find a configuration file.")

username = settings['username']
password = settings['password']
host = settings['host']
port = settings['port']

es = elasticsearch.Elasticsearch(
    http_auth=(username, password),
    host=host,
    port=port,
    use_ssl=True
)

q = {
    "bool": {
        "filter": [
            {
                "range": {
                    "@timestamp": {
                        "gt": f"now-{since}", "lt": until
                    }
                }
            }
        ]
    }
}

if limit:
    limit_parts = limit.split("=")
    try:
        field = limit_parts[0]
        value = limit_parts[1]
    except IndexError:
        print(f"Please pass a field=value pair to the --limit, not {limit}.")
        sys.exit(1)
    q["bool"]["filter"].append({"match": {field: value}})

if search_string:
    q["bool"]["filter"].append({"match_phrase": {"message": search_string}})

version = elasticsearch.__version__
try:
    if version[0] == 7 and version[1] < 11:
        body = { "query": q }
        res = es.search(index=index, body=body)
    else:
        res = es.search(index=index, query=q)
except Exception as exception:
    print(f"Failed to connect to Elastic Search: {exception}.")
    sys.exit(1)

hit_count = 0
if res['hits']['total']['value'] >= count:
    for hit in res['hits']['hits']:
        hit_count += 1
        if hit_count > lines:
            break
        if 'message' in hit['_source']:
            ALLOW_PATTERN = r'[^a-zA-Z 0-9_:\-.,()]'
            print(re.sub(ALLOW_PATTERN, ' ', hit['_source']['message']))
        else:
            print("Message field not present.")
    sys.exit(1)
