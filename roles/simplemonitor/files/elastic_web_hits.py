#!/usr/bin/python3

"""
Elastic web hit.

Search for most frequent hits in our web logs.
"""

import sys
import os
import json
import argparse
import yaml
import requests

# Parse the command line options.
parser = argparse.ArgumentParser(
    description=(
        'Search for the most frequent hits in our web logs.'
    )
)
parser.add_argument(
  '--conf',
  help='Path to configuration file, default /etc/simplemonitor/elastic.yml'
)
parser.add_argument(
    '--since',
    help=(
      'Limit to records since the given time, 10m by default'
    )
)
parser.add_argument(
  '--until',
  help=(
    'Limit to records until the given time Now by default'
  )
)
parser.add_argument(
  '--site',
  help='Group by site',
  action='store_true',
)
parser.add_argument(
  '--user-agent',
  help='Group by user agent',
  action='store_true',
)
parser.add_argument(
  '--url',
  help='Group by URL path',
  action='store_true',
)
parser.add_argument(
  '--ip',
  help='Group by IP',
  action='store_true',
)
parser.add_argument(
  '--truncate',
  help='Max characters to print for each field. Default is 30',
  type=int
)
parser.add_argument(
  '--host',
  help='Optionally, filter by host name, e.g. webproxy001',
  type=str
)
parser.set_defaults(
    conf = "/etc/simplemonitor/elastic.yml",
    site = False,
    user_agent = False,
    url = False,
    ip = False,
    host = None,
    truncate = 30,
    since = '10m',
    until = '0s',
)

args = parser.parse_args()

since = args.since
until = args.until
site = args.site
user_agent = args.user_agent
url = args.url
ip = args.ip
conf = args.conf
truncate = args.truncate
host = args.host

if not url and not site and not user_agent and not ip:
    print("Pease pass --url, --site, --user-agent or --ip")
    sys.exit(1)

settings = {}
if os.path.isfile(conf):
    with open(conf, 'r', encoding='utf-8') as file:
        settings_raw = file.read(10000)
        settings = yaml.safe_load(settings_raw)
if not settings:
    raise RuntimeError("Failed to find a configuration file.")

es_username = settings['username']
es_password = settings['password']
es_host = settings['host']
es_port = settings['port']


if host:
    # Filter reslts by the give host.
    query = {
        "size": 0,
        "query": {
            "bool": {
                "must": [
                    {
                        "range": { 
                            "@timestamp": { 
                                "gt": f"now-{since}", "lt": f"now-{until}" 
                            }
                        }
                    },
                    {
                        "term": {
                            "host.name": host 
                        }
                    }
                ]
            }
        },
        "aggs": {
            "count": {
                "composite": {
                    "size": 1000,
                    "sources": []
                 }
            }
        }
    }
else:
    query = {
        "size": 0,
        "query": {
            "range": { 
                "@timestamp": { 
                    "gt": f"now-{since}", "lt": f"now-{until}" 
                }
            }
        },
        "aggs": {
            "count": {
                "composite": {
                    "size": 1000,
                    "sources": []
                }
            }
        }
    }

if url:
    urlq = { "url": { "terms": { "field": "url.original"} } }
    query["aggs"]["count"]["composite"]["sources"].append(urlq)
if ip:
    ipq = { "ip": { "terms": { "field": "source.ip"} } }
    query["aggs"]["count"]["composite"]["sources"].append(ipq)
if user_agent:
    user_agentq = { "user_agent": { "terms": { "field": "user_agent.original"} } }
    query["aggs"]["count"]["composite"]["sources"].append(user_agentq)
if site:
    siteq = { "site": { "terms": { "field": "log.file.path"} } }
    query["aggs"]["count"]["composite"]["sources"].append(siteq)


try:
    #url = f'https://{host}:{port}/journalbeat-*/_search'
    headers = { 'Content-Type': 'application/json' }
    url = f'https://{es_host}:{es_port}/filebeat-*/_search'
    r = requests.get(
        url,
        timeout=15,
        headers=headers,
        data=json.dumps(query),
        auth=(es_username, es_password)
    )
except Exception as e:
    print(f"There was an error querying the elastic search server: {format(e)}.")
    sys.exit(1)

res = r.json()

if "aggregations" in res:
    rows = []
    headers = []
    fields = ["ip", "url", "site", "user_agent"]
    for key in res['aggregations']:
        for record in res['aggregations'][key]['buckets']:
            row = []
            if len(rows) == 0:
                headers.append("count")
            # Count is always first.
            row.append(int(record["doc_count"]))

            for field in fields:
                if field in record["key"]:
                    if len(rows) == 0:
                        headers.append(field)
                    value = record["key"][field]
                    if field == "site":
                        # We get /var/log/nginx/something.access.log.
                        # We want "something".
                        value = value.split('/')[-1].rsplit('.access.log', 1)[0]
                    row.append(value[:truncate])
            rows.append(row)
        rows.sort(key=lambda x: x[0], reverse=True)
    LIMIT = 40
    current = 0
    # Get max width of columns.
    col_widths = [max(len(str(item)) for item in column) for column in zip(*rows)]
    # Print headers
    print("  ".join(str(item).ljust(col_widths[i]) for i, item in enumerate(headers)))
    # Print header separator
    print("  ".join("-" * col_widths[i] for i in range(len(headers))))
    # Print each row
    for row in rows[1:]:
        print("  ".join(str(item).ljust(col_widths[i]) for i, item in enumerate(row)))
        if current > LIMIT:
            break
        current += 1
else:
    print(res)
