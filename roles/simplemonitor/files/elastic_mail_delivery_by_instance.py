#!/usr/bin/python3

"""
Elastic mail delivery by instance.

Report mail delivery status by instance and relay type.
"""

import sys
import os
import json
import argparse
import yaml
import requests

# Parse the command line options.
parser = argparse.ArgumentParser(
    description=(
        'Report mail delivery status by instance and relay type.'
    )
)
parser.add_argument(
  '--conf',
  help='Path to configuration file, default /etc/simplemonitor/elastic.yml'
)
parser.add_argument(
    '--since',
    help=(
      'Limit to records since the given time, 10m by default'
    )
)
parser.add_argument(
  '--until',
  help=(
    'Limit to records until the given time Now by default'
  )
)
parser.add_argument(
  '--relay-type',
  help='bulk, filtered or priority',
  required=True
)

parser.set_defaults(
    conf = "/etc/simplemonitor/elastic.yml",
    site = False,
    user_agent = False,
    url = False,
    ip = False,
    host = None,
    truncate = 30,
    since = '10m',
    until = '0s',
)

args = parser.parse_args()

conf = args.conf
since = args.since
until = args.until
relay_type = args.relay_type

settings = {}
if os.path.isfile(conf):
    with open(conf, 'r', encoding='utf-8') as file:
        settings_raw = file.read(10000)
        settings = yaml.safe_load(settings_raw)
if not settings:
    raise RuntimeError("Failed to find a configuration file.")

es_username = settings['username']
es_password = settings['password']
es_host = settings['host']
es_port = settings['port']


# Filter reslts by the relay type
query = {
    "size": 0,
    "query": {
        "bool": {
            "filter": [
                {
                    "range": { 
                        "@timestamp": { 
                            "gt": f"now-{since}", "lt": f"now-{until}" 
                        }
                    }
                },
                {
                    "wildcard": {
                        "dissect.postfix_instance": f"postfix-{relay_type}*"
                    }
                }
            ]
        }
    },
    "aggs": {
        "count": {
            "composite": {
                "size": 1000,
                "sources": [
                    {
                        "instance": {
                            "terms": {
                                "field": "dissect.postfix_instance"
                            }
                        }
                    },
                    {
                        "status": {
                            "terms": {
                                "field": "dissect.postfix_status"
                            }
                        }
                    }
                ]
             }
        }
    }
}


try:
    #url = f'https://{host}:{port}/journalbeat-*/_search'
    headers = { 'Content-Type': 'application/json' }
    url = f'https://{es_host}:{es_port}/journalbeat-*/_search'
    r = requests.get(
        url,
        timeout=15,
        headers=headers,
        data=json.dumps(query),
        auth=(es_username, es_password)
    )
except Exception as e:
    print(f"There was an error querying the elastic search server: {format(e)}.")
    sys.exit(1)

res = r.json()

if "aggregations" in res:
    totals = {}
    for key in res['aggregations']:
        for record in res['aggregations'][key]['buckets']:
            count = int(record["doc_count"])
            status = record["key"]["status"]
            instance = record["key"]["instance"]
            if instance not in totals:
                totals[instance] = {
                    "sent": 0,
                    "deferred": 0,
                    "bounced": 0,
                    "expired": 0
                }

            totals[instance][status] += count
    rows = []
    headers = ["instance", "snt%", "snt", "bnc", "def", "exp"]
    for instance in totals:
        sent = totals[instance]["sent"]
        bounced = totals[instance]["bounced"]
        expired = totals[instance]["expired"]
        deferred = totals[instance]["deferred"]
        count = sent + bounced + expired
        sentpct = sent / count
        rows.append([instance, f"{sentpct:.1%}", sent, bounced, deferred, expired])


    rows.sort(key=lambda x: x[1])
    # Get max width of columns.
    # Append headers just to get col widths
    rows.append(headers)
    col_widths = [max(len(str(item)) for item in column) for column in zip(*rows)]
    # Now remove the headers
    rows = rows[:-1]
    # Print headers
    print("  ".join(str(item).ljust(col_widths[i]) for i, item in enumerate(headers)))
    # Print header separator
    print("  ".join("-" * col_widths[i] for i in range(len(headers))))
    # Print each row
    for row in rows:
        print("  ".join(str(item).ljust(col_widths[i]) for i, item in enumerate(row)))
else:
    print(res)
