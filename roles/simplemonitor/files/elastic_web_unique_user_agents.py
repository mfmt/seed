#!/usr/bin/python3

"""
Elastic web hit.

Search for most frequent hits in our web logs.
"""

import sys
import os
import json
import yaml
import requests

CONF = "/etc/simplemonitor/elastic.yml"
SINCE = "7d"
UNTIL = "1s"
TRUNCATE = 70
LIMIT = 200

settings = {}
if os.path.isfile(CONF):
    with open(CONF, 'r', encoding='utf-8') as file:
        settings_raw = file.read(10000)
        settings = yaml.safe_load(settings_raw)
if not settings:
    raise RuntimeError("Failed to find a configuration file.")

es_username = settings['username']
es_password = settings['password']
es_host = settings['host']
es_port = settings['port']

query = {
    "size": 0,
    "query": {
        "range": { 
            "@timestamp": { 
                "gt": f"now-{SINCE}", "lt": f"now-{UNTIL}" 
            }
        }
    },
    "aggs": {
        "count": {
            "composite": {
                "size": 10000,
                "sources": []
            }
        }
    }
}

user_agentq = { "user_agent": { "terms": { "field": "user_agent.original"} } }
query["aggs"]["count"]["composite"]["sources"].append(user_agentq)


try:
    #url = f'https://{host}:{port}/journalbeat-*/_search'
    headers = { 'Content-Type': 'application/json' }
    url = f'https://{es_host}:{es_port}/filebeat-*/_search'
    r = requests.get(
        url,
        timeout=15,
        headers=headers,
        data=json.dumps(query),
        auth=(es_username, es_password)
    )
except Exception as e:
    print(f"There was an error querying the elastic search server: {format(e)}.")
    sys.exit(1)

res = r.json()

if "aggregations" in res:
    rows = []
    headers = []
    fields = ["user_agent"]
    for key in res['aggregations']:
        for record in res['aggregations'][key]['buckets']:
            row = []
            if len(rows) == 0:
                headers.append("count")
            # Count is always first.
            row.append(int(record["doc_count"]))

            for field in fields:
                if field in record["key"]:
                    if len(rows) == 0:
                        headers.append(field)
                    value = record["key"][field]
                    if field == "site":
                        # We get /var/log/nginx/something.access.log.
                        # We want "something".
                        value = value.split('/')[-1].rsplit('.access.log', 1)[0]
                    row.append(value[:TRUNCATE])
            rows.append(row)
        rows.sort(key=lambda x: x[0], reverse=True)
    current = 0
    # Get max width of columns.
    col_widths = [max(len(str(item)) for item in column) for column in zip(*rows)]
    # Print headers
    print("  ".join(str(item).ljust(col_widths[i]) for i, item in enumerate(headers)))
    # Print header separator
    print("  ".join("-" * col_widths[i] for i in range(len(headers))))
    # Print each row
    for row in rows[1:]:
        print("  ".join(str(item).ljust(col_widths[i]) for i, item in enumerate(row)))
        if current > LIMIT:
            break
        current += 1
else:
    print(res)
