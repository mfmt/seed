#!/usr/bin/python3

# A script see if any hosts are missing a pigeon heart beat. Pigeon is our
# service that runs on every host and checks to make sure everything is working
# ok. It sends a "heart beat" message via journald every minute to let us know
# it is running. If we detect a missing heart beat, this script will exit with
# error code 1 and a report of which hosts have stopped sending heart beats.

import datetime
import os
import sys
import argparse
import json
import requests
import yaml

# Parse the command line options.
parser = argparse.ArgumentParser(
    description=(
        'Ensure all hosts are sending heartbeats'
    )
)
parser.add_argument(
    '--since',
    help=(
      'Indicate how far back we should search for heartbeats. Default 15m.'
    )
)
parser.add_argument(
  '--conf',
  help='Path to configuration file, default /etc/simplemonitor/elastic.yml'
)
parser.add_argument(
  '--hosts',
  help='Path to hosts file, default /etc/simplemonitor/hosts.yml'
)
parser.add_argument(
  '--index',
  help='Index to search, defaults to "journalbeat-*"'
)
parser.set_defaults(
    conf = "/etc/simplemonitor/elastic.yml",
    hosts = "/etc/simplemonitor/hosts.yml",
    since = '10m',
    index = 'journalbeat-*'
)

args = parser.parse_args()

since = args.since
conf = args.conf
hosts = args.hosts
index = args.index

# Set our base times.
now = datetime.datetime.now(datetime.timezone.utc)
some_minutes_ago = now - datetime.timedelta(minutes=15)

settings = {}
if os.path.isfile(conf):
    with open(conf, 'r', encoding='utf-8') as file:
        settings_raw = file.read(10000)
        settings = yaml.safe_load(settings_raw)
if not settings:
    raise RuntimeError("Failed to find a configuration file.")

username = settings['username']
password = settings['password']
host = settings['host']
port = settings['port']

query = {
  "aggs": {
    "hostname": {
      "terms": {
        "field": "host.hostname",
        "order": {
          "_count": "desc"
        },
        "size": 1000
      },
      "aggs": {
        "hostname_count": {
          "filters": {
            "filters": {
              "Heartbeats": {
                "term": {
                  "dissect.pigeon_priority": "heartbeat"
                }
              }
            }
          }
        }
      }
    }
  },
  "size": 0,
  "query": {
    "bool": {
      "filter": [
        {
          "range": {
            "@timestamp": {
              "gte": f"now-{since}",
              "lte": "now",
              "format": "strict_date_optional_time"
            }
          }
        }
      ]
    }
  }
}

headers = { 'Content-Type': 'application/json' }
try:
    r = requests.get(
        f'https://{host}:{port}/{index}/_search',
        headers=headers,
        data=json.dumps(query),
        auth=(username, password),
        timeout=30
    )
except Exception as exception:
    print(f"Failed to connect to Elastic Search: {exception}.")
    sys.exit(1)

try:
    buckets = r.json()["aggregations"]["hostname"]["buckets"]
except KeyError:
    print(f"Failed to connect to Elastic Search: {exception}.")
    sys.exit(1)

# This query iterates over all the hosts that have sent any data to elastic
# search and it counts the pigeon heart beats we have received for each one.
#
# There are two possible things to check: 1. If we have some data for a host,
# but not a pigeon heart beat 2. If we have no data from a host at all.

no_heartbeat = []
no_data = []
found_hosts = []

# Load a list of hosts that we should have data for.
with open(hosts, 'r', encoding='utf-8') as file:
    all_hosts = yaml.safe_load(file)

for bucket in buckets:
    fqdn_host = f"{bucket['key']}.mayfirst.org"
    found_hosts.append(fqdn_host)
    if int(bucket['hostname_count']['buckets']['Heartbeats']['doc_count']) == 0:
        if fqdn_host in all_hosts:
            no_heartbeat.append(bucket['key'])

if len(no_heartbeat) > 0:
    print(f"Missing pigeon heartbeat from the following hosts.Please run 'systemctl start pigeon' to resolve the issue: {','.join(no_heartbeat)}.")
    sys.exit(1)

no_data = list(set(all_hosts) - set(found_hosts))
if len(no_data) > 0:
    print(f"No elastic search data following hosts. Please run 'systemctl restart journalbeat' or run sower with the --secrets option to resolve the issue: {','.join(no_data)}.")
    sys.exit(1)
