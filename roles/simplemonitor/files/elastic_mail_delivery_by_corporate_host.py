#!/usr/bin/python3

"""
Elastic mail delivery status by corporate host.

Print delivery stats for the main corporate hosts.
"""

import sys
import os
import json
import argparse
import yaml
import requests

# Parse the command line options.
parser = argparse.ArgumentParser(
    description=(
        "Print delivery stats for the main corporate hosts."
    )
)
parser.add_argument(
  '--conf',
  help='Path to configuration file, default /etc/simplemonitor/elastic.yml'
)
parser.add_argument(
    '--since',
    help=(
      'Limit to records since the given time, 10m by default'
    )
)
parser.add_argument(
  '--until',
  help=(
    'Limit to records until the given time Now by default'
  )
)
parser.add_argument(
  '--corp',
  help='Either google, microsoft, or yahoo',
)

parser.set_defaults(
    conf = "/etc/simplemonitor/elastic.yml",
    since = '10m',
    until = '0s',
    relay_type = None
)

args = parser.parse_args()

since = args.since
until = args.until
conf = args.conf
corp = args.corp

allowed_corps = [ "google", "microsoft", "yahoo" ]

if corp and corp not in allowed_corps:
    raise RuntimeError(f"Corp must be one of: {allowed_corps}")

settings = {}
if os.path.isfile(conf):
    with open(conf, 'r', encoding='utf-8') as file:
        settings_raw = file.read(10000)
        settings = yaml.safe_load(settings_raw)
if not settings:
    raise RuntimeError("Failed to find a configuration file.")

es_username = settings['username']
es_password = settings['password']
es_host = settings['host']
es_port = settings['port']


query = {
    "size": 0,
    "query": {
        "bool": {
            "filter": [
                {
                    "range": {
                        "@timestamp": {
                            "gt": f"now-{since}", "lt": f"now-{until}"
                        }
                    }
                },
            ]
        }
    },
    "aggs": {
        "count": {
            "composite": {
                "size": 10000,
                "sources": []
             }
        }
    }
}

if corp:
    if corp == "google":
        CORP_FILTER = "*google.com*"
    elif corp == "yahoo":
        CORP_FILTER = "*yahoodns.net*"
    elif corp == "microsoft":
        CORP_FILTER = "*outlook.com*"
    else:
        print(f"Unnknown corporation: {corp}")
        CORP_FILTER = None

    if CORP_FILTER:
        query["query"]["bool"]["filter"].append({
            "wildcard": {
                "dissect.postfix_relay": CORP_FILTER
            }
        })

statusq = { "status": { "terms": { "field": "dissect.postfix_status"} } }
query["aggs"]["count"]["composite"]["sources"].append(statusq)

try:
    headers = { 'Content-Type': 'application/json' }
    url = f'https://{es_host}:{es_port}/journalbeat-*/_search'
    r = requests.get(
        url,
        timeout=15,
        headers=headers,
        data=json.dumps(query),
        auth=(es_username, es_password)
    )
except Exception as e:
    print(f"There was an error querying the elastic search server: {format(e)}.")
    sys.exit(1)

res = r.json()

totals = {}
rows = []
if "aggregations" in res:
    if corp:
        print(f"Stats for {corp}")
    else:
        print("Status for all email")
    print("---------")
    for key in res['aggregations']:
        for record in res['aggregations'][key]['buckets']:
            count = int(record["doc_count"])
            status = record["key"]["status"]
            totals[status] = count

    sent = totals.get("sent", 0)
    expired = totals.get("expired", 0)
    bounced = totals.get("bounced", 0)
    deferred = totals.get("deferred", 0)

    total = sent + expired + bounced
    sentpct = f"{sent/total:.1%}"
    bouncepct = f"{(bounced+expired)/total:.1%}"

    headers = ["Label", "Count"]
    rows.append(headers)
    for status in totals:
        rows.append([status, totals[status]])
    rows.append(["----", "----"])
    rows.append(["Percent sent", sentpct])
    rows.append(["Percent bounced", bouncepct])

    # Get max width of columns.
    col_widths = [max(len(str(item)) for item in column) for column in zip(*rows)]
    # Print headers
    print("  ".join(str(item).ljust(col_widths[i]) for i, item in enumerate(headers)))
    # Print header separator
    print("  ".join("-" * col_widths[i] for i in range(len(headers))))
    # Print each row
    for row in rows[1:]:
        print("  ".join(str(item).ljust(col_widths[i]) for i, item in enumerate(row)))

else:
    print(res)
