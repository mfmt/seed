#!/usr/bin/python3

"""
Simple Monitor elastic pigeon search.

Search for any pigeon alerts.
"""

import sys
import os
import re
import argparse
import yaml
import elasticsearch

# Parse the command line options.
parser = argparse.ArgumentParser(
    description=(
        'Search pigeon alerts in our elastic search database.'
    )
)
parser.add_argument(
    '--priority',
    help=(
      'Limit search by priority, either critical, warning or info'
    ),
    choices=['critical', 'warning', 'info'],
    required=True
)
parser.add_argument(
    '--since',
    help=(
      'Limit search to records since the given time frame, e.g. 10m or 4h.',
      '10m by default'
    )
)
parser.add_argument(
  '--until',
  help=(
    'Limit search to records until the given time frame, e.g. 10m or 4h.',
    'Now by default'
  )
)
parser.add_argument(
  '--conf',
  help='Path to configuration file, default /etc/simplemonitor/elastic.yml'
)
parser.set_defaults(
    conf = "/etc/simplemonitor/elastic.yml",
    since = '10m',
    until = 'now',
)

args = parser.parse_args()

priority = args.priority
since = args.since
until = args.until
conf = args.conf

INDEX = 'journalbeat-*'

settings = {}
if os.path.isfile(conf):
    with open(conf, 'r', encoding='utf-8') as file:
        settings_raw = file.read(10000)
        settings = yaml.safe_load(settings_raw)
if not settings:
    raise RuntimeError("Failed to find a configuration file.")

username = settings['username']
password = settings['password']
host = settings['host']
port = settings['port']

es = elasticsearch.Elasticsearch(
    http_auth=(username, password),
    host=host,
    port=port,
    use_ssl=True
)

q = {
    "bool": {
        "filter": [
            {
                "range": {
                    "@timestamp": {
                        "gt": f"now-{since}", "lt": until
                    }
                }
            },
            {
                "match": {
                    "systemd.unit": "pigeon.service"
                }
            },
            {
                "match": {
                    "dissect.pigeon_priority": priority
                }
            }
        ]
    }
}

version = elasticsearch.__version__
try:
    if version[0] == 7 and version[1] < 11:
        body = { "query": q }
        res = es.search(index=INDEX, body=body)
    else:
        res = es.search(index=INDEX, query=q)
except Exception as exception:
    print(f"Failed to connect to Elastic Search: {exception}.")
    sys.exit(1)

report = []
if res['hits']['total']['value'] > 0:
    for hit in res['hits']['hits']:
        line = hit['_source']['dissect']['pigeon_query_key'] + \
            ": " + hit['_source']['dissect']['pigeon-msg']
        if line not in report:
            report.append(line)
    for line in report:
        # We have to sanitize output because it will be used as an
        # argument to alerter.
        ALLOW_PATTERN = r'[^a-zA-Z 0-9_:\-.,()]'
        print(re.sub(ALLOW_PATTERN, ' ', line))
    sys.exit(1)
