# Common config for all longshore bulk mail servers servers 
#
# Servers using this role must also use the opendkim role.

- name: install essential longshore relay packages
  apt:
    name:
      - postfix-pcre
      - letsencrypt

# Generate letsencrypt file
- name: generate lets encrypt file
  command: certbot certonly --standalone --preferred-challenges http -d {{ postfix_hostname }} --non-interactive --email ssl@mayfirst.org --agree-tos
  args:
    creates: /etc/letsencrypt/live/{{ postfix_hostname }}/fullchain.pem

- name: basic postfix settings 
  blockinfile:
    path: /etc/postfix/main.cf
    marker: "# {mark} ANSIBLE MANAGED BLOCK BULK"
    block: |
      master_service_disable = inet
      # OpenDKIM milter configuration
      milter_default_action = accept
      milter_protocol = 6
      smtpd_milters = inet:localhost:8892
  notify: reload postfix

# Bulk servers accept email that they are not responsible for, so
# we have to list IPs allowed to relay email.
- name: set postfix mynetworks parameters
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: mynetworks =
    line: mynetworks = {{ postfix_mynetworks }}
  notify: reload postfix

# We set more restricted relay permissions to prevent anyone from outside of
# our network from submitting email directly to the bulk servers rather then
# via the mx servers which have tighter restrictions. This line ensures that we
# only relay email from the allowed IPs and from authenticated senders.
- name: set relay restrictions 
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: smtpd_relay_restrictions =
    line: smtpd_relay_restrictions = permit_mynetworks reject 
  notify: reload postfix

- name: set fqdn as the mailname
  copy:
    content: "{{ postfix_hostname }}\n"
    dest: /etc/mailname
  notify: reload postfix

- name: set proper path to tls cert
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_tls_cert_file='
    line: 'smtpd_tls_cert_file=/etc/letsencrypt/live/{{ postfix_hostname }}/fullchain.pem'
  notify: reload postfix

- name: set proper path to tls key
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_tls_key_file='
    line: 'smtpd_tls_key_file=/etc/letsencrypt/live/{{ postfix_hostname }}/privkey.pem'
  notify: reload postfix

- name: set common block for all postfix servers 
  blockinfile:
    path: /etc/postfix/main.cf
    marker: "# {mark} ANSIBLE MANAGED BLOCK COMMON"
    block: |
      smtpd_tls_CApath = /etc/ssl/cert
      smtp_tls_CApath = /etc/ssl/cert
      message_size_limit = 26214400 
  notify: reload postfix

- name: set myhostname parameter
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: '^myhostname = '
    line: 'myhostname = {{ postfix_hostname }}'
  notify: reload postfix

- name: ensure root email goes to a human
  lineinfile:
    path: /etc/aliases
    regexp: 'root:'
    line: 'root: root@mayfirst.{{ m_tld }}'
  notify: newaliases 

- name: ensure postfix is running
  service:
    name: postfix 
    state: started
    enabled: true

## Setup multi instances.
- name: check if postfix multi instance has been initialized
  command: grep multi_instance_wrapper /etc/postfix/main.cf
  register: multi_initialized
  failed_when: multi_initialized.rc > 1

- name: initialize postfix multi instance support
  command: postmulti -e init
  when: multi_initialized.rc == 1

- name: setup multi instances
  command: 
    cmd: postmulti -I postfix-{{ item.name }} -e create
    creates: /etc/postfix-{{ item.name }}
  loop: "{{ postfix_instances }}"

# Note: our template includes multi_instance_enable = yes so we don't
# need to use the multi command to enable them.
- name: replace main.cf for multi sites
  template:
    dest: /etc/postfix-{{ item.name }}/main.cf
    src: bulk/multi.main.cf.j2
  loop: "{{ postfix_instances }}"
  notify: reload postfix

- name: set outgoing smtp bind ip address in master.cf 
  lineinfile:
    path: /etc/postfix-{{ item.name }}/master.cf
    regexp: ^smtp      unix
    line: smtp      unix  -       -       y       -       -       smtp -o smtp_bind_address={{ item.ip }}
  loop: "{{ postfix_instances }}"

- name: copy over our header-checks file that blocks email from corporate from addresses. 
  copy:
    src: bulk/header-checks.pcre
    dest: /etc/postfix-{{ item.name }}/header-checks.pcre
  loop: "{{ postfix_instances }}"
  notify: reload postfix

- name: enable postfix instances 
  command: systemctl enable postfix@postfix-{{ item.name }}
  loop: "{{ postfix_instances }}"


