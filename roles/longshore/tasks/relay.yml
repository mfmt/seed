# Common config for all longshore relay mail servers servers 
#
# This configuration is a temporary configuration so that longshore servers can use the new
# ansible setup while the main relay servers are still on the old puppet settings.
#
# It is essentially like a bulk mail server, however, it includes DKIM signing. Also, it does not
# support ldap.

- name: install opendkim and essential packages
  apt:
    name:
      - postfix
      - opendkim
      - rsync
      - sudo
      - letsencrypt
      - opendkim-tools

# Generate letsencrypt file
- name: generate lets encrypt file
  command: certbot certonly --standalone --preferred-challenges http -d {{ postfix_hostname }} --non-interactive --email ssl@mayfirst.org --agree-tos
  args:
    creates: /etc/letsencrypt/live/{{ postfix_hostname }}/fullchain.pem

# Simplify opendkim configuration by listening via TCP instead of
# via a socket.
- name: set opendkim to listen via localhost 
  lineinfile:
    path: /etc/opendkim.conf
    regexp: "Socket *inet:"
    line: "Socket inet:8892@localhost" 

- name: set opendkim to NOT listen via socket 
  lineinfile:
    path: /etc/opendkim.conf
    regexp: "Socket\\s*local:"
    line: "#Socket local:/var/run/opendkim/opendkim.sock" 
  notify: restart opendkim 

- name: create opendkim config direcgtory.
  file:
    state: directory
    path: /etc/opendkim

- name: configure opendkim to consult files for domain configuration
  blockinfile:
    path: /etc/opendkim.conf
    marker: "# {mark} ANSIBLE MANAGED OPENDKIM INCLUDE BLOCK"
    block: |
      KeyTable /etc/opendkim/KeyTable 
      SigningTable /etc/opendkim/SigningTable
      ExternalIgnoreList /etc/opendkim/TrustedHosts
      InternalHosts /etc/opendkim/TrustedHosts
  notify: restart opendkim 

- name: copy bulk transport map
  template:
    src: relay/transport.bulk.j2
    dest: /etc/postfix/transport
  notify: 
    - restart postfix
    - postmap transport

- name: basic postfix settings 
  blockinfile:
    path: /etc/postfix/main.cf
    marker: "# {mark} ANSIBLE MANAGED BLOCK BULK"
    block: |
      transport_maps = hash:/etc/postfix/transport
      # OpenDKIM milter configuration
      milter_default_action = accept
      milter_protocol = 6
      smtpd_milters = inet:localhost:8892
  notify: restart postfix

# Bulk servers accept email that they are not responsible for, so
# we have to list IPs allowed to relay email.
- name: set postfix mynetworks parameters
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: mynetworks =
    line: mynetworks = {{ postfix_mynetworks }}
  notify: restart postfix

# We set more restricted relay permissions to prevent anyone from outside of
# our network from submitting email directly to the bulk servers rather then
# via the mx servers which have tighter restrictions. This line ensures that we
# only relay email from the allowed IPs and from authenticated senders.
- name: set relay restrictions 
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: smtpd_relay_restrictions =
    line: smtpd_relay_restrictions = permit_mynetworks reject 
  notify: restart postfix

- name: create user for creating and sync'ing opendkim keys
  user:
    name: opendkim-creator 

- name: create ssh directory 
  file:
    state: directory
    path: /home/opendkim-creator/.ssh
    owner: opendkim-creator
    group: opendkim-creator

- name: create ssh keypair for root so it can rsync dkim keys to other servers
  openssh_keypair:
    path: /root/.ssh/id_rsa

- name: allow longshore servers to ssh in to create opendkim keys
  template:
    src: relay/authorized_keys.j2
    dest: /home/opendkim-creator/.ssh/authorized_keys

- name: copy dkim helper files into place
  copy:
    src: "{{ item }}"
    dest: /usr/local/bin/
    mode: 0755
  loop:
    - relay/dkim-rsync
    - relay/dkim-place
    - relay/dkim-generate

- name: ensure sudo access by opendkim-creator user
  copy:
    src: relay/opendkim-creator.sudo
    dest: /etc/sudoers.d/opendkim-creator
    mode: 0600

- name: set fqdn as the mailname
  copy:
    content: "{{ postfix_hostname }}\n"
    dest: /etc/mailname
  notify: restart postfix

- name: set proper path to tls cert
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_tls_cert_file='
    line: 'smtpd_tls_cert_file=/etc/letsencrypt/live/{{ postfix_hostname }}/fullchain.pem'
  notify: restart postfix

- name: set proper path to tls key
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: 'smtpd_tls_key_file='
    line: 'smtpd_tls_key_file=/etc/letsencrypt/live/{{ postfix_hostname }}/privkey.pem'
  notify: restart postfix

- name: set common block for all postfix servers 
  blockinfile:
    path: /etc/postfix/main.cf
    marker: "# {mark} ANSIBLE MANAGED BLOCK COMMON"
    block: |
      smtpd_tls_CApath = /etc/ssl/cert
      smtp_tls_CApath = /etc/ssl/cert
      message_size_limit = 26214400 
  notify: restart postfix

- name: set myhostname parameter
  lineinfile:
    path: /etc/postfix/main.cf
    regexp: '^myhostname = '
    line: 'myhostname = {{ postfix_hostname }}'
  notify: restart postfix

- name: ensure root email goes to a human
  lineinfile:
    path: /etc/aliases
    regexp: 'root:'
    line: 'root: root@mayfirst.{{ m_tld }}'
  notify: newaliases 

- name: ensure postfix is running
  service:
    name: postfix 
    state: started
    enabled: true

