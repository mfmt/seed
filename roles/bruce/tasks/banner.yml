- name: install debian packages for bruce banner
  apt:
    name:
      - nftables
      - autossh

- name: create symlinks to bruce executables
  file:
    state: link
    src: /usr/local/src/bruce/{{ item }}
    dest: /usr/local/bin/{{ item }}
  with_items:
    - bruce-banner

- name: create bruce-banner user
  user:
    name: bruce-banner
    home: /var/lib/bruce-banner
    system: yes

- name: create ssh directory
  file:
    state: directory
    path: /var/lib/bruce-banner/.ssh
    owner: bruce-banner
    group: bruce-banner

- name: set private ssh key for bruce-banner
  copy:
    content: "{{ vault_bruce_ssh_private_key }}"
    dest: /var/lib/bruce-banner/.ssh/id_rsa
    owner: bruce-banner
    group: bruce-banner
    mode: 0600
  when: vault_bruce_ssh_private_key is defined

- name: copy sudoers file
  copy:
    remote_src: true
    src: /usr/local/src/bruce/sudoers.d/bruce-banner.sudoers
    dest: /etc/sudoers.d/bruce-banner

- name: copy bruce banner defaults file
  copy:
    remote_src: yes
    src: /usr/local/src/bruce/systemd/etc-default-bruce-banner
    dest: /etc/default/bruce-banner
    force: no
  notify: restart bruce-banner

- name: set bruce server proxy login 
  lineinfile:
    path: /etc/default/bruce-banner
    regexp: BRUCE_SSH_PROXY_LOGIN=
    line: BRUCE_SSH_PROXY_LOGIN={{ bruce_ssh_proxy_login }}
  notify: restart bruce-banner

- name: set subscribe topics
  lineinfile:
    path: /etc/default/bruce-banner
    regexp: BRUCE_BANNER_SUBSCRIBE_TOPICS=
    line: BRUCE_BANNER_SUBSCRIBE_TOPICS="{{ bruce_subscribe_topics }}"
  notify: restart bruce-banner

# BRUCE_DEBUG causes bruce-banner to just echo commands
# to standard out instead of executing them. Since nft
# doesn't work on docker containers, we need to echo 
# out or we'll get errors.
- name: set to debug in dev and testing 
  lineinfile:
    path: /etc/default/bruce-banner
    regexp: BRUCE_DEBUG=
    line: BRUCE_DEBUG=1
  notify: restart bruce-banner
  when: m_environment == "dev" or m_environment == "test"

- name: copy systemd files
  copy:
    src: /usr/local/src/bruce/systemd/{{ item }}
    remote_src: yes
    dest: /etc/systemd/system/{{ item }}
  with_items:
    - bruce-banner.service
    - bruce-banner-ssh-proxy.service
  notify:
    - reload systemd
    - enable bruce-banner-ssh-proxy
    - start bruce-banner-ssh-proxy
    - enable bruce-banner
    - start bruce-banner

- name: ensure bruce-banner service is enabled and started
  service:
    name: "{{ item }}"
    state: started
    enabled: true
  with_items:
    - bruce-banner.service
    - bruce-banner-ssh-proxy.service

