# vim: filetype=python
# Generate reports on various aspects of our servers. 
import subprocess
import csv
from tabulate import tabulate
import os
import dns
import string
from IPy import IP

class SowerReport():
    inventory = None
    host = None
    csv_path = None 
    available = False
    network = None

    def get_location(self, host):
        locations = ["telehouse", "greenhost", "monkeybrains", "koumbit", "elisa", "ccr", "easydns", "usdedicated", "awknet", "oax", "tachanka"]
        for location in locations:
            if self.inventory.hosts_yaml['all']['children'][location]['hosts']:
              if host in self.inventory.hosts_yaml['all']['children'][location]['hosts']:
                  return location 
        return "unknown" 

    def get_a_records(self, domain):
        records = {}
        for letter in list(string.ascii_lowercase):
            check_domain = f"{letter}.{domain}"
            try:
              answers = dns.resolver.resolve(check_domain, 'A')
              if not answers:
                continue
            except dns.resolver.NXDOMAIN:
                continue 
            records[check_domain] = answers[0].to_text()
        return records

    def get_report_for_name_prefix(self, prefix):
        report = []
        for host in self.inventory.host_vars_yaml:
            if host.startswith(prefix):
              values = self.inventory.host_vars_yaml[host]
              parent = values["kvm"]["host"]
              location = self.get_location(host)
              report.append(f"{host} : {location} {parent}")
        return sorted(report)

    def get_report_for_a_records(self, group, a_records):
        report = []
        hosts = list(self.inventory.hosts_yaml['all']['children'][group]['hosts'].keys())
        for host in hosts:
            values = self.inventory.host_vars_yaml[host]
            if "network" in values:
                for network in values['network']:
                    if "networks" in network:
                        for address_block in network['networks']:
                            if 'address' in address_block:
                                ip = address_block["address"].split('/')[0]
                                # Iterate over all IPs in the a_records we were given.
                                if ip in list(a_records.values()):
                                    # Keep track of which a record fqdn corresponds to this IP
                                    a_record_fqdns = [key for key, val in a_records.items() if val == ip]
                                    for a_record_fqdn in a_record_fqdns:
                                        try:
                                            parent = self.inventory.host_vars_yaml[host]["kvm"]["host"]
                                            parent = f"on {parent}"
                                        except KeyError:
                                            parent = ""
                                        location = self.get_location(host)
                                        report.append(f"{a_record_fqdn} => {host} : {location} {parent}")
        return sorted(report)

    def map(self):
        # Map out all the domain names that currently resolve in the
        # .webproxy.mayfirst.org name space
        self.inventory.set_hosts()
        self.inventory.set_host_vars()
        print()
        print("Active Web Proxies")
        webproxies = self.get_a_records("webproxy.mayfirst.org")
        report = self.get_report_for_a_records('webproxy', webproxies)
        for line in report:
            print(f" {line}")
        print()
        print("Active DNS Servers")
        dns_servers = self.get_a_records("ns.mayfirst.org")
        report = self.get_report_for_a_records('nsauth', dns_servers)
        for line in report:
            print(f" {line}")
        print()
        print("Active MX Servers")
        mx_servers = self.get_a_records("mx.mayfirst.org")
        report = self.get_report_for_a_records('postfix', mx_servers)
        for line in report:
            print(f" {line}")
        print()
        print("Web Origin Servers")
        report = self.get_report_for_name_prefix("weborigin")
        for line in report:
            print(f" {line}")
        print()
        print("Mail Store Servers")
        report = self.get_report_for_name_prefix("mailstore")
        for line in report:
            print(f" {line}")
        print()
        print("MySQL Servers")
        report = self.get_report_for_name_prefix("mysql")
        for line in report:
            print(f" {line}")
        print()

    def data(self):
        report = self.get_data_report()
        for key in report:
            if self.available:
                print(key)
            else:
                print(f"{key}: {report[key]}")

    def get_data_report(self):
        self.inventory.set_hosts()
        self.inventory.set_host_vars()
        data_disks = {}
        data_disk_count = 0
        for host in self.inventory.host_vars_yaml:
            try:
                for data_disk in self.inventory.host_vars_yaml[host]["kvm"]["data_disks"]:
                    key = data_disk["name"]
                    if "data" in key:
                        data_disks[key] = host
                        data_disk_count += 1
            except KeyError:
                pass
                    
        report = {}
        index = 1
        while True:
            if index >= (data_disk_count + 10) or index > 1001:
                break
            key = f"data{index:04}"
            index += 1
            try:
                value = data_disks[key]
                # If we only want the next available, continue without adding
                # it to data_disks since this one is not available.
                if self.available:
                    continue
            except KeyError:
                # This one is available.
                value = ""
            report[key] = value
            if self.available and value == "":
                # Break - we found the first available.
                break

        return report

    def usable_ip(self, ip, network):
        network_address = str(network.net())
        broadcast_address = str(network.broadcast())
        try:
            probable_gateway = str(network[1])
        except IndexError:
            probable_gateway = None
        ip = str(ip)

        if self.ip_is_reserved(ip) or ip == network_address or ip == broadcast_address or ip == probable_gateway:
            return False

        return True

    def ip_is_reserved(self, ip):
        """
        Test if the IP is in a range that we may want to reserve for special purposes.
        """

        # This is a hacky hack - we are reserving IPs in our 204 range that are lower than
        # 64 for mail relay purposes (in case we want to expand the IP list in our SPF record).
        if str(ip).startswith("204."):
            pieces = str(ip).split(".")
            if int(pieces[3]) < 65:
                return True 
        return False 
      
    def ip(self):
        report = self.get_ip_report()

        margin = "  "
        if self.available is True:
            margin = ""
        for network in report:
            if self.available is False:
                print(f"{network}: {report[network]['summary']}")
            for ip in report[network]["ips"]:
                print(f"{margin}{ip}")

    def get_ip_report(self):
        self.inventory.set_hosts()
        self.inventory.set_host_vars()
        # Dict of IPs, keyed to IP with value of host it is assigned to.
        ip_hosts = {}
        # final var for report
        report = {}
        for host in self.inventory.host_vars_yaml:
            if "network" in self.inventory.host_vars_yaml[host]:
                for network in list(self.inventory.host_vars_yaml[host]['network']):
                    if "networks" in network:
                        for net in network["networks"]:
                            if "address" in net:
                                ip = IP(net["address"].split("/")[0])
                                if ip.version() == 4:
                                    ip_hosts[str(ip)] = host

        for location in self.inventory.hosts_yaml['all']['vars']['m_networks']:
            # We have to limit to just the defined network
            for network in self.inventory.hosts_yaml['all']['vars']['m_networks'][location]:
                if self.network and not network.startswith(self.network):
                    # Skip this network.
                    skip_network = True 
                    continue

                report[network] = {
                    "summary": "",
                    "ips": []
                }
                total = 0
                assigned = 0
                network_obj = IP(network)
                if network_obj.version() == 4:
                    for ip in network_obj:
                        total += 1
                        if str(ip) in ip_hosts:
                            if self.available is False:
                                assigned += 1
                                report[network]["ips"].append(f"{ip}: {ip_hosts[str(ip)]}")
                        else:
                            if self.usable_ip(str(ip), network_obj):
                                report[network]["ips"].append(str(ip))
                                if self.available is True:
                                    break
                    if self.available is False:
                        report[network]["summary"] = f"{assigned} / {total} assigned, {assigned/total*100}% in use"

        return report             
                             

    def show(self):
        self.inventory.set_hosts()
        self.inventory.set_host_vars()
        values = self.inventory.host_vars_yaml[self.host]
        print(self.host)
        if "kvm" in values:
            print(f"  Host: {values['kvm']['host']}")
            print(f"  CPUs: {values['kvm']['smp']}")
            print(f"  RAM: {values['kvm']['ram']}")
        else:
            # Gather all hosts on this server
            guests = []
            for server in list(self.inventory.host_vars_yaml.keys()):
                if "kvm" in self.inventory.host_vars_yaml[server]:
                    if self.inventory.host_vars_yaml[server]["kvm"]["host"] == self.host:
                        guests.append(server)
            if len(guests) > 0:
                print(f"  Hosts: {', '.join(guests)}") 
        # Gather all ips on this server
        ips = []
        for network in values['network']:
            if "networks" in network:
                for netblock in network['networks']:
                        if "address" in netblock:
                            ips.append(netblock['address'])
            
        if len(ips) > 0:
            print(f"  IPs: {', '.join(ips)}") 


        
    def hardware(self):
        self.inventory.set_hosts()
        headers = [ "host", "cpu", "mem", "no. guests", "no. disks", "size", "internal_disks" ]
        csv_writer = None
        csv_file = None
        if self.csv_path:
            if os.path.exists(self.csv_path):
                raise Exception(f"The CSV file path exists ({self.csv_path}). Please delete it and try again.")
            csv_file = open(self.csv_path, 'w', newline='')
            csv_writer = csv.writer(csv_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(headers)

        screen_data = []
        metal_hosts = list(self.inventory.hosts_yaml['all']['children']['metal']['hosts'].keys())
        telehouse_hosts = list(self.inventory.hosts_yaml['all']['children']['telehouse']['hosts'].keys())
        hosts = []
        for host in metal_hosts:
            # Limit to telehouse
            if host in telehouse_hosts:
                hosts.append(host)
        # Now we add a couple hosts not under ansible so we have a complete picture
        hosts.append('franz.mayfirst.org')
        hosts.append('baubo.mayfirst.org')
        count = len(hosts)
        print(f"Found {count} servers.")
        for host in hosts:
            print(".", end='', flush=True)
            if self.host and self.host != host:
                continue
            cpu_cmd = "cat /proc/cpuinfo | grep ^processor | tail -n1 | cut -d: -f2 | tr -d ' '"
            mem_cmd = "free -g | grep ^Mem: | awk '{print $2}'"
            kvm_cmd = "kvm-status | grep -v inactive |egrep '^[0-9]' |wc -l"
            disks_cmd = "cat /proc/partitions  | egrep 'nvme[0-9]n1$|sd[a-z]$' |wc -l"
            cmd = f'ssh root@{host} "{cpu_cmd} && {mem_cmd} && {kvm_cmd} && {disks_cmd}"'
            result = subprocess.run(cmd, shell=True, check=True, capture_output=True, text=True)
            answers = result.stdout.split("\n")
            # CPU's are number from 0 so add 1 to the total.
            cpu = int(answers[0]) + 1
            mem = answers[1]
            kvm = answers[2]
            disks = int(answers[3])
            size = "1u"
            internal_disks = "no"
            if disks == 6:
                internal_disks = "yes"
            if disks > 6:
                size = "2u"

            # Special exceptions based on knowledge
            if host == 'linda.mayfirst.org':
                size = '2u'
                internal_disks = 'no'
            elif host == 'baubo.mayfirst.org':
                kvm = 4
            elif host == 'franz.mayfirst.org':
                kvm = 1

            if csv_writer:
                csv_writer.writerow([host, cpu, mem, kvm, disks, size, internal_disks])
            screen_data.append([host, cpu, mem, kvm, disks, size, internal_disks])

        print()
        print(tabulate(screen_data, headers=headers))
        if csv_file:
            csv_file.close()

            
