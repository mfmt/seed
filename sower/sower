#!/usr/bin/python3

# sower is a May First created helper script to run routine tasks on our
# inventory, such as executing ansible playbook commands, listing our servers,
# looking up available IP addresses, etc.
# 
# It takes a subcommand and arguments. Each subcommand has a complimentary
# sower file.
#
# Run without arguments to get usage output.

import argparse
import sowerinventory
import sowerplaybook
import sowerdocker
import sowerred
import sowerls
import sowerreport
import sowergenerate
import os
import getpass

# We keep track of two directories - one is the seed directory, which is the
# directory containing this code and the roles, etc. The other is the inventory
# directory, which contains the server definitions. When using INVENTORY_LIVE
# the inventory directory will be stored somewhere else in the file system.
# When using INVENTORY_DEV it should be set to `inventories/dev` in this repo.

# Keep track of this repo's directory so we can derive others from it.
SEED_DIR = seed_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

DEFAULT_SUITE="bookworm"

# Parse the command line options.
parser = argparse.ArgumentParser(description='Run ansible code for May First.')
parser.add_argument('--live', help='By default, run on dev inventory, specify --live to run on live inventory', action='store_true')
parser.add_argument('--validation-level', help='Validate the inventory prior to running, defaults to 1 (minimal). Set to 0 for none or 2 for full.')
parser.set_defaults(live=False, validation_level=sowerinventory.SowerInventory.VALIDATION_LEVEL_MINIMAL)

# See: https://stackoverflow.com/questions/18282403/argparse-with-required-subcommands
# Ensure this works with different versions of python
#subparsers = parser.add_subparsers(required=True)
subparsers = parser.add_subparsers()
subparsers.required = True
subparsers.dest = 'command'

### Playbook code.
def playbook(args):
  pb = sowerplaybook.SowerPlaybook()
  pb.inventory = inventory
  if args.live == True:
    pb.playbook = "site-live.yml"
    pb.secrets = args.secrets
  else:
    pb.playbook = "site-dev.yml"
    pb.secrets = True
  pb.seed_dir = SEED_DIR
  pb.tags = args.tags
  if args.hosts:
    pb.hosts = args.hosts
  pb.main()

parser_playbook = subparsers.add_parser('playbook', help='Run the live playbook')
parser_playbook.add_argument('--tags', help='Limit to tags, repeat as necessary for multiple tags', default=[], action='append')
parser_playbook.add_argument('--secrets', help='Whether or not to include ansible vault, defaults to no on live, always yes on dev.', action='store_true')
parser_playbook.add_argument('hosts', nargs='*', help='Limit to hosts, repeat as necessary for multiple hosts', default=[])
parser_playbook.set_defaults(func=playbook, secrets=False )

## Initializing a docker dev environment
def docker_init(args):
    docker = sowerdocker.SowerDocker()
    docker.inventory = inventory 
    docker.seed_dir = SEED_DIR
    if args.live:
        raise RuntimeError("You can't specify live when initializing a development docker environment.")
    docker.rebuild = args.rebuild
    docker.commit = args.commit
    docker.apt_proxy = args.apt_proxy
    docker.suite = args.suite
    docker.limit_to_host = args.limit_to_host
    docker.initialize()

parser_docker_init = subparsers.add_parser('docker:init', help='Initialize development docker environment')
parser_docker_init.add_argument('--rebuild', help='Force rebuild of all hosts, specify either containers or images depending on what you want rebuilt.', choices=[sowerdocker.REBUILD_CONTAINERS, sowerdocker.REBUILD_IMAGES])
parser_docker_init.add_argument('--apt-proxy', help='Specify an Apt proxy to use.')
parser_docker_init.add_argument('--commit', help='Ensure all containers are started and then save them as images to be used next time.', action='store_true')
parser_docker_init.add_argument('--suite', help='Specify a debian suite (buster, bullseye, etc).')
parser_docker_init.add_argument('--limit-to-host', help='Only start/rebuild the given host.')
parser_docker_init.set_defaults(func=docker_init, rebuild=None, commit=False, suite=DEFAULT_SUITE, limit_to_host=None)

## Produce simple lists of servers matching the given criteria
def ls_ls(args):
    ls = sowerls.SowerLs()
    ls.inventory = inventory
    ls.seed_dir = SEED_DIR
    ls.pattern = args.pattern
    if args.display:
        ls.display = args.display
    if args.separator:
        ls.separator = args.separator

    ls.ls()

parser_ls_ls = subparsers.add_parser('ls', help='List servers matching the given criteria')
parser_ls_ls.add_argument('pattern', help='Limit to ansible pattern (e.g. all:!external)', nargs='?')
parser_ls_ls.add_argument('--display', help='In addition to the name, display this attribute. Comma separate multiple fields. E.g. --display kvm.host,network.0.networks.0.address.')
parser_ls_ls.add_argument('--separator', help='When displaying multiple attributes, separate values using this string. Defaults to a colon.', default=':')

parser_ls_ls.set_defaults(func=ls_ls, pattern="all")

## Communicate with our red control panel
def red_sync(args):
    if not args.live:
        raise RuntimeError("You can only sync the live environment.")
    red = sowerred.SowerRed()
    red.inventory = inventory 
    red.username = args.username
    if args.url:
        red.url = args.url
    if args.password:
        red.password = args.password
    else:
        red.password = getpass.getpass("Please enter your control panel password: ")
    red.sync()

parser_red_sync = subparsers.add_parser('red:sync', help='Sync inventory with red control panel.')
parser_red_sync.add_argument('--username', help='Username to login to red control panel with', required=True)
parser_red_sync.add_argument('--password', help='Password to login to red control panel with, if blank you will be prompted')
parser_red_sync.add_argument('--url', help='URL of red API, if blank will be set to default')
parser_red_sync.set_defaults(func=red_sync)

def red_grant(args):
    if not args.live:
        raise RuntimeError("You can only set grants in the live environment.")
    red = sowerred.SowerRed()
    red.inventory = inventory 
    if args.host:
        red.host = args.host
    red.grant()

parser_red_grant = subparsers.add_parser('red:grant', help="Grant the given host's red database user access to the seso database so it can do it's job.")
parser_red_grant.add_argument('--host', help='The fully qualified host name of the red host to grant access.', required=True)
parser_red_grant.set_defaults(func=red_grant)

def report_hardware(args):
    if not args.live:
        raise RuntimeError("You can only generate a hardware report in the live environment.")
    report = sowerreport.SowerReport()
    report.inventory = inventory 
    if args.host:
        report.host = args.host
    if args.csv_path:
        report.csv_path = args.csv_path
    report.hardware()

parser_report_hardware = subparsers.add_parser('report:hardware', help="Print CSV output documenting our physical server hardware.")
parser_report_hardware.add_argument('--host', help='The fully qualified host name of the the physical server to report on or empty to report on all.')
parser_report_hardware.add_argument('--csv-path', help='The path to the file you want created as a csv file with the results. Must not exist.')
parser_report_hardware.set_defaults(func=report_hardware)

def report_map(args):
    report = sowerreport.SowerReport()
    report.inventory = inventory 
    report.map()

parser_report_map = subparsers.add_parser('report:map', help="Map relationship between shared servers (e.g. webproxies, MX servers, etc) and domain names (e.g. a.webproxy, a.mx, etc).")
parser_report_map.set_defaults(func=report_map)

def report_ip(args):
    report = sowerreport.SowerReport()
    report.inventory = inventory 
    report.network = args.network
    report.available = args.available
    report.ip()

parser_report_ip = subparsers.add_parser('report:ip', help="Print out all IP usage based on assignments in our inventory, can be used to find the next available ip.")
parser_report_ip.add_argument('--network', help='Limit to the given network, e.g. 10.9.67.0 - you only need to specify the beginning, e.g. 10 is enough, or 204.')
parser_report_ip.add_argument('--available', help='By default, show all IPs, specify --available to only show the next available IP', action='store_true')
parser_report_ip.set_defaults(func=report_ip, next=False)

def report_data(args):
    report = sowerreport.SowerReport()
    report.inventory = inventory 
    report.available = args.available
    report.data()

parser_report_data = subparsers.add_parser('report:data', help="Print out all data disks in order, can be used to find the next available number.")
parser_report_data.add_argument('--available', help='By default, show all data disks, specify --available to only show the next available data disk', action='store_true')
parser_report_data.set_defaults(available=False, func=report_data)

def report_show(args):
    report = sowerreport.SowerReport()
    report.inventory = inventory 
    report.host = args.host
    report.show()

parser_report_show = subparsers.add_parser('report:show', help="Print info about a given host.")
parser_report_show.add_argument('--host', help='The fully qualified host name of the server to report on.', required=True)
parser_report_show.set_defaults(func=report_show)

def generate_weborigin(args):
    generate = sowergenerate.SowerGenerate()
    generate.parent = args.parent
    generate.inventory = inventory 
    generate.username = args.username
    generate.password = args.password
    generate.host = args.host
    generate.seed_dir = SEED_DIR
    generate.weborigin()

parser_generate_weborigin = subparsers.add_parser('generate:weborigin', help="Generate a weborigin yaml file based on template.")
parser_generate_weborigin.add_argument('--parent', help='The fully qualified host name of the host server.', required=True)
parser_generate_weborigin.add_argument('--host', help='The fully qualified host name of the weborigin you would like to create. Optional. The next available name will be chosen if left empty.')
parser_generate_weborigin.add_argument('--username', help='Username to login to red control panel with, if blank you will be prompted')
parser_generate_weborigin.add_argument('--password', help='Password to login to red control panel with, if blank you will be prompted')
parser_generate_weborigin.set_defaults(func=generate_weborigin)

def generate_mailstore(args):
    generate = sowergenerate.SowerGenerate()
    generate.parent = args.parent
    generate.inventory = inventory
    generate.username = args.username
    generate.password = args.password
    generate.host = args.host
    generate.seed_dir = SEED_DIR
    generate.mailstore()

parser_generate_mailstore = subparsers.add_parser('generate:mailstore', help="Generate a mailstore yaml file based on template.")
parser_generate_mailstore.add_argument('--parent', help='The fully qualified host name of the host server.', required=True)
parser_generate_mailstore.add_argument('--host', help='The fully qualified host name of the mailstore you would like to create. Optional. The next available name will be chosen if left empty.')
parser_generate_mailstore.add_argument('--username', help='Username to login to red control panel with, if blank you will be prompted')
parser_generate_mailstore.add_argument('--password', help='Password to login to red control panel with, if blank you will be prompted')
parser_generate_mailstore.set_defaults(func=generate_mailstore)

def generate_mysql(args):
    generate = sowergenerate.SowerGenerate()
    generate.parent = args.parent
    generate.inventory = inventory
    generate.username = args.username
    generate.password = args.password
    generate.host = args.host
    generate.seed_dir = SEED_DIR
    generate.mysql()

parser_generate_mysql = subparsers.add_parser('generate:mysql', help="Generate a mysql yaml file based on template.")
parser_generate_mysql.add_argument('--parent', help='The fully qualified host name of the host server.', required=True)
parser_generate_mysql.add_argument('--host', help='The fully qualified host name of the mysql you would like to create. Optional. The next available name will be chosen if left empty.')
parser_generate_mysql.add_argument('--username', help='Username to login to red control panel with, if blank you will be prompted')
parser_generate_mysql.add_argument('--password', help='Password to login to red control panel with, if blank you will be prompted')
parser_generate_mysql.set_defaults(func=generate_mysql)

def generate_webproxy(args):
    generate = sowergenerate.SowerGenerate()
    generate.parent = args.parent
    generate.inventory = inventory
    generate.username = args.username
    generate.password = args.password
    generate.host = args.host
    generate.seed_dir = SEED_DIR
    generate.webproxy()

parser_generate_webproxy = subparsers.add_parser('generate:webproxy', help="Generate a webproxy yaml file based on template.")
parser_generate_webproxy.add_argument('--parent', help='The fully qualified host name of the host server.', required=True)
parser_generate_webproxy.add_argument('--host', help='The fully qualified host name of the webproxy you would like to create. Optional. The next available name will be chosen if left empty.')
parser_generate_webproxy.add_argument('--username', help='Username to login to red control panel with, if blank you will be prompted')
parser_generate_webproxy.add_argument('--password', help='Password to login to red control panel with, if blank you will be prompted')
parser_generate_webproxy.set_defaults(func=generate_webproxy)

def generate_generic(args):
    generate = sowergenerate.SowerGenerate()
    generate.parent = args.parent
    generate.inventory = inventory
    generate.username = args.username
    generate.password = args.password
    generate.host = args.host
    generate.seed_dir = SEED_DIR
    generate.generic()

parser_generate_generic = subparsers.add_parser('generate:generic', help="Generate a generic yaml file based on template.")
parser_generate_generic.add_argument('--parent', help='The fully qualified host name of the host server.', required=True)
parser_generate_generic.add_argument('--host', help='The fully qualified host name of the generic you would like to create. Optional. The next available name will be chosen if left empty.', required=True)
parser_generate_generic.add_argument('--username', help='Username to login to red control panel with, if blank you will be prompted')
parser_generate_generic.add_argument('--password', help='Password to login to red control panel with, if blank you will be prompted')
parser_generate_generic.set_defaults(func=generate_generic)

def generate_fstab(args):
    generate = sowergenerate.SowerGenerate()
    generate.parent = args.parent
    generate.inventory = inventory 
    generate.host = args.host
    generate.fstab()

parser_generate_fstab = subparsers.add_parser('generate:fstab', help="Make filesystems and generate fstab on the given host.")
parser_generate_fstab.add_argument('--parent', help='The fully qualified host name of the host server.', required=True)
parser_generate_fstab.add_argument('--host', help='The fully qualified host name of the weborigin you would like to create. Optional. The next available name will be chosen if left empty.')
parser_generate_fstab.set_defaults(func=generate_fstab)

# Main program execution.
args = parser.parse_args()
inventory = sowerinventory.SowerInventory()
inventory.live = args.live
inventory.validation_level = args.validation_level
inventory.seed_dir = SEED_DIR
inventory.set_inventory_dir()

args.func(args)

