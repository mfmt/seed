# Allow communication and sync'ing between our ansible repo and our control panel.
from jinja2 import Environment, FileSystemLoader
import sowerreport
import sowerred
import os
import getpass
import sowerred
import random
import subprocess
import re

class SowerGenerate():
    seed_dir = None
    inventory = None
    parent = None
    username = None
    password = None
    host = None
    # MySQL data disks should be mounted to these locations.
    mysql_data_disk_targets = [ '/var/lib/mysql', '/var/local/db-backup', '/tmp' ]
    # Webproxy data disks
    webproxy_data_disk_targets = [ '/var/lib/nginx', '/var/lib/nginx/cache', '/var/log/nginx' ]

    def get_data_disks(self, template):
        """
        Data disks are numbered incrementally, e.g. data0001, data0002, etc. This method
        finds the next available data disks that are not in use.
        """
        report = sowerreport.SowerReport()
        report.inventory = self.inventory
        data_report = report.get_data_report()
        values = []
        count = 0
        total_disks = 5

        if template.startswith('mysql'):
            # mysql servers have 3 data disks
            total_disks = 3
        elif template.startswith('webproxy'):
            total_disks = 3

        # We have to specify a unique alphabetical index for each disks. The
        # root disk (which is not part of this array) is A, so we start
        # counting with B.
        index_map = {
            0: "B",
            1: "C",
            2: "D",
            3: "E",
            4: "F",
        }
        for key in data_report:
            # If there is no value for this data disk, then it's available to use.
            if not data_report[key]:
                size = None
                driver = None
                comment = None
                if template.startswith('mailstore') or template.startswith('weborigin'):
                    size = '250g'
                    driver = "virtio-blk-pci"
                elif template.startswith('mysql'):
                    comment = f"# Used for {self.mysql_data_disk_targets[count]}" 
                    if count == 0:
                        size = "500g"
                        driver = "scsi-hd"
                    elif count == 1:
                        size = "100g"
                        driver = "virtio-blk-pci"
                    elif count == 2:
                        size = "10g"
                        driver = "scsi-hd"
                elif template.startswith('webproxy'):
                    comment = f"# Used for {self.webproxy_data_disk_targets[count]}" 
                    if count == 0:
                        size = "10g"
                        driver = "scsi-hd"
                    elif count == 1:
                        size = "10g"
                        driver = "virtio-blk-pci"
                    elif count == 2:
                        size = "50g"
                        driver = "virtio-blk-pci"
   
                index = index_map[count]
                count += 1
                disk_spec = {
                    "name": key,
                    "vg": "vg0",
                    "size": size,
                    "comment": comment,
                    "index": index,
                    "driver": driver,
                }

                values.append(disk_spec)
                if len(values) == total_disks:
                    break

        return values

    def get_next_ip(self, network):
        """
        Get the next unused IP address in the given network.
        """
        report = sowerreport.SowerReport()
        report.inventory = self.inventory
        report.available = True
        report.network = network
        out = report.get_ip_report()
        return list(out.values())[0]['ips'][0]

    def get_next_mysql_hostgroup_id(self):
        """
        Every mysql server has a unique hostgroup id so proxy sql can tell it
        apart from the others. They are numbered incrementally. This method gets
        the next available number.
        """
        self.inventory.set_host_vars()

        # If we already have a host, use it.
        if self.host:
            if self.host in self.inventory.host_vars_yaml:
                return int(self.inventory.host_vars_yaml[self.host]['proxysql']['hostgroup_id'])

        numbers = []
        for host in self.inventory.host_vars_yaml:
            if 'proxysql' in self.inventory.host_vars_yaml[host]:
                numbers.append(int(self.inventory.host_vars_yaml[host]['proxysql']['hostgroup_id']))
        numbers.sort()
        return numbers.pop() + 1

    def get_next_host_number(self, host_type):
        """
        Hosts are counted incrementally (e.g. weborigin001,
        weborigin002, etc.). We want to figure out which number comes next.
        """
        self.inventory.set_host_vars()
        numbers = []
        for hostfile in self.inventory.host_vars_yaml:
            if hostfile.startswith(host_type):
                hostname = hostfile.split('.')[0]
                if hostname.startswith('weborigin999'):
                    # weborigin999 is a special weborigin for compromised sites, don't count
                    # it as part of our normal increment-by-one pattern.
                    continue
                numbers.append(int(hostname.replace(host_type, '')))
        if len(numbers) == 0:
            raise RuntimeError("Failed to find next hosst number.")
        numbers.sort()
        return numbers.pop() + 1

    def get_uuid(self, device, host, format=True):
        print(f"Running blkid on {host} for {device}")
        cmd = [ "ssh", f"root@{host}", "blkid", device ]
        result = subprocess.run(
            cmd,
            capture_output=True,
            text=True,
            check=False
        )
        if result.returncode != 0:
            if not format:
                return None

            # This block device is not formatted.
            print(f"{device} is not yet formatted, adding ext4 file system.")
            cmd = [ "ssh", f"root@{self.host}", "mkfs", "-t", "ext4", device]
            subprocess.run(
                cmd,
                capture_output=True,
                text=True,
                check=True
            )

            # Now try again
            return self.get_uuid(device, host=host)

        result_parts = result.stdout.strip().split(" ")
        uuid_parts = result_parts[1].split("=")
        if uuid_parts[0] == 'PTUUID':
            # This block device is partitioned, so it must be the
            # root/OS partition. Ignore.
            return None
        return uuid_parts[1].strip('"')


    def fstab(self):
        """
        Generate /etc/fstab on the given host.
        """
        self.inventory.set_inventory_dir()
        self.inventory.set_hosts()
        self.inventory.set_host_vars()

        cmd = [ "ssh", f"root@{self.host}", "ls", "/dev" ]
        print(f"Reading /dev on {self.host}")
        result = subprocess.run(
            cmd,
            capture_output=True,
            text=True,
            check=False
        )
        all_devices = result.stdout.strip().split('\n')
        pattern = re.compile(r'^(sd|vd)[a-z]$')
        devices = [f"/dev/{device}" for device in all_devices if pattern.match(device)]
        uuids = []
        for device in devices:
            # We simply want to ensure all devices have a UUID (which indicates they are formatted)
            uuid = self.get_uuid(device, self.host)
            if uuid:
                uuids.append(uuid)

        # Now that all devices are formatted, we can check the UUID on the host so we can tell
        # which UUID belongs to which data drive name (e.g. data0023, etc).
        count = 0
        for disk in self.inventory.host_vars_yaml[self.host]["kvm"]["data_disks"]:
            # Increment counter first - we may continue depending on conditions below.
            count += 1
            parent_path = f"/dev/mapper/{disk['volume_group']}-{disk['name']}"
            uuid = self.get_uuid(parent_path, self.parent, format=False)
            if not uuid:
                print(f"Failed to find UUID for {parent_path}")
                continue
            if uuid not in uuids:
                print(f"The uuid ({uuid}) was found for {parent_path} on {self.parent}, but is not on host {self.host}")
                continue
            if self.host.startswith('weborigin') or self.host.startswith('mailstore'):
                host_path = f"/media/{disk['name']}"
                fstab_line = f"UUID={uuid} {host_path} ext4    defaults,usrjquota=aquota.user,jqfmt=vfsv1,nosuid,nodev       0       2"
            elif self.host.startswith('mysql'):
                host_path = self.mysql_data_disk_targets[count-1]
                fstab_line = f"UUID={uuid} {host_path} ext4    defaults      0       2"
            elif self.host.startswith('webproxy'):
                host_path = self.webproxy_data_disk_targets[count-1]
                fstab_line = f"UUID={uuid} {host_path} ext4    defaults      0       2"
            else:
                raise RuntimeError("Failed to determine host path and fstab line.")

            if self.host.startswith('mysql') and host_path == '/tmp':
                print("Ensure original /tmp is not present in /etc/fstab")
                # Our goal is to delete the /tmp line if the UUID doesn't match the line
                # we want.
                # This commands first checks if the UUID matches. If so,the b says skip to the
                # end (without deleting it). Next, we search for /tmp and delete
                # that line if present.
                cmd = [
                    "ssh",
                    f"root@{self.host}",
                    "sed",
                    "-i",
                    f"'/UUID={uuid}/b;/tmp /d'",
                    "/etc/fstab" 
                ]

                result = subprocess.run(
                    cmd,
                    check=True,
                )

            print(f"Checking for {uuid} in /etc/fstab.")
            cmd = [ "ssh", f"root@{self.host}", "grep", uuid, "/etc/fstab" ]
            result = subprocess.run(
                cmd,
                check=False,
                stdout=subprocess.PIPE
            )
            if result.returncode == 1:
                
                print(f"Adding line to fstab")
                cmd = [ "ssh", f"root@{self.host}", "tee", "-a", "/etc/fstab" ]
                subprocess.run(
                    cmd,
                    input=fstab_line + '\n',
                    text=True,
                    stdout=subprocess.PIPE
                )
            print(f"Ensuring {host_path} exists on {self.host}")
            cmd = [ "ssh", f"root@{self.host}", "mkdir", "-p", host_path ]
            subprocess.run(
                cmd,
                input=fstab_line + '\n',
                text=True,
                stdout=subprocess.PIPE
            )

        print("Running systemctl daemon-reload")
        cmd = [ "ssh", f"root@{self.host}", "systemctl", "daemon-reload" ]
        subprocess.run(
            cmd,
            stdout=subprocess.PIPE,
            check=True
        )

        if self.host.startswith('weborigin') or self.host.startswith('mailstore'):
            print("Ensuring quota package is installed.")
            cmd = [ "ssh", f"root@{self.host}", "apt-get", "-y", "install", "quota" ]
            subprocess.run(
                cmd,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                check=True
            )


        count = 0
        for disk in self.inventory.host_vars_yaml[self.host]["kvm"]["data_disks"]:
            count += 1
            if self.host.startswith('weborigin') or self.host.startswith('mailstore'):
                host_path = f"/media/{disk['name']}"
            elif self.host.startswith('mysql'):
                host_path = self.mysql_data_disk_targets[count-1]
            elif self.host.startswith('webproxy'):
                host_path = self.webproxy_data_disk_targets[count-1]
            else:
                raise RuntimeError("Failed to determine host path and fstab line.")

            print(f"Ensure {host_path} is mounted.")
            cmd = [ "ssh", f"root@{self.host}", "mount", host_path ]
            # Will throw error if mounted - returncode 32
            result = subprocess.run(
                cmd,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                check=False
            )
            if result.returncode != 0 and result.returncode != 32:
                print(f"ERROR: Something went wrong mounting {host_path}")
                return

            if self.host.startswith('weborigin') or self.host.startswith('mailstore'):
                print(f"Check if quotacheck has been run for {host_path}")
                cmd = [ "ssh", f"root@{self.host}", "test", "-e", f"{host_path}/aquota.user" ]
                result = subprocess.run(
                    cmd,
                    check=False
                )
                if result.returncode == 1:
                    print(f"Running quotacheck on {host_path}.")
                    cmd = [ "ssh", f"root@{self.host}", "quotacheck", "-cum", host_path ]
                    subprocess.run(
                        cmd,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                        check=True
                    )
                print(f"Check if quota is on for {host_path}")
                cmd = [ "ssh", f"root@{self.host}", "quotaon", "--user", "-p", host_path ]
                result = subprocess.run(
                    cmd,
                    stdout=subprocess.PIPE,
                    check=False,
                )
                if result.returncode == 0:
                    print(f"Turning on quota for {host_path}")
                    cmd = [ "ssh", f"root@{self.host}", "quotaon", host_path ]
                    subprocess.run(
                        cmd,
                        stdout=subprocess.PIPE,
                        check=True,
                    )

    def mysql(self):
        """
        Write out the next mysql yaml file.
        """

        # Determine host name.
        if self.host:
            host_parts = self.host.split('.')
            hostname = host_parts[0]
        else:
            next_number = self.get_next_host_number('mysql')
            hostname = f"mysql{next_number:03d}"

        hostgroup_id = self.get_next_mysql_hostgroup_id()

        template = 'mysql.yml.j2'
        self.create_host_yaml_file(hostname, template, hostgroup_id=hostgroup_id)
        print(f"You may need to restart {self.host} to ensure /tmp is properly mounted.")

    def webproxy(self):
        """
        Write out the next webproxy yaml file.
        """

        # Determine host name.
        if self.host:
            host_parts = self.host.split('.')
            hostname = host_parts[0]
        else:
            next_number = self.get_next_host_number('webproxy')
            hostname = f"webproxy{next_number:03d}"

        template = 'webproxy.yml.j2'
        self.create_host_yaml_file(hostname, template)

    def mailstore(self):
        """
        Write out the next mailstore yaml file.
        """

        # Determine host name.
        if self.host:
            host_parts = self.host.split('.')
            hostname = host_parts[0]
        else:
            next_number = self.get_next_host_number('mailstore')
            hostname = f"mailstore{next_number:03d}"

        template = 'mailstore.yml.j2'
        self.create_host_yaml_file(hostname, template)

    def generic(self):
        """
        Write out the next generic server yaml file.
        """

        # Determine host name.
        host_parts = self.host.split('.')
        hostname = host_parts[0]

        template = 'generic.yml.j2'
        self.create_host_yaml_file(hostname, template)

    def weborigin(self):
        """
        Write out the next weborigin yaml file.
        """

        # Determine host name.
        if self.host:
            host_parts = self.host.split('.')
            hostname = host_parts[0]
        else:
            next_number = self.get_next_host_number('weborigin')
            hostname = f"weborigin{next_number:03d}"

        template = 'weborigin.yml.j2'
        self.create_host_yaml_file(hostname, template)

    def create_host_yaml_file(self, hostname, template, hostgroup_id=None):
        """
        Create a host yaml file with the given hostname, using the given template.
        """
        # Derive some values from our inventory.
        self.inventory.set_inventory_dir()
        self.inventory.set_hosts()
        self.inventory.set_host_vars()
        ip_public_network = self.inventory.hosts_yaml['all']['vars']['sower_ip_public_network']
        ip_private_network = self.inventory.hosts_yaml['all']['vars']['sower_ip_private_network']
        tld = self.inventory.hosts_yaml['all']['vars']['m_tld']

        # Determine IP address.
        ip_public_address = None
        ip_private_address = None
        if self.host:
            # If host is defined, attempt to derive already assigned IPs
            try:
                ip_public_address = self.inventory.host_vars_yaml[self.host]["network"][0]["networks"][0]['address'].split('/')[0]
            except KeyError:
                pass
            try:
                ip_private_address = self.inventory.host_vars_yaml[self.host]["network"][0]["networks"][1]['address'].split('/')[0]
            except KeyError:
                pass

        # Otherwise, determine the next available one.
        if not ip_public_address:
            ip_public_address = self.get_next_ip(ip_public_network)
        if not ip_private_address:
            ip_private_address = self.get_next_ip(ip_private_network)

        context = {
            "ip_public_prefix": self.inventory.hosts_yaml['all']['vars']['sower_ip_public_prefix'],
            "ip_public_netmask": self.inventory.hosts_yaml['all']['vars']['sower_ip_public_netmask'],
            "ip_public_gateway": self.inventory.hosts_yaml['all']['vars']['sower_ip_public_gateway'],
            "ip_public_address": ip_public_address,
            "ip_private_prefix": self.inventory.hosts_yaml['all']['vars']['sower_ip_private_prefix'],
            "ip_private_netmask": self.inventory.hosts_yaml['all']['vars']['sower_ip_private_netmask'],
            "ip_private_address": ip_private_address,
            "backup_hostname": self.inventory.hosts_yaml['all']['vars']['sower_backup_hostname'],
            "backup_hour": random.randint(5, 10),
            "data_disks": self.get_data_disks(template),
            "hostname": hostname,
            "parent": self.parent,
            "tld": tld,
            "hostgroup_id": hostgroup_id
        }

        output_path = f"{self.inventory.inventory_dir}/host_vars/{hostname}.mayfirst.{tld}.yml"
        write_file = False
        write_file_answer = None
        while write_file_answer not in ["y", "n"]:
            write_file_answer = input(f"Create {output_path}? [y/n] ").strip()
            if write_file_answer == "n":
                break
            elif write_file_answer == "y":
                if os.path.exists(output_path):
                    overwrite_answer = None
                    while overwrite_answer not in ["y", "n"]:
                        overwrite_answer = input(f"Overwrite existing file? [y/n] ")
                        if overwrite_answer == "n":
                            break
                        elif overwrite_answer == 'y':
                            write_file = True
                else:
                    write_file = True

        if write_file:
            env = Environment(loader=FileSystemLoader(f"{self.seed_dir}/sower/templates"))
            template = env.get_template(template)
            rendered_content = template.render(context)

            with open(output_path, 'w') as output_file:
                output_file.write(rendered_content)
            print(f"Created host file:")
            print(output_path)
            print("Please review to ensure all values are correct.")
        else:
            print("File was not written.")

        answer = None
        while answer not in ["y", "n"]:
            answer = input(f"Create dns record for {hostname}.mayfirst.{tld} <=> {ip_public_address}? [y/n] ")
            if answer == "n":
                print("Did not create DNS record.")
            else:
                if self.username is None:
                    self.username = input(f"Enter your control panel username: ")
                if self.password is None:
                    self.password = getpass.getpass("Please enter your control panel password: ")
                red = sowerred.SowerRed()
                if tld == 'dev':
                    red.url = 'https://red.loc.cx/api.php'
                red.inventory = self.inventory 
                red.username = self.username
                red.password = self.password
                if not red.dns_record_exists('a', f'{hostname}.mayfirst.{tld}', ip_public_address):
                    red.create_dns_record('a', f'{hostname}.mayfirst.{tld}', ip_public_address)
                    print("Created A record.")
                else:
                    print("DNS A record already exists.")
                if not red.dns_record_exists('ptr', f'{hostname}.mayfirst.{tld}', ip_public_address):
                    red.create_dns_record('ptr', f'{hostname}.mayfirst.{tld}', ip_public_address)
                    print("Created PTR record.")
                else:
                    print("DNS PTR record already exists.")




