import os
import sys
import subprocess


class SowerPlaybook:
    inventory = None 
    seed_dir = None
    playbook = None
    tags = []
    hosts = [ 'all:!external' ]
    secrets = False
    
    def main(self):
        # Calculate variables.
        playbook = self.seed_dir + "/" + self.playbook
        inventory_path = self.inventory.inventory_dir + "/hosts.yml"
        ansible_config = self.seed_dir + "/ansible.cfg"
        self.inventory.validate()

        args = [];
        args.append('ansible-playbook')
        args.append('--inventory')
        args.append(inventory_path)

        if self.tags:
            args.append('--tags')
            args.append(",".join(self.tags))

        if self.secrets:
            args.append('-e')
            args.append("@{0}/secrets.yml".format(self.inventory.inventory_dir))
            secrets_file = self.inventory.inventory_dir + "/secret.txt"
            if os.path.exists(secrets_file):
                args.append('--vault-password-file')
                args.append(secrets_file)
            else:
                args.append("--ask-vault-password")
        if self.hosts:
            args.append('--limit')
            args.append(" ".join(self.hosts))
        
        args.append(playbook)
        print("Executing:")
        print(" ".join(args))

        # Include our environment variables so we can find things like our SSH agent paths.
        env = os.environ.copy()

        # Ensure we include our ansible.cfg file - even if we are running this command
        # from a different directory.
        env["ANSIBLE_CONFIG"] = ansible_config
        subprocess.check_call(args, stdout=sys.stdout, stderr=subprocess.STDOUT, env=env)

