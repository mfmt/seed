#!/usr/bin/python3

# This script helps build either a dev or testing environment by creating and
# instatiating docker containers on your workstation.
#
# `initialize-docker-environment.py` ensures you have a working base image, and a
# working network. Then, it creates and starts indentical docker containers
# named after each kind of host that is defined in the ansible host file.

import os
import sys
import yaml
import subprocess
import time

# Allowed rebuild options
REBUILD_CONTAINERS = 'containers'
REBUILD_IMAGES = 'images'

class SowerDocker():
    inventory = None
    seed_dir = None
    apt_server = None
    # tuple of name and ip of hosts to build
    hosts = [] 
    # optionally limit to just the given host
    limit_to_host = None
    apt_server = "http.us.debian.org/debian/"
    base_net = "172.18.0"
    domain_names = []
    suite = "bookworm"
    rebuild = None 
    commit = False

    def initialize(self):
        self.inventory.set_hosts() 
        self.inventory.set_host_vars() 

        try:
          with open(self.seed_dir + "/docker/id_rsa.pub") as file:
              pass
        except FileNotFoundError:
            print("Failed to find your id_rsa.pub file in the docker folder. Please add your ssh public key so you can ssh into each docker image.")
            return False

        self.parse_data(self.inventory.hosts_yaml, self.inventory.host_vars_yaml)
            
        if self.apt_proxy:
            if not self.apt_proxy.endswith('/'):
                self.apt_proxy = apt_proxy + '/'
            self.apt_server = self.apt_proxy + self.apt_server
        if self.rebuild: 
            print(f"Forcing rebuild of {self.rebuild}.")

        self.build()
        return True

    # Parse out the hosts_yaml and host_vars_yaml dicts to create both a list
    # of docker containers to build as well as a list of domain name/IP entries
    # that should be added to /etc/hosts.
    def parse_data(self, hosts_yaml, host_vars_yaml):
        seen_hosts = []
        for group, group_data in hosts_yaml['all']['children'].items():
            if 'hosts' in group_data and group_data['hosts']:
                for host, host_data in group_data['hosts'].items():
                    if host not in seen_hosts:
                        if self.limit_to_host and self.limit_to_host != host:
                            continue
                        seen_hosts.append(host)
                        # In the hosts.yml file we just have keys with no data, e.g.:
                        # hosts:
                        #   server001.mayfirst.dev:
                        #   server002.mayfirst.dev:
                        # So we have to copy in the vars from the host_vars_yaml file.
                        try:
                            host_data = host_vars_yaml[host]
                        except KeyError:
                            raise RuntimeError("I can't find a yml file for the host {0}. Please create it in host_vars and ensure it ends in .yml".format(host))
                        # Pluck out the first address and get rid of the network part.
                        ip = host_data['network'][0]['networks'][0]['address'].split('/', 1)[0]
                        # Take the hostname without the domain name.
                        name = host.split('.', 1)[0]

                        # Build a docker container with it
                        self.hosts.append((name, ip))

                        # Add hostname to /etc/hosts file
                        self.domain_names.append((host, ip))

                        # Check for additional bootstrapped dns entries
                        if 'knot_bootstrap' in host_data:
                            for dns_tuple in host_data['knot_bootstrap'].items():
                                self.domain_names.append((dns_tuple[0], dns_tuple[1]))

    def build(self):
        if not self.build_os_image():
            # If we don't have a os image, can't continue.
            return False
        self.build_seed_network()
        self.build_seed_base_image()
        if self.check_etc_hosts_file():
            # Only build and start if hosts file is complete. Otherwise
            # we can't login to restart networking.
            self.build_and_start_containers()
        else:
            print("Please make the etc host file changes and re-run.")

    # Run command, return True on returncode 0 or raise error. 
    def execute(self, args, cwd = None):
        result = subprocess.run(args, cwd = cwd)
        if result.returncode != 0:
            raise RuntimeError("Failed to run command. Args: {0}".format(args))
        return True

    # Execute shell command, return true if return code is 0, false if 1, raise
    # error otherwise.
    def shell(self, command):
        result = subprocess.run(command, shell=True, stdout=subprocess.DEVNULL)
        if result.returncode == 0:
            return True
        elif result.returncode == 1:
            return False
        else:
            raise RuntimeError("Failed to run shell command. Error: {0}".format(result.stderr))

    def network_exists(self):
        return self.shell("docker network ls | grep seed")
        
    def image_exists(self, image):
        return self.shell("docker images {0} | grep {0}".format(image))

    def container_is_running(self, container):
        return self.shell("docker ps | grep '{0}$'".format(container))

    def container_is_built(self, container):
        return self.shell("docker ps -a | grep '{0}$'".format(container))

    def build_seed_network(self):
        if self.network_exists():
            print("The network already exists.")
            return True
        # We should pass --ipv6 but that seems broken:
        # https://github.com/moby/moby/issues/28055
        return self.execute(["docker", "network", "create", "--subnet={0}.0/16".format(self.base_net), "seed"])

    def build_os_image(self):
        if self.image_exists("my-{0}".format(self.suite)):
            print("OS image already built.")
            return True

        print("Please run the ./build-os-image script in this directory as root to build your base image for the {0} suite.".format(self.suite))
        return False

    def build_seed_base_image(self):
        if self.rebuild != REBUILD_IMAGES and self.image_exists("seed-base"):
            print("The seed-base image already exists.")
            return True
        path = self.seed_dir + "/docker"
        return self.execute(["docker", "build", "-t", "seed-base" , "./"], cwd=path)
        
    def build_container(self, name, ip):
        if self.rebuild:
            if self.container_is_running(name):
                subprocess.run(["docker", "stop", name], check=True)
            if self.container_is_built(name):
                subprocess.run(["docker", "rm", name], check=True)
        else:
            if self.container_is_built(name):
                print("The {0} container is already built".format(name))
                return True

        # Check for an image for this particular container.
        image = f"seed-{name}"
        if not self.image_exists(image) or self.rebuild == REBUILD_IMAGES:
            # Use our seed base image so we start from scratch.
            image = "seed-base"
            print("Rebuilding image based on seed-base")
        else:
            print(f"Using existing {image} image")

        print("Building container {0}".format(name))
        args = [
            "docker",
            "create",
            "--net",
            "seed",
            "--ip",
            ip,
            # FIXME - We should be mounting /sys/fs/cgroups in read-only mode and we should not
            # be passing --cgroups=host. These two changes are a work-around for what seems to be
            # a bug in systemd's use of cgroups v2.
            # see https://serverfault.com/questions/1053187/docker-container-does-not-work-without-cgroupns-host-with-cgroup-v2
            # Also, does this work with earlier versions of systemd that do not default to cgroups v2?
            ## Original:
            #"-v", #"/sys/fs/cgroup/systemd:/sys/fs/cgroup/systemd:ro",
            ## Fixed per link above:
            "-v", "/sys/fs/cgroup:/sys/fs/cgroup:rw",
            "--cgroupns", "host",
            '--cap-add', 'NET_ADMIN',
            #
            # And, the link above has more recent posts suggesting:
            # '--privileged',
            #'--cap-add', 'SYS_ADMIN',
            #'--security-opt', 'seccomp=unconfined',
            #'--cgroup-parent=docker.slice',
            #'--cgroupns', 'private',
            # BUT... when I run --privileged it fully borks my laptop and
            # when I run everything but privileged the container is built
            # but does not start.
            "-v", self.seed_dir + "/docker/resolv.conf:/etc/resolv.conf",
            "--tmpfs", "/run:size=100M",
            "--tmpfs", "/run/lock:size=100M",
            "--tmpfs", "/tmp:exec,size=100M",
            "--hostname", name,
            "--name", name
        ]

        args.append(image)
        return self.execute(args)
        
    def commit_container(self, name):
        if not self.commit:
            return True
        image_name = f"seed-{name}"
        args = [
            "docker",
            "commit",
            name,
            image_name 
        ]
        print(f"Committing {image_name}")
        return self.execute(args)

    def start_container(self, name):
        if self.container_is_running(name):
            print("The {0} container is already running".format(name))
            return True
        self.execute(["docker", "start", name])
        if self.rebuild is None:
            self.restart_services(name)
        
    def restart_services(self, name):
        # This kills me but... something with docker and systemd seems to prevent some systemd services
        # from satisfying the wait-online service which prevents knot and possibly other services
        # from starting properly.

        # add tuple of server name, service to restart, e.g. ('mailmx001', 'postfix.service')
        restarts = [
            ("nsauth001", "knot.service"),
            ("mailcf001", "saslauthd.service"),
            ("red001", "nginx.service"),
        ]
        time.sleep(2)
        try:
            # All images get systemd-network restarted.
            self.execute(["ssh", f"root@{name}.mayfirst.dev", "systemctl", "restart", "systemd-networkd"])
        except RuntimeError:
            # Try again, with a little more sleep.
            print("Trying to ssh again with a little more sleep...")
            time.sleep(3)
            self.execute(["ssh", f"root@{name}.mayfirst.dev", "systemctl", "restart", "systemd-networkd"])
        # Now do custom restarts.
        for restart in restarts:
            if name == restart[0]:
                print(f"Restarting {restart[1]} on {restart[0]}")
                max_tries = 7
                attempt = 0
                while True:
                    if attempt == max_tries:
                        print(f"Failed to restart {restart[1]} {restart[0]}")
                        break;
                    try:
                        self.execute(["ssh", f"root@{name}.mayfirst.dev", "systemctl", "restart", restart[1]])
                        print(f"Successfully restarted restart {restart[1]} {restart[0]} on try number {attempt}.")
                        break
                    except RuntimeError:
                        # Try again, with a little more sleep.
                        attempt += 1
                        print(f"Trying to restart service with a little more sleep. Attempt {attempt} out of {max_tries}")
                        time.sleep(10)

    def build_and_start_containers(self):
        for name, ip in self.hosts:
            self.build_container(name, ip) 
            self.start_container(name)
            self.commit_container(name)

    def check_etc_hosts_file(self):
        print()
        print("Checking /etc/hosts file for necessary additions...")
        print()
        additions_needed = False
        for name, ip in self.domain_names:
            with open('/etc/hosts') as f:
                if not "{0} {1}".format(ip, name) in f.read():
                    additions_needed = True
                    print("{0} {1}".format(ip, name))
        print()
        if additions_needed:
            return False
        return True
