# Allow communication and sync'ing between our ansible repo and our control panel.
import re
import requests
import subprocess

class SowerRed():
    username = None 
    password = None
    url = "https://members.mayfirst.org/cp/api.php"
    inventory = None

    def strip_unit(self, str):
        match = re.match('(^[0-9]+)', str)
        if match:
            return int(match.group(1))
        raise RuntimeError("Failed to strip {0}.".format(str))

    def post_to_red_api(self, query, obj=None):
        data= {
            'user_name': self.username,
            'user_pass': self.password,
            'object': obj,
        }
        for key in query:
            data[key] = query[key]

        # Make this warning go away: https://github.com/urllib3/urllib3/issues/497
        import logging
        logging.captureWarnings(True)
        r = requests.post(self.url, data=data )
        r.raise_for_status()
        json = r.json()
        if json["is_error"] != 0:
            print("Entering the following item.")
            print(query)
            raise RuntimeError(" ".join(json["error_message"]))
        return r.json()

    def get_red_vps_entries(self):
        query = {
            'action': 'select',
            'where:vps_status:': 'active',
            'max_records': 0 
        }
        response = self.post_to_red_api(query, obj='vps')

        # Rekey the entries by server name so it's easy to see which entries
        # we have and which ones we need.
        entries = {}
        if "values" in response:
            for entry in response["values"]:
                server = entry["vps_server"]
                entries[server] = entry

        return entries

    def insert_vps_entry(self, entry):
        query = {
                "action": "insert"
        }
        for key in entry:
            query["set:{0}".format(key)] = entry[key]

        r = self.post_to_red_api(query, obj='vps')

    def update_vps_entry(self, vps_id, entry):
        query = {
                "action": "update",
                "where:vps_id": vps_id
        }
        for key in entry:
            query["set:{0}".format(key)] = entry[key]

        r = self.post_to_red_api(query, obj='vps')

    def dns_record_exists(self, dns_type, fqdn, ip):
        query = {
            'action': 'select',
            'where:dns_type:': dns_type,
            'where:dns_fqdn': fqdn,
            'where:dns_ip': ip,
            'where:service_id': 9,
            'where:hosting_order_id': 1,
        }
        response = self.post_to_red_api(query, obj='item')

        if "values" in response:
            if len(response["values"]) > 0:
                return True
        return False

    def create_dns_record(self, dns_type, fqdn, ip):
        query = {
            'action': 'insert',
            'set:dns_type:': dns_type,
            'set:dns_fqdn': fqdn,
            'set:dns_ip': ip,
            'set:service_id': 9,
            'set:hosting_order_id': 1,
        }
        response = self.post_to_red_api(query, obj='item')
        
    def grant(self):
        # Login to the given server as root and extract the password saved in the
        # /usr/local/etc/red/red_node.conf file.
        login = "root@{0}".format(self.host) 
        host_parts = self.host.split(".")
        user = "red-{0}".format(host_parts[0])
        path = "/usr/local/etc/red/red_node.conf"
        out = subprocess.run(["/usr/bin/ssh", login, "grep db_pass /usr/local/etc/red/red_node.conf"], capture_output=True, text=True)
        if ( out.returncode != 0 or not out.stdout):
          print("Failed to extract password from the host's /usr/local/etc/red/red_node.conf file.")
          return False
        else:
          password = out.stdout.split('=')[1].strip().rstrip(';')
          print("Found password for {0}, granting permissions on seso database on hay.mayfirst.org".format(self.host))
          # Now execute the grant statements on hay.
          grants = [
              "GRANT SELECT on seso.* to '{0}'@'%' identified by {1} REQUIRE SSL;".format(user, password),
              "GRANT UPDATE on seso.red_item to '{0}'@'%';".format(user, password),
              "GRANT INSERT on seso.red_error_log to '{0}'@'%';".format(user, password)
          ];
          for grant in grants:
            cmd = [
                "/usr/bin/ssh",
                "root@hay.mayfirst.org",
                "/usr/bin/mysql",
                "-e",
                '"{0}"'.format(grant)
            ]
            print(cmd)
            subprocess.run(cmd, check=True)


    def sync(self):
        self.inventory.set_hosts()
        self.inventory.set_host_vars()
        host_vars = self.inventory.host_vars_yaml
        ansible_entries = {}
        for host in host_vars:
            values = host_vars[host]
            try:
                member_id = values["member_id"]
            except KeyError:
                print("FYI, missing member_id in host {0}.".format(host))
            if member_id != 1:
                cpus = 0
                ram = 0
                ssd = 0
                disk = 0
                managed = 'y' 
                if "kvm" in values:
                    kvm = values["kvm"]
                    ram = self.strip_unit(kvm["ram"])
                    if "smp" in kvm:
                        cpus = int(kvm["smp"])
                    else:
                        cpus = 1
                    disk = 0
                    ssd = 0

                    if kvm["root_disk"]["driver"] == "scsi-hd":
                        ssd = self.strip_unit(kvm["root_disk"]["size"]) * 1024 * 1024 * 1024
                    else:
                        if "cp_allocated_disk" in kvm:
                            disk = self.strip_unit(kvm["cp_allocated_disk"]) * 1024 * 1024 * 1024
                        else:
                            disk = self.strip_unit(kvm["root_disk"]["size"]) * 1024 * 1024 * 1024

                    for data_disk in kvm["data_disks"]:
                        if data_disk["driver"] == "scsi-hd":
                            ssd = ssd + self.strip_unit(data_disk["size"]) * 1024 * 1024 * 1024
                        else:
                            if "cp_allocated_disk" in kvm:
                                continue
                            disk = disk + self.strip_unit(data_disk["size"]) * 1024 * 1024 * 1024
                    vps_type = "virtual"
                else:
                    vps_type = "physical"

                if host == "saadawi.mayfirst.org" or host == "ngugi.mayfirst.org":
                    location = "linode"
                else:
                    location = "telehouse"

                if "mayfirst_managed" in values and values["mayfirst_managed"] == False:
                    managed = 'n' 

                ansible_entries[host] = {
                    "member_id": member_id,
                    "vps_server": host,
                    "vps_cpu": cpus,
                    "vps_ram": ram,
                    "vps_ssd": ssd,
                    "vps_hd": disk,
                    "vps_type": vps_type,
                    "vps_location": location,
                    "vps_managed": managed
                }

        red_entries = self.get_red_vps_entries()
        update_entries = {}
        insert_entries = {}
        int_attributes = [ "member_id", "vps_cpu", "vps_ram", "vps_hd", "vps_ssd" ]
        for host in ansible_entries:
            ansible_entry = ansible_entries[host]
            if host in red_entries:
                update = [] 
                red_entry = red_entries[host]
                for attr in red_entry:
                    if not attr in ansible_entry:
                        continue
                    if attr in int_attributes:
                        red_entry[attr] = int(red_entry[attr])
                    if red_entry[attr] != ansible_entry[attr]:
                        diff = "{0}: update from {1} to {2}".format(attr, red_entry[attr], ansible_entry[attr])
                        update.append(diff)
                if len(update) > 0:
                    update_entries[host] = ",".join(update)
            else:
                insert_entries[host] = ansible_entries[host]
        
        for host in update_entries:
            print("{0}: {1}".format(host, update_entries[host]))
        for host in insert_entries:
            print("{0}: insert new record".format(host))

        count_insert = len(insert_entries)
        count_update = len(update_entries)
        if count_insert == 0 and count_update == 0:
            print("Nothing to insert or update. In sync")
            return

        print("{0} new servers to insert, {1} to update.".format(len(insert_entries), len(update_entries)))
        answer = input("Do You Want To Continue? [y/n] ")
        if answer == "y":
            print("Inserting new records...")
            for host in insert_entries:
               self.insert_vps_entry(insert_entries[host]) 
            print("Updating entries...")
            for host in update_entries:
                vps_id = red_entries[host]["vps_id"]
                self.update_vps_entry(vps_id, ansible_entries[host])

        else:
            print("Not proceeding.")
