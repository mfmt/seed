# Sower

The `sower` command provides an easy way to run command commands. See our
[sower docs](https://docs.mayfirst.org/ansible-sower/) for more information.
